<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* module id*/ 
$module_name = 'xmail';
$version='20240808';
$project = "Mailing";
$main_file="adres";
$sub_file = "standen";
$default_template = '/mail.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffl::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;

/* file references */
$oFC->file_ref [ 99 ] = LOAD_DBBASE . "_".$main_file;
$oFC->file_ref [ 98 ] = LOAD_DBBASE . "_" .$sub_file;
$oFC->file_ref [ 97 ] = TABLE_PREFIX . 'users';

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity");

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 0;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten */
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
			$oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}		

/* spif test */
$_SESSION[ 'page_h' ] = $oFC->page_content [ 'HASH' ]; 

/* get memory values */
$oFC->gsm_memorySaved ( );

/* Verwerk standaard input on selection / deselection mail destinations */
foreach( $_POST as $pay => $load) { 
	if ( substr ( $pay, 0, 4 ) == "gsm_" ) $oFC->page_content  [ strtoupper ( substr ( $pay, 4 ) ) ] = $load; 
} 

$Match = isset ($_POST [ 'vink' ] ) 
	? $_POST [ 'vink' ] 
	: array ();
$Check = ( count ( $Match ) > 0 ) 
	? true 
	: false; 
/* when absent neglect them */

if ($oFC->setting [ 'debug' ] == "yes" )  Gsm_debug (array ( "post"=>$_POST, $Match , $Check ), __LINE__ . __FUNCTION__ ); 

/* alternatief from e-mail address */
$oFC->setting [ 'droplet' ] [ LANGUAGE. "10" ] = isset ( $oFC->page_content  [ 'FROM' ] ) 
	? $oFC->page_content  [ 'FROM' ] 
	: $oFC->setting [ 'droplet' ] [ LANGUAGE. "10" ] ;

/* process (default) destination selection */
if ( !isset ( $oFC->page_content  [ 'TO' ] ) ) {
	if ( $oFC->memory [ 1 ] > 0 ) { 
		$oFC->page_content  [ 'TO' ] = $oFC->memory [ 1 ];
	} else { 
		$oFC->page_content [ 'TO'] = 1 ;
	} 
}
$oFC->page_content  [ 'TO1' ] = $oFC->language [ 'sel_mode' ] [ $oFC->page_content  [ 'TO' ] ];
$oFC->page_content  [ 'OPT_TO' ] = $oFC->gsm_selectOption ( $oFC->language [ 'sel_mode' ], $oFC->page_content [ 'TO1' ] );
$oFC->memory [ 1 ] = $oFC->page_content  [ 'TO' ] ;

/* process Mail text format selection */
$SecArr = $oFC->gsm_StandenText ( $oFC->setting [ 'mail' ] ); 
if ( !isset ($oFC->page_content  [ 'SEC' ] ) ) {
	if ( $oFC->memory [ 2 ] > 1 ) { 
		$oFC->page_content  [ 'SEC' ] = $oFC->memory [ 2 ];
	} else { 
		$oFC->page_content  [ 'SEC' ] = array_search ( $oFC->setting [ 'mail' ], $SecArr ); 
	}
}

$oFC->page_content  [ 'SEC1' ] = $SecArr [ $oFC->page_content  [ 'SEC' ] ] ?? "";
$oFC->page_content  [ 'OPT_SEC' ] = $oFC->gsm_selectOption ( $SecArr, $oFC->page_content  [ 'SEC' ] );
$oFC->memory [ 2 ] = $oFC->page_content  [ 'SEC' ];

/* get mail tekst */
$oFC->page_content  [ 'CONTENT' ] = $oFC->gsm_StandenText ( $oFC->page_content  [ 'SEC' ], 1 );
$oFC->page_content  [ 'CONTENT' ] = $oFC->gsm_sanitizeStringS ( $oFC->page_content  [ 'CONTENT' ], "s{TAGS|STRIX|TOASC}" );

/* process default Groep */
if ( !isset ( $oFC->page_content  [ 'REF' ] ) ) {
	if ( strlen ($oFC->memory [ 3 ] ) == 2) { 
		$oFC->page_content  [ 'REF' ] = $oFC->memory [ 3 ];
	} else { 
		$oFC->page_content  [ 'REF'] = "XX" ;
	}
}
$oFC->page_content [ 'REF1'] = $oFC->setting [ 'entity' ][ $oFC->page_content [ 'REF'] ];
$oFC->page_content [ 'OPT_REF'] = $oFC->gsm_selectOption( $oFC->setting [ 'entity' ] , $oFC->page_content [ 'REF1'] );
$oFC->memory [ 3 ] = $oFC->page_content [ 'REF'] ;

/* process default Standen */

/* list of references */
$StandArr = array();
$checkArr = array();
$database->execute_query( 
	"SELECT * FROM `" . $oFC->file_ref [ 98 ] . "` WHERE `active` = '1' ORDER BY `updated` DESC", 
	true, 
	$checkArr);
foreach ($checkArr as $row) {
	if ( !isset( $StandArr  [ $row [ 'ref' ] ] ) )
	$StandArr [ $row [ 'ref' ] ] = $oFC->gsm_sanitizeStrings ( $row[ 'ref' ], "s{CLEAN}" ) . " - " . $oFC->gsm_sanitizeStrings ( $row [ 'content_long' ], "s{CLEAN}" ) ;
}

if ( !isset ( $oFC->page_content  [ 'STAND' ] ) ) {
	if ( strlen ( $oFC->memory [ 4 ] ) > 2) { 
		$oFC->page_content [ 'STAND' ] = $oFC->memory [ 4 ];
	} else { 
		$oFC->page_content [ 'STAND' ] = current ( $StandArr );
	}
}

if ( !isset ( $StandArr [ $oFC->page_content  [ 'STAND' ] ] ) ) { $oFC->page_content  [ 'STAND1' ] = "--"; 
} else {
	$oFC->page_content  [ 'STAND1' ] = $StandArr [ $oFC->page_content  [ 'STAND' ] ];
}

$oFC->page_content [ 'OPT_STAND'] = $oFC->gsm_selectOption ( $StandArr , $oFC->page_content [ 'STAND1'] );
$oFC->memory [ 4 ] = $oFC->page_content [ 'STAND'] ;

/* process selection */
$oFC->search_mysql = "";
if (isset ( $selection ) && strlen ( $selection ) >1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= "WHERE `" . $oFC->file_ref[ 99 ] . "`.`zoek` LIKE '" . $help . "'";
	$oFC->page_content  [ 'PARAMETER' ] = $selection;
	$oFC->selection = $selection;
	$oFC->page_content  [ 'SUB_HEADER' ] = strtoupper ( $oFC->page_content [ 'PARAMETER' ] );
} else { $oFC->page_content  [ 'PARAMETER' ] = "";}
$oFC->memory [ 5 ] = $oFC->page_content  [ 'PARAMETER' ];

// job to do ?
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "Mail":  //send mailing
			$oFC->page_content [ 'MODE' ] = 6;
			// to output evidence on pdf
			$oFC->setting [ 'pdf_filename' ] = sprintf ( '%s-%s-%s_%s.pdf', 
				$oFC->setting [ 'owner' ],
				date( "Y-m-d-Hi", time() ),
				$project,
				$oFC->page_content [ 'START' ]); 
			require_once( $place['includes'] . 'classes/' . 'pdf.inc' );
			break;
		case "Test":  // test mail
			$oFC->page_content [ 'MODE' ] = 7;
			break;
		case "View": //volgende / next
			$oFC->page_content  [ 'REFERENCE_ACTIVE2'] = ' active'; // show input fields
			$oFC->page_content  [ 'REFERENCE_ACTIVE3'] = ' active'; // show input fields
			$oFC->page_content [ 'MODE' ] = 8;
			break; 
		case "Reset": //terug
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			$oFC->page_content  [ 'P1'] = true; // eerste cycle 
			break;
	} 
} elseif ( isset ( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} else { // so standard display
	// set initial values
	$oFC->page_content  [ 'P1'] = true; // eerte cycle 
	$oFC->page_content  [ 'TO'] = 1; // default function
	$oFC->page_content  [ 'SUBJECT'] = "no subject";
}

// datacollection
if ( $oFC->page_content  [ 'P1'] ) {
	// actions first cycle
	$oFC->page_content  [ 'PARAMETER'] = $oFC->memory [ 5 ];
	$oFC->page_content  [ 'REFERENCE_ACTIVE1'] = ' active'; // show input fields
	$oFC->page_content  [ 'REFERENCE_ACTIVE2'] = ' active'; // show input fields
	$oFC->page_content  [ 'START'] = 1;
	$oFC->page_content  [ 'FROM'] = $oFC->setting [ 'droplet' ] [ LANGUAGE. "10" ] ;
} else {
	//create SQL
	$fields = array( "id", "name", "ref", "contact", "type", "comp", "email", "adres", "dat1", "dat2", "ref0" );
	$sql = "SELECT `" . implode ("`, `", $fields) . "` FROM `" . $oFC->file_ref[ 99 ] . "`";
	$sql .= " WHERE `" . $oFC->file_ref[ 99 ] . "`.`id` >= '1' ";
	if (strlen ($oFC->search_mysql) > 10) $sql = str_replace ( "WHERE", $oFC->search_mysql." AND ", $sql ) ;
	if ($oFC->page_content [ 'TO'] == 2 ) {
		$hulp = "WHERE `" . $oFC->file_ref[ 99 ] . "`.`type` > '1' AND ";
		$sql = str_replace ( "WHERE", $hulp , $sql ) ;	
	}
	if ($oFC->page_content [ 'TO'] == 3 ) {
		$hulp = "WHERE `" . $oFC->file_ref[ 99 ] . "`.`dat1` > '1970-01-01' AND ";
		$sql = str_replace ( "WHERE", $hulp , $sql ) ;	
	}
	if ($oFC->page_content [ 'TO'] == 5 ) {
		$hulp = "WHERE `" . $oFC->file_ref[ 99 ] . "`.`ref` LIKE '%".$oFC->page_content [ 'REF']."%' AND ";
		$sql = str_replace ( "WHERE", $hulp , $sql ) ;	
	}

	$results = array();
	$database->execute_query( $sql, true, $results);
	foreach ( $results as $keyrow => $row ) {
		$row = array_merge ($row, $oFC->gsm_standenSelect ( "s{TO}" , $row , $oFC->page_content ) );
		if ( isset ( $row [ 'keuze']) && strlen ( $row [ 'keuze'] ) > 1 ) {
			$key = array_search ( $row ['id'], $Match); 
			if (false !== $key) { 
				if ( $row ['keuze'] == "not" ) {
					if (strlen ( $row [ 'email' ] < 10 ) ) { $row ['keuze'] = "pst"; 
					} elseif ( $row [ 'note' ] == 1) { $row ['keuze'] = "pst"; 
					} else { $row ['keuze'] = "sel";}
				}
			} else {
				if ( $Check ) $row ['keuze'] = "des";
			}
			if ( strlen ( $row [ 'extra' ] ) > 3 
				|| $row [ 'keuze' ] == "des" 
				|| $row [ 'keuze' ] == "sel" 
				|| $row [ 'keuze' ] == "pst" ) 
					$oFC->page_content  ['RAP'][] = $row;
		}
		unset ($row [ 'extra' ]);
	}
}

// Verdere processing 
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0: //niet toegestaan
		$oFC->page_content  ['RAP'] = array();
		break;
	case 6: // send mail
		if ( !isset($first_case6 ) ) {
			$first_case6=true;
			$cntmailed=0;
			$cnttobeposted=0;
			$cntskipped=0;
			$vanaf = $oFC->page_content [ 'START']; 
			$tot = $oFC->page_content [ 'START'] + $oFC->setting [ 'max_mail' ];
			// init pdf
			$pdf = new PDF();
			global $owner;
			$owner = $oFC->setting [ 'owner' ];
			global $title;
			$title = ucfirst( $project );
			$run = date( DATE_FORMAT, time() ) . " " . date( TIME_FORMAT, time() );
			$kop= array ( '0'=>' ', '1'=>' ', '2'=>' ', '3'=>' ' );
			$kop_par= array ('1'=>'Mailing','2'=>'Te posten','3'=>'Not mailed');
			$cols=array(15, 15, 80, 80, 0, 0);
			$pdf->AliasNbPages();
			$pdf->AddPage();
			$pdf->ChapterTitle( 1, $kop_par[1] );  
			$pdf_text = '';
			$pdf_data = array( );
			$pdf_header = array( ucfirst( $kop[ 0 ] ), 	ucfirst( $kop[ 1 ] ), ucfirst( $kop[ 2 ] ), ucfirst( $kop[ 3 ] ) );
		}	
		foreach ($oFC->page_content  ['RAP'] as $row) {
			$WEB_ADRES0 = explode ('|', $row[ 'adres' ]);
			$row = array_merge ($row, $oFC->gsm_standenSelect ( "s{TO|}", $row ,$oFC->page_content ) );
			$parseArr = array ( 
				'WEB_SUBJECT' 	=> $oFC->page_content [ 'SUBJECT'],
				'WEB_VAR1' 		=> $oFC->page_content [ 'VAR1'],
				'WEB_VAR2'		=> $oFC->page_content [ 'VAR2'],
				'WEB_EMAIL'		=> $row['email'],
				'WEB_NAME'		=> trim ( str_replace ("|", " ", $row[ 'name' ] ) ),
				'WEB_ADRES'		=> trim ( str_replace ("|", " ", $row[ 'adres' ] ) ),
				'WEB_CONTACT'	=> trim ( str_replace ( "|", "  ", $row [ 'contact' ] ) ),
				'WEB_SINDS'		=> ($row['dat1'] > "1970-01-01" ) ? $row['dat1'] :"",
				'WEB_QTYSTR'	=> $row['ref0'],
				'WEB_ADRES1'	=> isset ( $WEB_ADRES0 [ 0 ]) ? $WEB_ADRES0 [ 0 ] : "",
				'WEB_ADRES2'	=> isset ( $WEB_ADRES0 [ 1 ]) ? $WEB_ADRES0 [ 1 ] : "",
				'WEB_ADRES3'	=> isset ( $WEB_ADRES0 [ 2 ]) ? $WEB_ADRES0 [ 2 ] : "",
				'WEB_ADRES4'	=> isset ( $WEB_ADRES0 [ 3 ]) ? $WEB_ADRES0 [ 3 ] : "",
				'WEB_STAND'		=> $row [ 'extra' ],
				'WEB_STAND_REF'	=> strlen ( $row [ 'extra' ] ) > 10 ? "referentie: " . $row [ 'id' ] : "",
				'WEB_MASTER'	=> $oFC->setting [ 'droplet' ] [ LANGUAGE. "4" ] ,
				'WEB_MASTER_MAIL'=>$oFC->setting [ 'droplet' ] [ LANGUAGE. "10" ] ,
				'WEB_DATE' 		=> date ( DATE_FORMAT, time () ),
				'WEB_TIME' 		=> date ( TIME_FORMAT, time () ),
				'WEB_ID'		=> $row [ 'ref' ] );
			$oFC->setting [ 'email' ] = $oFC->setting [ 'droplet' ] [ LANGUAGE. "10" ];
			$oFC->setting [ 'master' ] = $oFC->setting [ 'droplet' ] [ LANGUAGE. "4" ];
			$double_switch = $cntmailed + $cntskipped + $cnttobeposted +1;
			$skip = false;
			if 	( $double_switch < $vanaf) $skip = true;
			if 	( $double_switch >= $tot ) $skip = true; 
			if ( !$skip) { 
				if ( $row ['keuze' ] == 'sel' ) {
					$cntmailed ++;
					$oFC->page_content ['TOEGIFT'] .= $oFC->gsm_mail ($oFC->page_content [ 'CONTENT'], $parseArr ['WEB_EMAIL'], $parseArr, 2, $oFC->page_content [ 'SUBJECT'], $oFC->page_content [ 'CONTENT'] );
					$oFC->description .= " ". $cntmailed ."s:".$row [ 'ref' ] ?? $row [ 'id' ];
					$pdf_data[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						$row[ 'id' ], 
						$row [ 'keuze' ],
						$row [ 'email' ], 
						trim(str_replace ("|", " ", $row[ 'name' ]) ),
						'',
						'' ) ) );
					$oFC->page_content [ 'START'] = 1;
				} elseif ( $row ['keuze' ] == 'pst' ) {
					$cnttobeposted++;
					if (strlen ( $row [ 'email' ] ) > 10  ) { 
						$oFC->page_content ['TOEGIFT'] .= $oFC->gsm_mail ($oFC->page_content [ 'CONTENT'], $parseArr ['WEB_EMAIL'], $parseArr, 2, $oFC->page_content [ 'SUBJECT'], $oFC->page_content [ 'CONTENT'] );
					} else {
						$oFC->page_content ['TOEGIFT'] .= $oFC->gsm_mail ($oFC->page_content [ 'CONTENT'], $oFC->user ['email'], $parseArr, 2, $oFC->page_content [ 'SUBJECT'], $oFC->page_content [ 'CONTENT'] );

					}
					$oFC->description .= " ". $double_switch ."p:".$row [ 'ref' ];
					$pdf_data[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
						$row[ 'id' ], 
						$row [ 'keuze' ],
						$row [ 'email' ], 
						trim(str_replace ("|", " ", $row[ 'name' ]) ),
						'',
						'' ) ) );
					$oFC->page_content [ 'START'] = 1;
				}
			} else {
				$double_switch = $cntmailed + $cntskipped + $cnttobeposted +1;
				if 	( $double_switch < $vanaf) {
					$pdf_data[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", 
					$row[ 'id' ], 
					$row [ 'keuze' ]. " *",
					$row [ 'email' ], 
					trim(str_replace ("|", " ", $row[ 'name' ]) ),
					'',
					'' ) ) );
				}
				$cntskipped++;
				$oFC->page_content [ 'START'] = $tot;
			}
		}
		if ( isset ( $first_case6 ) ) {
			$pdf->DataTable( $pdf_header, $pdf_data, $cols );
			$pdf->AddPage();
			$pdf_text .= "\n\n" . "Subject :". "\n" . iconv ( "UTF-8", "ASCII//TRANSLIT", strip_tags ( $oFC->page_content  [ 'SUBJECT'] ) ); 
			$pdf_text .= "\n\n" . "Inhoud :" . "\n" . iconv ( "UTF-8", "ASCII//TRANSLIT", $oFC->page_content  [ 'CONTENT'] );
			$pdf->ChapterBody( $pdf_text );
			$pdf_text = '';
			$pdf->AddPage();
			$pdf_text .= "\n\n" . $oFC->setting [ 'droplet' ] [ LANGUAGE. "0" ] ;
			$pdf_text .= "\n\n" . $oFC->setting [ 'pdf_filename' ] . "\n" ;
			$LocalHulp = $cntmailed + $cnttobeposted - $cntskipped;
			$pdf_text .= "\n". $oFC->language [ 'PDF_TAIL'] ['TOTAL'] . $LocalHulp;
			$pdf_text .= "\n". $oFC->language [ 'PDF_TAIL'] ['MAILED'] . ( $cntmailed - $cntskipped ) ;
			$pdf_text .= "\n". $oFC->language [ 'PDF_TAIL'] ['POSTED'] . $cnttobeposted;
			$pdf_text .= "\n". $oFC->language [ 'PDF_TAIL'] ['MAILING'] . str_replace( "_", " ", $run ) . "\n\n";
			$pdf_text .= "\n". $oFC->language [ 'PDF_TAIL'] ['SELECTION'] . "  :";
			if ( isset ($vanaf) ) $pdf_text .= sprintf ( "\nProcessed range range %s%s", 
				$vanaf,
				($oFC->page_content  [ 'START'] >$vanaf ) ? " - ". ( $oFC->page_content  [ 'START'] - 1 ) : "eind");
			$pdf_text .= "\n". $oFC->page_content  [ 'START'];
			$pdf_text .= "\n". $oFC->page_content  [ 'TO1'];
			$pdf_text .= "\n". $oFC->page_content  [ 'REF']."/".$oFC->page_content  [ 'STAND'];	
			$pdf_text .= "\n". $oFC->page_content  [ 'SUBJECT'];	
			$pdf_text .= "\n". $oFC->page_content  [ 'VAR1'];				
			$pdf_text .= "\n". $oFC->page_content  [ 'VAR2'];			
			if ( strlen( $oFC->page_content ['PARAMETER'] ) > 1 ) $pdf_text .= "\n\n" . $oFC->language [ 'PDF_TAIL'] ['SELECTION']. $oFC->page_content ['PARAMETER'];
			// pdf output
			$pdf->ChapterBody( $pdf_text );
			$pdflink = sprintf ( "%s%s/%s/pdf/%s", LEPTON_PATH, MEDIA_DIRECTORY, LOAD_MODULE, $oFC->setting [ 'pdf_filename' ] );
			$pdf->Output( $pdflink, 'F' );
		}
		break;		
	case 7: // testmail
		if ( !isset ( $first_case7 ) ) {
			$first_case7 = true;
			foreach ($oFC->page_content  ['RAP'] as $row) {
				$WEB_ADRES0 = explode ('|', $row[ 'adres' ]);
				$row = array_merge ($row, $oFC->gsm_StandenSelect ( "s{TO|}", $row ,$oFC->page_content ) );
				$parseArr = array ( 
					'WEB_SUBJECT' 	=> "TESTMAIL :" . $oFC->page_content [ 'SUBJECT'],
					'WEB_VAR1' 		=> $oFC->page_content [ 'VAR1'],
					'WEB_VAR2'		=> $oFC->page_content [ 'VAR2'],
					'WEB_EMAIL'		=> $oFC->setting [ 'droplet' ] [ LANGUAGE. "10" ] ,
					'WEB_NAME'		=> trim ( str_replace ("|", " ", $row[ 'name' ] ) ),
					'WEB_ADRES'		=> trim ( str_replace ("|", " ", $row[ 'adres' ] ) ),
					'WEB_CONTACT'	=> "tel: " . trim ( str_replace ( "|", "  ", $row [ 'contact' ] ) ),
					'WEB_SINDS'		=> ($row['dat1'] > "1970-01-01" ) ? $row['dat1'] :"",
					'WEB_QTYSTR'	=> $row['ref0'],
					'WEB_ADRES1'	=> isset ( $WEB_ADRES0 [ 0 ]) ? $WEB_ADRES0 [ 0 ] : "",
					'WEB_ADRES2'	=> isset ( $WEB_ADRES0 [ 1 ]) ? $WEB_ADRES0 [ 1 ] : "",
					'WEB_ADRES3'	=> isset ( $WEB_ADRES0 [ 2 ]) ? $WEB_ADRES0 [ 2 ] : "",
					'WEB_ADRES4'	=> isset ( $WEB_ADRES0 [ 3 ]) ? $WEB_ADRES0 [ 3 ] : "",
					'WEB_STAND'		=> $row [ 'extra' ],
					'WEB_STAND_REF'	=> strlen ( $row [ 'extra' ] ) > 10 ? "referentie: " . $row [ 'id' ] : "",
					'WEB_MASTER'	=> $oFC->setting [ 'droplet' ] [ LANGUAGE. "4" ] ,
					'WEB_MASTER_MAIL'=>$oFC->setting [ 'droplet' ] [ LANGUAGE. "10" ] ,
					'WEB_DATE' 		=> date ( DATE_FORMAT, time () ),
					'WEB_TIME' 		=> date ( TIME_FORMAT, time () ),
					'WEB_ID'		=> $row [ 'id' ] );
					$oFC->setting [ 'email' ] = $oFC->setting [ 'droplet' ] [ LANGUAGE. "10" ];
					$oFC->setting [ 'master' ] = $oFC->setting [ 'droplet' ] [ LANGUAGE. "4" ];
				if ( $row ['keuze' ] == 'sel' ) {
					$oFC->page_content ['TOEGIFT'] .= $oFC->gsm_mail ($oFC->page_content [ 'CONTENT'], $parseArr ['WEB_EMAIL'], $parseArr, 1, $parseArr ['WEB_SUBJECT'], $oFC->page_content [ 'CONTENT'] );
					break;
				}
			}
		}
		break;	
	default: // default list 
		break;
}

/* the selection options */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	case 8:
	case 9:
	default: 
		$oFC->page_content ['SELECTION'] = $oFC->gsm_opmaakSel2 ( array ( 23, 6 ), '-' , $oFC->setting [ 'pdf_filename' ] ?? "" );
		if ( !$oFC->page_content [ 'P1'] && $Check ) $oFC->page_content ['SELECTION'] = $oFC->gsm_opmaakSel2 ( array ( 6, 21, 22, 23, 11 ), '-' , $oFC->setting [ 'pdf_filename' ] ?? "");
		break;
}

/* memory save */
$oFC->page_content  [ 'MEMORY' ] = $oFC->gsm_memorySaved (  ); 
	
/* output processing */
$oFC->page_content [ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content [ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content ['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content [ 'VERSIE' ] = $oFC->version; 
$oFC->page_content  [ 'MODE' ] = $oFC->page_content [ 'MODE' ];
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>