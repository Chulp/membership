<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* module id*/ 
$module_name = 'xsetupl';
$version='20240430';
$project="Login Module";
$main_file="adres";
$sub_file="standen";
$default_template = '/display.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffl::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;
$project= sprintf ("%s %s " , $oFC-> language [ 'TXT_SETUP' ], strtoupper ( $main_file )) ;

/* file references */
$oFC->file_ref [ 99 ] = LOAD_DBBASE . "_".$main_file;
$oFC->file_ref [ 98 ] = LOAD_DBBASE . "_" .$sub_file;
$oFC->file_ref [ 97 ] = TABLE_PREFIX . 'users';

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity");

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten */
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
			$oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

// spif test
$_SESSION[ 'page_h' ] = $oFC->page_content[ 'HASH' ]; 

// get memory values
$oFC->gsm_memorySaved ( );

if ($oFC->setting [ 'debug' ] == "yes" )  Gsm_debug (array ($_POST, $_GET ?? "", $oFC->page_content, $oFC , $selection ), __LINE__ . __FUNCTION__ );

/* 12 selection functions */
if (isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$selection = strtolower ( $selection );
	foreach ( array( "print" => "print" , "all" => "all", "debug" => "debug") as $pay => $load ) {
		if ( strstr ( $selection, $pay ) ) {
			$xmode .= $load; 
			$selection = trim ( str_replace ( $pay, "", strtolower ( $selection ) ) );
}	}	}

/* 13 selection */
$oFC->search_mysql = "";
if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE `zoek` LIKE '" . $help . "'";
} else { 
	$selection = "";
}
$oFC->page_content  [ 'PARAMETER' ] = trim( $selection );
$oFC->page_content  [ 'SUB_HEADER' ]= strtoupper ( $oFC->page_content [ 'PARAMETER' ] );

/* sips test before job */ 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
		$oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

/* some job to do */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "up":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("up", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ],  $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "down":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("down", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ], $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;

		case "Save":
			$oFC->page_content [ 'MODE' ] = 9;
			$oFC->recid = $_POST [ 'gsm_id' ];
			$FieldArr = array ();
			$FieldArr [ 'name'] = $oFC->gsm_sanitizeStringNA ( 3, 'name', $_POST [ 'gsmc_name1' ] ?? "" , $_POST [ 'gsmc_name2' ] ?? "", $_POST [ 'gsmc_name3' ] ?? "" , $_POST [ 'gsmc_name4' ] ?? "");	
			$FieldArr [ 'adres'] = $oFC->gsm_sanitizeStringNA ( 3, 'adres', $_POST [ 'gsmc_adres1' ] ?? "" , $_POST [ 'gsmc_adres2' ] ?? "", $_POST [ 'gsmc_adres3' ] ?? "" , $_POST [ 'gsmc_adres4' ] ?? "");
			$FieldArr [ 'ref'] =  $_POST [ 'gsmc_ref' ] ?? "XX" . $_POST [ 'gsm_id' ] ?? $_POST [ 'gsm_recid' ] ;			
			$oFC->page_content [ 'ACCESS'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file );
			$oFC->page_content [ 'ACCESS'] = array_merge ( $oFC->page_content [ 'ACCESS'] , $oFC->gsm_sanitizeStringNA ( 2, 'name' , $oFC->page_content [ 'ACCESS'] [ 'name' ] ) ) ;
			$oFC->page_content [ 'ACCESS'] = array_merge ( $oFC->page_content [ 'ACCESS'] , $oFC->gsm_sanitizeStringNA ( 2, 'adres' , $oFC->page_content [ 'ACCESS'] [ 'adres' ] ) ) ;
			break;
		case "New":
			$oFC->page_content [ 'MODE' ] = 9;
			$oFC->recid = $_POST [ 'gsm_id' ];
			$result = array();
			$database->execute_query( 
				sprintf ("SELECT * FROM `%s` WHERE `email` = '%s'", $oFC->file_ref [ 99 ], $_POST [ 'gsm_email' ] ),
				true, 
				$result);
			if ( count ( $result ) == 0 || $_POST [ 'gsm_email' ] == "" ) { 
				$FieldArr = array ();
				$FieldArr [ 'name'] = $oFC->gsm_sanitizeStringNA ( 3, 'name', $_POST [ 'gsmc_name1' ] ?? "" , $_POST [ 'gsmc_name2' ] ?? "", $_POST [ 'gsmc_name3' ] ?? "" , $_POST [ 'gsmc_name4' ] ?? "");
				$FieldArr [ 'adres'] = $oFC->gsm_sanitizeStringNA ( 3, 'adres', $_POST [ 'gsmc_adres1' ] ?? "" , $_POST [ 'gsmc_adres2' ] ?? "", $_POST [ 'gsmc_adres3' ] ?? "" , $_POST [ 'gsmc_adres4' ] ?? "");
				$FieldArr [ 'ref'] =  $_POST [ 'gsmc_ref' ] ?? "XX";			
				$oFC->page_content [ 'ACCESS'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 2, $main_file );
				$oFC->page_content [ 'ACCESS'] = array_merge ( $oFC->page_content [ 'ACCESS'] , $oFC->gsm_sanitizeStringNA ( 2, 'name' , $oFC->page_content [ 'ACCESS'] [ 'name' ] ) ) ;
				$oFC->page_content [ 'ACCESS'] = array_merge ( $oFC->page_content [ 'ACCESS'] , $oFC->gsm_sanitizeStringNA ( 2, 'adres' , $oFC->page_content [ 'ACCESS'] [ 'adres' ] )) ;
			} else {
				$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
					$oFC->language [ 'TXT_ACTIVE_DATA' ]. NL;
			}
			break;
		case "Select":
		case "View":
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} elseif ( isset ( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		case "select":
			if ($oFC->recid == "") { 
				$oFC->page_content [ 'MODE' ] = 9; 
			} else {
				$oFC->page_content [ 'MODE' ] = 8;
				$FieldArr = array ();
				$oFC->page_content [ 'ACCESS'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file );
				$oFC->page_content [ 'ACCESS'] = array_merge ( $oFC->page_content [ 'ACCESS'] , $oFC->gsm_sanitizeStringNA ( 2, 'name' , $oFC->page_content [ 'ACCESS'] [ 'name' ] ) ) ;
				$oFC->page_content [ 'ACCESS'] = array_merge ( $oFC->page_content [ 'ACCESS'] , $oFC->gsm_sanitizeStringNA ( 2, 'adres' , $oFC->page_content [ 'ACCESS'] [ 'adres' ] )) ;
			}
			break;
		case 'remove': //    
			/* 13_ Delete a record */
			if ($oFC->recid == "") { 
				$oFC->page_content [ 'MODE' ] = 9; 
			} else {
				$oFC->page_content [ 'MODE' ] = 9;
				$FieldArr = array ();
				$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 3, $main_file );
				$oFC->recid = "";
			}
			/* Delete a record */
			break;
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} else { 
	/* first run */
	$oFC->page_content [ 'P1' ] = true;
	$oFC->page_content [ 'MODE' ] = 9;	
	
	/* Default settings */
	$job = array ();
	
	foreach ( $oFC->file_ref as $LocalHulp ) {
		$payload = $place[ 'includes'] . str_replace ( TABLE_PREFIX, "setup_", $LocalHulp ) . ".php";
		if ( file_exists ( $payload ) ) { 
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade checked ' . $LocalHulp .  NL; 
			require_once ( $payload ); 
		} else {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' No Upgrade function ' . $LocalHulp .  NL; 
		}
	}
	/* create directories */
	$oFC->gsm_existDir ( sprintf ('%s%s' ,LEPTON_PATH , $oFC->setting [ 'collectdir' ] ), true );
	$oFC->gsm_existDir ( sprintf ('%s%s' ,LEPTON_PATH , $oFC->setting [ 'mediadir' ] ), true );
}

/* Additional functions */
if (LOAD_MODE == "x" && isset ( $xmode ) && strlen ( $xmode ) >3 ) require_once ( $place[ 'includes'] . 'repair.php' );

if ( $oFC->page_content [ 'MODE' ] == 9 ) {
	$pageok = true;
	
	/* bepaal aantal records */
	$result = array ( );
	$database->execute_query(
		sprintf ( "SELECT count(`id`) FROM `%s` %s", $oFC->file_ref[99], $oFC->search_mysql), 
		true, 
		$result);
	$row = current ( $result );
	$oFC->page_content [ 'aantal' ] = $row [ "count(`id`)" ];
		
	/* paging / accordeon /  make records unique / restore zoek/sort field */
	$limit_sql = $oFC->gsm_pagePosition ("sql", $oFC->page_content [ 'POSITION' ] , $oFC->page_content [ 'aantal' ], $oFC->setting [ 'qty_max' ]  );
	
	$query = "SELECT 
		`" . $oFC->file_ref[ 99 ] . "`.*,
		`" . $oFC->file_ref[ 97 ] . "`.`email` AS `mail`,
		`" . $oFC->file_ref[ 97 ] . "`.`username`,
		`" . $oFC->file_ref[ 97 ] . "`.`groups_id`
		FROM `" . $oFC->file_ref[ 99 ] . "`
		LEFT JOIN `" . $oFC->file_ref[ 97 ] . "`
		ON `" . $oFC->file_ref[ 97 ] . "`.`user_id` = `" . $oFC->file_ref[ 99 ] . "`.`adresid` " . $oFC->search_mysql  .
		"ORDER BY `" . $oFC->file_ref[ 99 ] . "`.`zoek` ASC ". $limit_sql;
	$result = array();
	$job= array();
	$database->execute_query( $query, true, $result);
	if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( $result, __LINE__ . __FUNCTION__ ); 
	if ( count ( $result ) > 0 ) { 
		$job= array();
		foreach ( $result as $rec => $row ) {
			
			/* membership */
			$result [ $rec ][ 'name' ] = $oFC->gsm_sanitizeStringS ( $result [ $rec ][ 'name' ], "s{CLEAN}" );
			$result [ $rec ][ 'adres' ] = $oFC->gsm_sanitizeStringS ( $result [ $rec ][ 'adres' ], "s{CLEAN}" );
			$member='--';
			if ($row[ 'dat1' ] > "1970-01-01" && $row[ 'dat1' ] < date("Y-m-d")){
				if ($row[ 'dat2' ] > $row[ 'dat1' ] && $row[ 'dat2' ] < date("Y-m-d")){
					$member="ex-lid";
				} else {
					$member="lid";
					$numtest_qtystr = $oFC->gsm_sanitizeStringv ($row['ref0'], 'v{0;0;100000}');
					if ($numtest_qtystr >0) $member .= "<br />". $numtest_qtystr;
			}	}
			$result [ $rec ][ 'member' ] = $member;
				
			/* checks	*/		
			$TEMPLATE = "UPDATE `%s` SET %s WHERE `id` = '%s'";
			$arout = $oFC->setting [ 'zoek' ] [ 'adres' ];
			$adresArr = array();
			
			/* referlist correct */
			if ( isset ( $oFC->setting [ 'entity' ] [ substr ( $row [ 'ref' ], 0, 2 ) ] ) ) {
				if ( substr ( $row [ 'ref' ], 2 ) != $row [ 'id' ] ) { 
					//check first art
					$result [ $rec ][ 'ref' ] = substr ( $row [ 'ref' ],0, 2 ) . $row [ 'id' ];
					$adresArr [ 'ref' ] = $result [ $rec ][ 'ref' ];
				}
			$result [ $rec ][ 'group' ] = $oFC->setting [ 'entity' ] [ substr ( $row [ 'ref' ], 0, 2 ) ] ?? "XX";	
			} else {
				$LocalHulp = sprintf ( "entity missing %s", $row [ 'ref' ] );
				$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . $LocalHulp . NL;
				$result [ $rec ][ 'group' ] = $oFC->setting [ 'owner' ];	
				
				/* correction * /
				$result [ $rec ][ 'ref' ] = $oFC->setting [ 'owner' ] . $row [ 'id' ];
				$adresArr [ 'ref' ] = $result [ $rec ][ 'ref' ];
				/* end correction */
			}
			
			/* zoek correct */
			foreach ( $row as $pay => $load ) $arout = str_replace( "|".$pay."|", "|".$load."|", $arout);
			$arout = str_replace ("||", "|", $arout);
			$arout = strtolower ( $oFC->gsm_sanitizeStringS ( $arout, 's{TOASC}' ) ); 
			if ( $row ['zoek'] != $arout ) $adresArr ['zoek'] = $arout;
			
			/* iets te schrijven */
			if ( count ( $adresArr ) >0 ) {
				/* iets te schrijven */
				$job [] = sprintf( $TEMPLATE, $oFC->file_ref[99], $oFC->gsm_accessSql ( $adresArr, 2 ), $row['id'] );
				/* debug * / Gsm_debug ( sprintf( $TEMPLATE, $oFC->file_ref[99], $oFC->gsm_accessSql ( $adresArr, 2 ), $row['id'] ), __LINE__ . __FUNCTION__ ); 
				/* klaar gezet */
		}	}
		if ( isset ( $job ) && count ( $job ) > 0 ) {
			foreach ( $job as $key => $query ) $database->simple_query ( $query ) ;
			$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  .
				$oFC->language [ 'TXT_REC_CHANGE' ] . count ( $job ). NL;
		}
	} else {
		$oFC->page_content[ 'STATUS_MESSAGE' ] .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . 
			$oFC->language [ 'TXT_ERROR_DATA' ] . NL;
		 
		// verify consistency executed
//		$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . 
			$oFC->language [ 'TXT_CONSISTENCY' ] . NL;
		$update_go = false;
		$job = array();
		$result = array();
		$query = "SELECT *  FROM `" . $oFC->file_ref [ 97 ] . "`";
		$database->execute_query( $query, true, $result);
		// looplangs de user records en check adres record aanwezigheid. voeg toe of pas aan als er eenzelfde e-mail adres is 
		foreach ($result as $row) {
			$check= "SELECT *  FROM `" . $oFC->file_ref [ 99 ] . "` WHERE `adresid` = '". $row['user_id'] . "'";
			$checks = array();
			$database->execute_query( 
				sprintf ( "SELECT *  FROM `%s` WHERE `adresid` = '%s'", $oFC->file_ref [ 99 ],  $row['user_id'] ),
				true, 
				$checks);
			if ( count ( $checks ) == 0 ) {
				$checks2 = array();
				$database->execute_query ( 
					sprintf ( "SELECT *  FROM `%s` WHERE `email` = '%s'", $oFC->file_ref [ 99 ],  $row[ 'email' ] ),
					true, 
					$checks2);
				if ( count($checks2) == 0){
					$hulpArr = array( );
					$hulpArr[ 'name' ] = $row [ 'display_name' ];
					$hulpArr[ 'email' ] = trim ( $row[ 'email' ]);
					$hulpArr[ 'adresid' ] = $row [ 'user_id' ];
					$hulpArr[ 'ref' ] = $oFC->setting [ 'owner' ];
					$hulpArr[ 'active' ] = '1';
					$job [ ] = "INSERT INTO `" . $oFC->file_ref [ 99 ] . "` " .  $oFC->gsm_accessSql ( $hulpArr, 1 );
				}  elseif ( count ( $checks2 ) > 0 ) {
					foreach ($checks2 as $chk) {
						$repairarr = array ( "adresid" => $row[ 'user_id' ] ); 
						$job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . $oFC->gsm_accessSql ( $repairarr, 2 ) . " WHERE `id` = '" . $chk['id'] . "'";  
		}	}	} 	}
		if ( isset ( $job ) && count ( $job ) > 0 ) {
			$m = count ( $job );
			foreach ( $job as $key => $query) {
				$database->simple_query( $query);
				$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . $query. NL;
			}
			$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' .
				$oFC->language [ 'TXT_REC_CHANGE' ] .$m .NL;
	}	}
	$oFC->page_content [ 'RESULTS' ] = $result;
}

if ( $oFC->page_content [ 'MODE' ] == 8 ) {

	/* label */
	$oFC->page_content [ 'FORM_CLASS_D' ] = "";
	if ( $oFC->page_content [ 'FORM_CLASS_D' ] != 'nixhier' ) { 
		$oFC->page_content [ 'GSM_ADRES_LABEL' ] = $oFC->gsm_persoonInfo ( 'a', $oFC->page_content [ 'ACCESS'] );
		$oFC->page_content [ 'GSM_NOTES' ] = $oFC->gsm_persoonInfo ( 'b' , $oFC->page_content [ 'ACCESS']);
		$oFC->page_content [ 'GSM_REF' ] = $oFC->gsm_persoonInfo ( 'c', $oFC->page_content [ 'ACCESS'] );
	}

	/* Standen */
	$oFC->page_content [ 'FORM_CLASS_S' ] = "";
	if ( $oFC->page_content [ 'FORM_CLASS_S' ] != 'nixhier' ) { 
		$oFC->page_content [ 'GSM_STANDEN' ] = $oFC->gsm_standenStatus ( $oFC->page_content [ 'ACCESS'] [ 'id' ] );
		if (strlen ( $oFC->page_content ['GSM_STANDEN' ] ) < 10 )  $oFC->page_content ['FORM_CLASS_S'] = 'nixhier';
	}

	/* documenten */
	$oFC->page_content [ 'FORM_CLASS_F' ] = "";
	if ( $oFC->page_content [ 'FORM_CLASS_F' ] != 'nixhier' ) { 
		$oFC->page_content [ 'GSM_ARCHIEF' ] = "";
		if ( strlen( $oFC->user [ 'ref' ] ) > 2 ) {
			$help = array( $oFC->page_content [ 'ACCESS'] [ 'ref' ] , substr ( $oFC->page_content [ 'ACCESS'][ 'ref' ] , 0, 2 ) ) ;
			$oFC->page_content ['GSM_ARCHIEF' ] .= $oFC->gsm_scanDir ( LEPTON_PATH . $oFC->setting [ 'mediadir' ], 3, 50, $help );
			$oFC->page_content ['GSM_ARCHIEF' ] .= $oFC->gsm_scanCastor ( 
				substr (  $oFC->page_content [ 'ACCESS'][ 'ref' ], 0, 2 ), 
				substr (  $oFC->page_content [ 'ACCESS'][ 'ref' ], 2 )  );
		}
		if (strlen($oFC->page_content ['GSM_ARCHIEF' ]) <10)  $oFC->page_content ['FORM_CLASS_F'] ="nixhier";
	}
}

switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->page_content [ 'SELECTION' ] = "";
		break;
	case 8:
		/* debug * / Gsm_debug (array ( $oFC ), __LINE__ . __FUNCTION__ ); /* debug */
		$oFC->page_content [ 'ACCESS' ] [ 'TYPE' ] = $oFC->gsm_selectOption ( $oFC->language [ 'TXT_TYPE' ], $oFC->page_content [ 'ACCESS' ] [ 'type' ], 1);
		$oFC->page_content [ 'ACCESS' ] [ 'ACTIVE' ] = $oFC->gsm_selectOption ( $oFC->language [ 'active' ], $oFC->page_content [ 'ACCESS' ] [ 'active' ], 1);
		$oFC->page_content [ 'ACCESS' ] [ 'COMP' ] = $oFC->gsm_selectOption ( $oFC->language [ 'TXT_COMP' ], $oFC->page_content [ 'ACCESS' ] [ 'comp' ], 1);
		$oFC->page_content [ 'ACCESS' ] [ 'REF' ] = $oFC->gsm_selectOption  ( $oFC->setting [ 'entity' ], substr ( $oFC->page_content [ 'ACCESS' ] [ 'ref' ], 0, 2 ), 2	);
		$oFC->page_content [ 'SELECTIONA' ] = $oFC->gsm_opmaakSel2 ( array ( 2, 6, 8, 11), '-', $pdflink ?? "-" );
		break;
	case 9:
	default: 
		$oFC->page_content [ 'SELECTIONA' ] = $oFC->gsm_opmaakSel2 ( array ( 10 ) );
		if ( $oFC->page_content [ 'aantal' ] > $oFC->setting [ 'qty_max' ] ) 
			$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel2 ( array ( 13), "-", "-", $oFC->page_content [ 'POSITION' ], $oFC->page_content [ 'aantal' ] , $oFC->setting [ 'qty_max' ]  );
		break;
}  
 
/* output processing */
// memory save
$oFC->page_content [ 'MEMORY' ] = $oFC->gsm_memorySaved (  ); 

// als er boodschappen zijn deze tonen in een error blok
$oFC->page_content[ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content [ 'STATUS_MESSAGE' ] ) >4 ) $oFC->page_content [ 'MESSAGE_CLASS' ] = "ui error message"; 
$oFC->page_content [ 'VERSIE' ] = $oFC->version; 
$oFC->page_content [ 'MODE' ] = $oFC->page_content [ 'MODE' ];
if ( LOAD_MODE == "x" )  $_SESSION [ 'last_edit_section' ] = $section_id; 

switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0: //list
		break;
	case 8: //list
		if ( isset ( $oFC->language [ 'TXT_ADRES' ] ) ) $oFC->page_content [ 'ACCESS' ] = array_merge ( $oFC->page_content [ 'ACCESS' ], $oFC->language [ 'TXT_ADRES' ] );
		$oFC->page_content [ 'ACCESS'] [ 'adres' ] = $oFC->gsm_sanitizeStringNA ( 4, "", $oFC->page_content [ 'ACCESS'] [ 'adres' ] )  ;	
		$oFC->page_content [ 'ACCESS'] [ 'name' ] = $oFC->gsm_sanitizeStringNA ( 4, "", $oFC->page_content [ 'ACCESS'] [ 'name' ] )  ;			
		$oFC->page_content [ 'RECID' ] = $oFC->recid;
		$template_name= '@'. LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . '/back.lte';
		$oFC->page_content [ 'REFERENCE_ACTIVE1' ] = "active";
		break;
  default: //list
  		$oFC->page_content [ 'REFERENCE_ACTIVE1' ] = "active";
		$template_name= '@'. LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . '/back.lte';
		break;
} 

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>