<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
$this->version ['print1'] = "20250210";


/* welke overzichten */
$prmode = array (); 
$printOK = false;
$partOK = true;
if ( strstr ( $selection, "print1" ) ) 	$prmode [ ] = 1; /* nog niet ondersteund */
if ( strstr ( $selection, "print2" ) ) 	$prmode [ ] = 2; /* nog niet ondersteund */
if ( strstr ( $selection, "print3" ) ) 	$prmode [ ] = 3; /* nog niet ondersteund */
if ( strstr ( $selection, "part1" ) ) 	$prmode [ ] = 5; 
if ( strstr ( $selection, "part2" ) ) 	$prmode [ ] = 6; 
if ( strstr ( $selection, "part3" ) ) 	$prmode [ ] = 7; 
if ( strstr ( $selection, "part4" ) ) 	$prmode [ ] = 8;
if ( strstr ( $selection, "part5" ) ) 	$prmode [ ] = 9;
if ( strstr ( $selection, "part6" ) ) 	$prmode [ ] = 10;
if ( strstr ( $selection, "part7" ) ) 	$prmode [ ] = 11;
if ( strstr ( $selection, "part8" ) ) 	$prmode [ ] = 12;
if ( strstr ( $selection, "part9" ) ) 	$prmode [ ] = 13;
if ( strstr ( $selection, "all" ) )		$prmode = array ( 0 ); /* alles */
if ( count ( $prmode ) < 1) $prmode = array ( 1 ); 

// $this->setting [ 'debug' ] = "yes";
// input
if ($this->setting [ 'debug' ] == "yes") gsm_debug (array (
	'query' => $query,
	'project' => $project,
	'selection' => $selection,
	'func' => $func,
	'run' => $run,
	'this' => $this ), __LINE__ . __FUNCTION__ ); 
	
$title = ucfirst ( $project );
$regelcount  = 0;
$chaptercount = 0; //amount of chapters
$pdf_data   = array( );
$pdf_text   = '';

/* processing part 1 and part 2 */
$sql1 = "SELECT `" . $this->file_ref [ 98 ] . "`.*,
	`" . $this->file_ref [ 99 ] . "`.`id` as `user_id`,
	`" . $this->file_ref [ 99 ] . "`.`name` as `user_name`,
	`" . $this->file_ref [ 99 ] . "`.`adres` as `user_adres`,
	`" . $this->file_ref [ 99 ] . "`.`ref` as `user_referlist`,
	`" . $this->file_ref [ 99 ] . "`.`ref1` as `user_ref1`,
	`" . $this->file_ref [ 99 ] . "`.`contact` as `user_contact`,	
	`" . $this->file_ref [ 99 ] . "`.`email` as `user_email`
FROM `" . $this->file_ref [ 98 ] . "` LEFT JOIN `" . $this->file_ref [ 99 ] . "`
ON `" . $this->file_ref[ 99 ] . "`.`id` = `" . $this->file_ref [ 98 ] . "`.`adresid`";

if ( strlen ( $query ) > 1 ) {
	$zoekstring = "%". $query . "%";
	$sqla = sprintf ( " WHERE `%s`.`active`='1' AND `%s`.`ref` LIKE '%s' ORDER BY `%s`.`type`, `%s`.`ref`, `%s`.`comment`", 
		$this->file_ref [ 98], 
		$this->file_ref [ 98], 
		$zoekstring, 
		$this->file_ref [ 98],
		$this->file_ref [ 98],
		$this->file_ref [ 98]);
	$sql1 .= $sqla; 
} else {
	$sqla = sprintf ( " WHERE `%s`.`active`='1' ORDER BY `%s`.`type`, `%s`.`ref`, `%s`.`datumist`", 
		$this->file_ref [ 98], 
		$this->file_ref [ 98], 
		$this->file_ref [ 98],
		$this->file_ref [ 98]);
	$sql1 .= $sqla; 
}

$fields = "*";
$sql2 = sprintf ( "SELECT `%s`.%s FROM `%s` WHERE `%s`.`active`='1' ORDER BY `%s`.`ref`",
		$this->file_ref [ 99 ], 
		$fields , 
		$this->file_ref [ 99 ], 
		$this->file_ref [ 99 ],
		$this->file_ref [ 99 ]);

$results = array();
$database->execute_query( 
	$sql1, 
	true, 
	$results);
$LocalHulp = count ($results);

if ($this->setting [ 'debug' ] == "yes") gsm_debug (array ( 'sql' => $sql1, 'results' => $results ), __LINE__ . __FUNCTION__ ); 

/* this function works with label layout  */
$printOK = false;
/* end what is supported 
$ledenAryOk = array ();
$ledenAryNok = array ();
if ( $LocalHulp >0 ) $printOK = true;
*/

if ( $printOK ) {
	if ( in_array ( 1, $prmode ) ) {
		$regelcount=0;
		$prtype [ ] = 1;
		$levelbreak = "--";
		$pdf_cols = array( 25, 120, 35, 0, 0, 0 ); 
		$pdf_header = array ( "ref", "persoon", "", "", "", "" );
		foreach	( $results as $row ) {
			/* skip this record */
			$row ['name'] = $this->gsm_sanitizeStrings( $row ['name'], "s{CLEAN}");
			$row ['user_adres'] = $this->gsm_sanitizeStrings( $row ['user_adres'], "s{CLEAN}");
			if ( !in_array ( $row [ 'type' ], $prtype ) ) continue; // niet gevraagd
			if ( $row [ 'type' ] == 2 ) continue; // nog niet geimplementeerd
			if ( $row [ 'type' ] == 3 ) continue; // nog niet geimplementeerd
			/* nog niet betaald  check */
			$row ['statusopen'] = ( $row[ 'amtsoll'] > $row[ 'amtist'] ) ? true : false;
			if ( $row [ 'type' ] == 1 && $row ['statusopen'] ) continue; // not paid
			if  ( $levelbreak != $row [ 'type' ] . $row [ 'ref' ] ) {
				/* afsluiten */
				/* level end calc */
				/* level end printing */
				if ( count ( $pdf_data ) > 0 ) $pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
				$pdf_data = array( );
				/* new one */
				$levelbreak = $row [ 'type' ] . $row [ 'ref' ] ;
				$chaptercount++;
				$pdf->AddPage();
				$pdf->ChapterTitle( $chaptercount, sprintf ( "( %s ) %s %s ", $tekst [ $row [ 'type' ] ] [ 0 ], $tekst [ $row [ 'type' ] ] [ 1 ], $row [ 'ref' ] ) );
			}
			$regelcount++;
			if ( in_array ( $row [ 'type' ], array ( 1  ) ) ) {
				$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
					$row [ 'user_referlist' ],
					$row [ 'name' ],
					$row [ 'datumist' ],
					"",
					"",
					$row [ 'amtsoll' ] ) ) );
				$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
					"",
					$row ['user_adres'],
					( $row [ 'amtist' ] > $row [ 'amtsoll' ] ) ? "excess" : "",
					"",
					"",
					( $row [ 'amtist' ] > $row [ 'amtsoll' ] ) ? $row [ 'amtist' ] - $row [ 'amtsoll' ] : "" ) ) );	
				$hulpa= explode ( "|", $row [ 'comment' ]);	
				$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
					"",
					sprintf ("%s  %s", $row ['user_email'], $row [ 'user_contact' ]),
					"",
					"",
					sprintf ("Positie : %s", $hulpa [ 1 ] ?? 0 ),
					"" ) ) );								
			}
		}
		if ( count ( $pdf_data ) > 0 ) $pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
		$pdf_data = array( );
		if ($regelcount >0 ) { 
			$pdf_text .= sprintf ( "\n %s %s\n", $this->language [ 'pdf' ][ 1 ] , $regelcount ) ;
			$pdf->ChapterBody( $pdf_text );
			$pdf_text = "";
			$regelcount = 0;
		}
	}
}
if ( $printOK ) {
	if ( in_array ( 2, $prmode ) ) {

		$regelcount=0;
		$prtype [ ] = 1;
		$levelbreak = "--";
		$pdf_cols = array( 25, 120, 35, 0, 0, 0 ); 
		$pdf_header = array ( "ref", "persoon", "", "", "", "" );
		foreach	( $results as $row ) {
if ($this->setting [ 'debug' ] == "yes") gsm_debug (array ('data' => $row ), __LINE__ . __FUNCTION__ ); 
			/* skip this record */
			$row ['name'] = $this->gsm_sanitizeStrings( $row ['name'], "s{CLEAN}");
			$row ['user_adres'] = $this->gsm_sanitizeStrings( $row ['user_adres'], "s{CLEAN}");
			if ( !in_array ( $row [ 'type' ], $prtype ) ) continue; // niet gevraagd
			if ( $row [ 'type' ] == 2 ) continue; // nog niet geimplementeerd
			if ( $row [ 'type' ] == 3 ) continue; // nog niet geimplementeerd
if ($this->setting [ 'debug' ] == "yes") gsm_debug (array ('data' => $row ), __LINE__ . __FUNCTION__ ); 
			/* nog niet betaald  check */
			$row ['statusopen'] = ( $row[ 'amtsoll'] > $row[ 'amtist'] ) ? true : false;
			if ( $row [ 'type' ] == 1 && !$row ['statusopen'] ) continue; // not paid
if ($this->setting [ 'debug' ] == "yes") gsm_debug (array ('data' => $row ), __LINE__ . __FUNCTION__ . $row ['name']); 

			if  ( $levelbreak != $row [ 'type' ] . $row [ 'ref' ] ) {
				/* afsluiten */
				/* level end calc */
				/* level end printing */
				if ( count ( $pdf_data ) > 0 ) $pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
				$pdf_data = array( );
				/* new one */
				$levelbreak = $row [ 'type' ] . $row [ 'ref' ] ;
				$chaptercount++;
				$pdf->AddPage();
				$pdf->ChapterTitle( $chaptercount, sprintf ( "( %s ) %s %s ", $tekst [ $row [ 'type' ] ] [ 0 ], $tekst [ $row [ 'type' ] ] [ 2 ], $row [ 'ref' ] ) );
			}
			$regelcount++;
			if ( in_array ( $row [ 'type' ], array ( 1  ) ) ) {
				$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
					$row [ 'user_referlist' ],
					$row [ 'name' ],
					$row [ 'datumist' ],
					"",
					"",
					$row [ 'amtsoll' ] ) ) );
				$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
					"",
					$row ['user_adres'],
					( $row [ 'amtist' ] > $row [ 'amtsoll' ] ) ? "excess" : "",
					"",
					"",
					( $row [ 'amtist' ] > $row [ 'amtsoll' ] ) ? $row [ 'amtist' ] - $row [ 'amtsoll' ] : "" ) ) );	
				$hulpa= explode ( "|", $row [ 'comment' ]);	
				$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
					"",
					sprintf ("%s  %s", $row ['user_email'], $row [ 'user_contact' ]),
					"",
					"",
					sprintf ("Positie : %s", $hulpa [ 1 ] ?? 0 ),
					"" ) ) );								
			}
		}
		if ( count ( $pdf_data ) > 0 ) $pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
		$pdf_data = array( );
		if ($regelcount >0 ) { 
			$pdf_text .= sprintf ( "\n %s %s\n", $this->language [ 'pdf' ][ 1 ] , $regelcount ) ;
			$pdf->ChapterBody( $pdf_text );
			$pdf_text = "";
			$regelcount = 0;
		}
	}
}

if ( $printOK ) {
	if ( in_array ( 3, $prmode ) ) {
		$results = array();
		$database->execute_query( 
			$sql2, 
			true, 
			$results);
		$LocalHulp = count ($results);
		$regelcount=0;
		$levelbreak = "--";
		$pdf_cols = array( 25, 120, 35, 0, 0, 0 ); 
		$pdf_header = array ( "ref", "persoon", "", "", "", "" );
		foreach	( $results as $row ) {
			if ($this->setting [ 'debug' ] == "yes") gsm_debug (array ('data' => $row ), __LINE__ . __FUNCTION__ );
			$skip = false;
			/* skip this record */
			if ( $row [ 'dat1' ] > "1970-01-01" && $row [ 'dat1' ] < date ( "Y-m-d" ) ) {
				if ($row[ 'dat2' ] > $row[ 'dat1' ] && $row[ 'dat2' ] < date ( "Y-m-d" ) ) {
					$skip = true;
				} else {
					$member = "lid";
				}
			} else {
				$skip = true;
			}	
			if ( $skip ) continue;
			if ($this->setting [ 'debug' ] == "yes") gsm_debug (array ('data' => $row ), __LINE__ . __FUNCTION__ );
			if  ( $levelbreak != substr ( $row [ 'ref' ], 0, 2) ) {
				if ($this->setting [ 'debug' ] == "yes") gsm_debug (array ('data' => $row ), __LINE__ . __FUNCTION__ );
				/* afsluiten */
				if ( count ( $pdf_data ) > 0 ) $pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
				$pdf_data = array( );
				if  ( $levelbreak != substr ( $row [ 'ref' ], 0, 2) ) {
					/* afsluiten */
					/* level end calc */
					/* level end printing */
					if ( count ( $pdf_data ) > 0 ) 	$pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
					$pdf_data = array( );
					/* new one */
					$chaptercount++;
					$levelbreak = substr ( $row [ 'ref' ], 0, 2);
					$pdf->AddPage();
					$pdf->ChapterTitle ( $chaptercount, sprintf ( "( %s )  %s", substr ( $row [ 'ref' ], 0, 2) , $this->setting [ 'entity' ] [ substr ( $row [ 'ref' ], 0, 2) ] ) );
				}
			}
			$regelcount++;
			$row ['name'] = $this->gsm_sanitizeStrings( $row ['name'], "s{CLEAN}");
			$row ['adres'] = $this->gsm_sanitizeStrings( $row ['adres'], "s{CLEAN}");
			$pdf_data [ ] = explode( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				$row [ 'ref' ],
				str_replace ( "|", " ", $row [ 'name' ] ),
				$member . " : " . $row [ 'dat1' ],
				"",
				"",
				"" ) ) );
			$pdf_data [ ] = explode ( ';', trim ( sprintf( " %s;%s;%s;%s;%s;%s",
				"",
				str_replace ( "|", " ", $row [ 'adres' ] ),
				"",
				"",
				"",
				"" ) ) );
			if (strlen ( $row [ 'contact' ] ) >1 ) $pdf_data [ ] = explode ( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				"",
				$row [ 'contact' ],
				"",
				"",
				"",
				"" ) ) );
			if ( strlen ( $row [ 'email' ] ) > 1 ) $pdf_data [ ] = explode ( ';', trim ( sprintf ( " %s;%s;%s;%s;%s;%s",
				"",
				$row [ 'email' ],
				( $row [ 'type' ] == 1 ) ? "mail onbruikbaar" : "",
				"",
				"",
				"" ) ) );	
		}
		if ( count ( $pdf_data ) > 0 ) $pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
		$pdf_data = array ( );
		if ($regelcount >0 ) { 
			$pdf_text .= sprintf ( "\n %s %s\n", $this->language [ 'pdf' ][ 1 ] , $regelcount ) ;
			$pdf->ChapterBody( $pdf_text );
			$pdf_text = "";
			$regelcount = 0;
		}
	}
}

if ( $partOK ) {
	if ( in_array ( 0, $prmode ) ) {
		$i = 0;
		do {
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				sprintf ( "regel %s veld %s", $i, 1),
				sprintf ( "regel %s veld %s", $i, 2),
				sprintf ( "regel %s veld %s", $i, 3),
				sprintf ( "regel %s veld %s", $i, 4),
				sprintf ( "regel %s veld %s", $i, 5) ) ) );
			$i++;
			if ( intval( $i % 24 ) < 1) {
				$pdf->AddPage();
				$pdf->LabelBlock ( $pdf_data );
				$pdf_data = array( );
			}
		} while ( $i < 60);
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelBlock ( $pdf_data );
			$pdf_data = array( );
		}
		$i = 0;
		do {
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				sprintf ( "regel %s veld %s ", $i, 1),
				sprintf ( "regel %s veld %s", $i, 2),
				sprintf ( "regel %s veld %s", $i, 3),
				sprintf ( "regel %s veld %s", $i, 4),
				sprintf ( "regel %s veld %s", $i, 5) ) ) );
			$i=$i+2;
			if ( intval( $i % 24 ) < 1) {
				$pdf->AddPage();
				$pdf->LabelAll ( $pdf_data );
				$pdf_data = array( );
			}
		} while ( $i < 24);
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelAll ( $pdf_data );
			$pdf_data = array( );
		}
	}
}
if ( $partOK ) {	
	if ( in_array ( 5, $prmode ) ) {
		$i = 0;
		$Tafel = $this-> language [ 'TAFEL' ] [0];
		foreach ( $results as $row ) {
			/*skip als niet betaald */
			if  ( $row [ 'amtsoll'] > $row [ 'amtist'] ) continue;
			/* splits comment */
			$localhulp = explode ( "|" , $row [ 'comment' ] );
			if ( !isset ( $localhulp [ 1 ] ) ) $localhulp [ 1 ] = 0;
			$row [ 'comm' ] = $localhulp [ 1 ];
			/* opmaak naam */
			$row ['name'] = $this->gsm_sanitizeStrings( $row [ 'user_name' ], "s{CLEAN}");
			/* opmaak adres */
			$row ['user_adres'] = $this->gsm_sanitizeStrings( $row ['user_adres'], "s{CLEAN}");
			/*  output */
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				"",
				$row ['name'] ,
				"",
				"",
				($localhulp [ 1 ] != 0) ? sprintf ( "%s %s ", $Tafel , $row [ 'comm' ] ) : ""  ) ) );
			$i++;
			if ( intval( $i % 24 ) < 1) {
				$pdf->AddPage();
				$pdf->LabelBlock ( $pdf_data );
				$pdf_data = array( );
			}
		} 
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelBlock ( $pdf_data );
			$pdf_data = array( );
		}
	}	
}
if ( $partOK ) {	
	if ( in_array ( 6, $prmode ) ) {
		$i = 5;
		$Tafel = $this-> language [ 'TAFEL' ] [0];
				
		foreach ( $results as $row ) {
			/*skip als niet betaald */
			if ($this->setting [ 'debug' ] == "yes") gsm_debug (array ( 'row' => $row ), __LINE__ . __FUNCTION__ ); 
			if  ( $row [ 'amtsoll'] > $row [ 'amtist'] ) continue;
			/* splits comment */
			$localhulp = explode ( "|" , $row [ 'comment' ] );
			if ( !isset ( $localhulp [ 1 ] ) ) $localhulp [ 1 ] = 0;
			$row [ 'comm' ] = $localhulp [ 1 ];
			/* opmaak naam */
			$localhulpB = explode ( "|" , $row [ 'user_name' ] );
			$row [ 'user_name' ] = $this->gsm_sanitizeStrings ( str_replace ( $localhulpB [0], "", $row ['user_name'] ), "s{CLEAN}");
			/* opmaak adres */
			$row ['user_adres'] = $this->gsm_sanitizeStrings( $row ['user_adres'], "s{CLEAN}");
			/* output */
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				"",
				$row ['user_name'] ,
				"",
				"",
				( $row [ 'comm' ] != 0 ) ? sprintf ( "%s %s ", $Tafel , $row [ 'comm' ] ) : ""  ) ) );
			$i=$i+6;
			if ( count ( $pdf_data ) > 3 ) {
				$pdf->AddPage();
				$pdf->LabelBlock ( $pdf_data );
				$pdf_data = array( );
			}
		}
		
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelBlock ( $pdf_data );
			$pdf_data = array( );
		}
	}	
}
if ( $partOK ) {	
	if ( in_array ( 7, $prmode ) ) {
		$i = 3;
		foreach ( $results as $row ) {
			/*skip als niet betaald */
			if  ( $row [ 'amtsoll'] > $row [ 'amtist'] ) continue;
			/* splits comment */
			$localhulpA = explode ( "|" , $row [ 'comment' ] );
			if ( !isset ( $localhulp [ 1 ] ) ) $localhulp [ 1 ] = 0;
			$row [ 'comm' ] = $localhulp [ 1 ];
			/* opmaak naam */
			$localhulpB = explode ( "|" , $row [ 'user_name' ] );
			$row [ 'user_name' ] = $this->gsm_sanitizeStrings ( str_replace ( $localhulpB [0], "", $row ['user_name'] ), "s{CLEAN}");
			/* opmaak adres */
			$row ['user_adres'] = $this->gsm_sanitizeStrings( $row ['user_adres'], "s{CLEAN}");
			/* output */
				$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				$this->setting [ "tafel"] [ "tafel0" ] ?? "",
				$this->setting [ "tafel"] [ "tafel1" ] ?? "",
				$this->setting [ "tafel"] [ "tafel2" ] ?? "",
				$this->setting [ "tafel"] [ "tafel3" ] ?? "",
				$this->setting [ "tafel"] [ "tafel4" ] ?? "") ) );
			$i=$i+2;
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				$this->setting [ "tafel"] [ "tafel5" ] ?? "",
				$this->setting [ "tafel"] [ "tafel6" ] ?? "",
				$this->setting [ "tafel"] [ "tafel7" ] ?? "",
				$this->setting [ "tafel"] [ "tafel8" ] ?? "",
				$this->setting [ "tafel"] [ "tafel9" ] ?? "") ) );
			$i=$i+4;
			if ( count ( $pdf_data ) > 7 ) {
				$pdf->AddPage();
				$pdf->LabelCenter ( $pdf_data );
				$pdf_data = array( );
			}
		}
		
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelCenter ( $pdf_data );
			$pdf_data = array( );
		}
	}	
}
if ( $partOK ) {	
	if ( in_array ( 8, $prmode ) ) {
		$i = 3;
		$n = 1;
		do {
			/* output */
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				"",
				sprintf ( "Tafel  %s", $n) ,
				"",
				"",
				"" ) ) );
			$i=$i+2;
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				"",
				sprintf ( "Tafel  %s", $n) ,
				"",
				"",
				"" ) ) );
			$n++;
			$i=$i+4;
			if ( count ( $pdf_data ) > 7 ) {
				$pdf->AddPage();
				$pdf->LabelGroot ( $pdf_data );
				$pdf_data = array( );
			}
		} while ( $n < 9);
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelGroot ( $pdf_data );
			$pdf_data = array( );
		}
	}	
}

if ( $partOK ) {	
	if ( in_array ( 9, $prmode ) ) {
		$i = 0;
		$n = 0;
		$payload = "";
		$title = $this->setting [ "tafel"] [ "tafel10" ]  ?? "";
		foreach ( $results as $row ) {
			/*skip als niet betaald */
			if  ( $row [ 'amtsoll'] > $row [ 'amtist'] ) continue;
			/* splits comment */
			$localhulpA = explode ( "|" , $row [ 'comment' ] );
			if ( !isset ( $localhulp [ 1 ] ) ) $localhulp [ 1 ] = 0;
			$row [ 'comm' ] = $localhulp [ 1 ];
			/* opmaak naam */
			$localhulpB = explode ( "|" , $row [ 'user_name' ] );
			$row [ 'user_name' ] = $this->gsm_sanitizeStrings ( str_replace ( $localhulpB [0], "", $row ['user_name'] ), "s{CLEAN}");
			/* opmaak adres */
			$row ['user_adres'] = $this->gsm_sanitizeStrings( $row ['user_adres'], "s{CLEAN}");
			/* output */
			$payload .= sprintf ( "\n %s %s %s", $row ['id'], $row [ 'user_referlist' ], $row ['user_name'] );
			$n++;
			if (  $n  > 23 && strlen ( $payload ) > 20 ) {
				$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				"",
				$title,
				$payload,
				"",
				""  ) ) );
				$pdf->AddPage();
				$pdf->LabelAll ( $pdf_data );
				$pdf_data = array( );
				$payload = "";
				$n = 0;
			}
		}
		
		if ( strlen ( $payload ) > 20 ) {
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				"",
				$title,
				$payload,
				"",
				""  ) ) );
			$pdf->AddPage();
			$pdf->LabelAll ( $pdf_data );
			$pdf_data = array( );
		}
	}	
}

if ( $partOK ) {	
	if ( in_array ( 10, $prmode ) ) {
		$i = 0;
		foreach ( $results as $row ) {
			/* opmaak naam */
			$hulp1= explode ("|" ,$row [ 'user_name' ] ) ;
			$row ['name'] = $this->gsm_sanitizeStrings( $row [ 'user_name' ], "s{CLEAN}");
			$voornaam = $hulp1 [ 1 ] ?? $row ['name']; 
			/* opmaak adres */
			$hulp2= explode ("|" ,$row [ 'user_adres' ] ) ;
			$row ['user_adres'] = $this->gsm_sanitizeStrings( $row ['user_adres'], "s{CLEAN}");
			$bedrijf = $hulp2 [ 0 ];
			/*  output */
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				"",
				$voornaam,
				"",
				"",
				$bedrijf ) ) );
			$i++;
			if ( intval( $i % 24 ) < 1) {
				$pdf->AddPage();
				$pdf->LabelVisit ( $pdf_data );
				$pdf_data = array( );
			}
		} 
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelVisit ( $pdf_data );
			$pdf_data = array( );
		}
	}	
}

if ( $partOK ) {	
	if ( in_array ( 11, $prmode ) ) {
		$i = 0;
		foreach ( $results as $row ) {
			/* opmaak naam */
			$hulp1= explode ("|" ,$row [ 'user_name' ] ) ;
			$row ['name'] = $this->gsm_sanitizeStrings( $row [ 'user_name' ], "s{CLEAN}");
			$voornaam = $hulp1 [ 1 ] ?? $row ['name']; 
			/* opmaak adres */
			$hulp2= explode ("|" ,$row [ 'user_adres' ] ) ;
			$row ['user_adres'] = $this->gsm_sanitizeStrings( $row ['user_adres'], "s{CLEAN}");
			$bedrijf = $hulp2 [ 0 ];
			/*  output */
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				"",
				$voornaam,
				"",
				"",
				$bedrijf ) ) );
			$i++;
			if ( intval( $i % 2 ) < 1) {
				$pdf->AddPage();
				$pdf->LabelPresent ( $pdf_data );
				$pdf_data = array( );
			}
		} 
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelPresent ( $pdf_data );
			$pdf_data = array( );
		}
	}	
}

if ( $partOK ) {	
	if ( in_array ( 12, $prmode ) ) {

		$i = 0;
		foreach ( $results as $row ) {
			/* opmaak naam */
			$hulp1= explode ("|" ,$row [ 'user_name' ] ) ;
			$row ['name'] = $this->gsm_sanitizeStrings( $row [ 'user_name' ], "s{CLEAN}");
			$voornaam = $hulp1 [ 1 ] ?? $row ['name']; 
			/* opmaak adres */
			$hulp2= explode ("|" ,$row [ 'user_adres' ] ) ;
			$row ['user_adres'] = $this->gsm_sanitizeStrings( $row ['user_adres'], "s{CLEAN}");
			$bedrijf = $hulp2 [ 0 ];
			/*  output */
			
			$pdf_data [ ] = explode ( ';', trim ( sprintf( "%s;%s;%s;%s;%s;%s",
				$i,
				$bedrijf,
				$row ['name'],
				$this->setting [ "tafel"] [ "tafel2" ] ?? "",
				$this->setting [ "tafel"] [ "tafel3" ] ?? "",
				$this->setting [ "tafel"] [ "tafel4" ] ?? "") ) );
			$i++;
			if ( intval( $i % 2 ) < 1) {
				$pdf->AddPage();
				$pdf->LabelPresent ( $pdf_data );
				$pdf_data = array( );
			}
		} 
		if ( count ( $pdf_data ) > 0 ) {
			$pdf->AddPage();
			$pdf->LabelPresent ( $pdf_data );
			$pdf_data = array( );
		}
	}	
}

?>
