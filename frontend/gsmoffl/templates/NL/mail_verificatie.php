<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2023 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* change history
 * 20230702 lepton 7.0
 */
 
    $mail_subject = 'verificatie aanvullingen / registratie gegevens';
    $mail_content = 
	'Hallo {GSM_NAME},
	<br/><br/>U heeft {WEB_TIMESTAMP} aanvullingen / registratie gegevens aangepast. 
	<br/><br/>Hieronder vindt u de verificatie code die nodig is om de verstrekte gegevens in het systeem te verwerken:
	<br/>{GSM_LINK}<br/><br/><br/>Deze verificatie code is slechts een beperkte tijd bruikbaar. Indien u geen aanvullingen / registratie gegevens verstrekt heeft op {WEB_SITE} heeft negeer dan deze mail. 
	<br/><br/>Met vriendelijke groet, <i> {WEB_MASTER} <br />e-mail {WEB_EMAIL}</i>';
?>