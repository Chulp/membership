<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2023 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

$FC_SET [ 'version' ] 		= "frontend v2024808";

$FC_SET [ 'SET_function' ] 	= 'setupl|stand'; 		// backend menu ';
$FC_SET [ 'SET_menu' ] 		= 'pass'; 				// frontend menu ';

/* for the administrator and the editor */
if ( isset ($_SESSION [ 'GROUPS_ID' ] ) && ( $_SESSION [ 'GROUPS_ID' ] == 1 || 	$_SESSION [ 'GROUPS_ID' ] == 4 ) )  {
/* debug */		$FC_SET [ 'SET_menu' ] 		= 'pass|partner|mail|stand|leden|tafel';   /* debug */
/* debug */		$FC_SET [ 'SET_function' ] 	= 'setupl|mail|stand|leden'; /* debug */
}

/* on the screen there may appear a module name to select. */
$FC_SET [ 'SET_txt_menu' ] [ 'xsetupl' ]	= 'Instellingen';
$FC_SET [ 'SET_txt_menu' ] [ 'xmail' ]		= 'Mailing';
$FC_SET [ 'SET_txt_menu' ] [ 'xstand' ]		= 'Standen';
$FC_SET [ 'SET_txt_menu' ] [ 'xleden' ]		= 'Leden';

$FC_SET [ 'SET_txt_menu' ] [ 'vpass' ]		= 'Login';
$FC_SET [ 'SET_txt_menu' ] [ 'vpartner' ]	= 'Bezoekers';
$FC_SET [ 'SET_txt_menu' ] [ 'vmail' ]		= 'Mailing';
$FC_SET [ 'SET_txt_menu' ] [ 'vstand' ]		= 'Standen';
$FC_SET [ 'SET_txt_menu' ] [ 'vleden' ]		= 'Leden';
$FC_SET [ 'SET_txt_menu' ] [ 'vtafel' ]		= 'Tafel';

$FC_SET [ 'SET_txt_menu' ] [ 'xdummy' ]		= '-dummy-';
$FC_SET [ 'SET_txt_menu' ] [ 'vdummy' ]		= '-dummy-';

?>
