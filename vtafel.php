<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* module id*/ 
$module_name = 'vtafel';
$version='20241025';
$project = "tafel";
$main_file="adres";
$sub_file="standen";
$default_template = '/tafel.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffl::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;
// $project= sprintf ("%s %s " , $oFC-> language [ 'TXT_SETUP' ], strtoupper ( $main_file )) ;
$project= "Bijdrage overzichten";

/* file references */
$oFC->file_ref [ 99 ] = LOAD_DBBASE . "_".$main_file;
$oFC->file_ref [ 98 ] = LOAD_DBBASE . "_" .$sub_file;
$oFC->file_ref [ 97 ] = TABLE_PREFIX . 'users';

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "tafel");
/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;
// $oFC->setting [ 'debug' ] = "yes";
/* Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}		

/* extra */
$oFC->page_content  [ 'GROUP' ] =  ( isset( $_POST [ 'P2' ] ) ) ? $_POST[ 'P2' ] : 0;
$oFC->page_content  [ 'LINK1' ] = 'stand';
$oFC->page_content  [ 'LINK2' ] =  (LOAD_MODE == "x" ) ? "setupl" : 'partner';

/* spif test */
$_SESSION[ 'page_h' ] = $oFC->page_content [ 'HASH' ]; 

/* get memory values */
$oFC->gsm_memorySaved ( );


if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ($_POST, $_GET ?? "", $oFC , $selection ), __LINE__ . __FUNCTION__ );  

$match = isset ( $_POST [ 'vink' ] ) 
	? $_POST [ 'vink' ] 
	: array ();
$check = ( count ( $match ) > 0 ) 
	? true 
	: false; 
/* when absent neglect them */

/* selection1 */
if (isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$selection = strtolower ( $selection );
	foreach ( array( "printn" => "", 
			"print1" => "print1", 
			"print2" => "print2", 
			"print3" => "print3", 
			"print" => "print" , 
			"partn" => "", 
			"part9" => "part9", 
			"part1" => "part1", 
			"part2" => "part2", 
			"part3" => "part3" , 
			"part4" => "part4" , 
			"part5" => "part5" ,
			"part6" => "part6" ,
			"part7" => "part7" ,
			"part8" => "part8" ,
			"all" => "all") as $pay => $load ) {
		if ( strstr ( $selection, $pay ) ) {
			$xmode .= $load; 
			$selection = trim ( str_replace ( $pay, "", strtolower ( $selection ) ) );
}	}	}

/* selection2 */
$oFC->search_mysql = "";
if ( strstr ( $xmode, "part") ) {
	$oFC->setting ['pdf_filename'] = $oFC->gsm_sanitizeStringS ( "Overzicht_" . $selection . ".pdf" , "s{FILE}" );
} else { 
	if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
		$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
		$oFC->search_mysql .= " WHERE `" . $oFC->file_ref[ 98 ] . "`.`zoek` LIKE '" . $help . "'";
	} else { 
		$selection = "";
	}
}
$oFC->page_content  [ 'PARAMETER' ] = trim( $selection );
$oFC->page_content  [ 'SUB_HEADER' ]= strtoupper ( $oFC->page_content [ 'PARAMETER' ] );


/* Print function */
if ( strstr ( $xmode, "print") ) $oFC->setting ['pdf_filename'] = $oFC->gsm_sanitizeStringS ( "Overzicht_stand_" .  $oFC->page_content  [ 'PARAMETER' ] . ".pdf" , "s{FILE}" );
if ( strstr ( $xmode, "part") ) $oFC->setting ['pdf_filename'] = $oFC->gsm_sanitizeStringS ( "Overzicht_" .  $oFC->page_content  [ 'PARAMETER' ] . ".pdf" , "s{FILE}" );

/* sips test before job */ 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= $oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

/* some job to do */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "up":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("up", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ],  $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "down":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("down", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ], $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "Save":
			// VINK en bijbehorende datum
			if ( isset ( $_POST [ 'gsmc_B' ] ) ) {
				foreach ($_POST [ 'gsmc_B' ] as $pay => $load ) {
					$payload = explode ("|", $load);
					$updateArr = array();
					$updateArr [ 'amtist' ] = $payload [ 1 ];
					$hulp = sprintf ( 'gsmc_A|%s|0000-00-00', $payload [ 0 ]);
					if (isset ($_POST [ $hulp ])) {
						$updateArr [ 'datumist' ] = $_POST [ $hulp ];	
						unset ( $_POST [ $hulp ] );
					} else {
						$updateArr [ 'datumist' ] = $oFC->page_content  [ 'DATE' ];
					}
					$database->build_and_execute(
						"update",
						$oFC->file_ref [ 98 ],
						$updateArr,
						"`id` = '" . $payload [ 0 ] . "'");
				}
			}
			//  wijziging
			foreach ($_POST as $pay => $load ) {
				if (substr( $pay, 0, 5) == "gsmc_") {
					switch ( substr ( $pay, 5, 1) ) {
						case "A": //Aanpassing ist date
							$payload = explode ("|", $pay);
							if ( isset ($payload [ 2 ] ) && $payload [ 2 ] > "1970-01-01" && $payload [ 2 ] != $load ) {
								$updateArr = array();
								$updateArr [ 'datumist' ] = $load;
								$database->build_and_execute(
									"update",
									$oFC->file_ref [ 98 ],
									$updateArr,
									"`id` = '" . $payload [ 1 ] . "'");
							}
							break;
						case "C": //Aanpassing comment
							$payload = explode ("|", $pay);
							$payload [ 2 ] = ( isset ( $payload [ 2 ] ) ) ? $payload [ 2 ] : 0;
							if ( $load == $payload [ 2 ]  ) break;
							$query = "SELECT `" . $oFC->file_ref [ 98 ] . "`.`id`,
								`" . $oFC->file_ref [ 98 ] . "`.`comment`,
								`" . $oFC->file_ref [ 99 ] . "`.`name` as `user_name`
								FROM `" . $oFC->file_ref [ 98 ] . "` LEFT JOIN `" . $oFC->file_ref [ 99 ] . "`
								ON `" . $oFC->file_ref[ 99 ] . "`.`id` = `" . $oFC->file_ref [ 98 ] . "`.`adresid`";
							$query .= " WHERE `" . $oFC->file_ref[ 98 ] . "`.`id` = " .$payload [ 1 ]; 
							// output
							$een_result = array();
							$database->execute_query( $query, true, $een_result);
							if ( strlen ($load) != 0 ) Gsm_debug (array ($query, $een_result, $pay, $payload, $load, strlen ($load) ), __LINE__ . __FUNCTION__ );  
							$row= current ( $een_result );
							$LocalhulpA = $row ['comment'];
							$LocalhulpB = explode ( "|" , $LocalhulpA );
							$row ['comm'] = $load ;
							$LocalhulpC = explode ( "|" , $row [ 'user_name' ]);
							if (!isset ($LocalhulpC [ 3 ] )) $LocalhulpC [ 3 ]  = "xxxx";
							$row ['comment'] = sprintf ( '%s|%s' , $LocalhulpC [ 3 ]  , $row ['comm'] );
							$updateArr = array();
							$updateArr [ 'comment' ] = $row [ 'comment' ];
							$database->build_and_execute(
								"update",
								$oFC->file_ref [ 98 ],
								$updateArr,
								"`id` = '" . $payload [ 1 ] . "'");
							break;
					}
				}
			}
			break;

		case "Reset":
			if ( isset( $POST ) ) unset($_POST);
			$oFC->page_content  [ 'GROUP' ] = 0 ;
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} else { 
	// so standard display / first run
}

// at this point we are preparing the printout
 if ( isset ( $oFC->setting [ 'pdf_filename' ] )  && strlen( $oFC->setting [ 'pdf_filename' ] ) > 5 ) {
	if ( strstr ( $xmode, "part" )) {
		$pdflink = $oFC->gsm_print ( $oFC->page_content  [ 'PARAMETER' ] , $project, $xmode , 5 );
		$oFC->page_content  [ 'PARAMETER' ] ="";
	} else {		
		$pdflink = $oFC->gsm_print ( $oFC->page_content  [ 'PARAMETER' ] , $project, $xmode , 1 );
	}
}

switch ( $oFC->page_content [ 'MODE' ] ) {
	case 9: // default list 
		$pageok = true;
		$result = array ( );
		$sql1 = "SELECT count(`id`) FROM `" . $oFC->file_ref [ 98 ] . "` WHERE `active` = '1'  AND `type` = '1'";
		if (strlen ( $oFC->search_mysql ) > 10 ) $sql1 = str_replace ( "WHERE" , $oFC->search_mysql. " AND ", $sql1);
		$database->execute_query( $sql1, 
			true, 
			$result);
		$row = current ( $result );
		$oFC->page_content [ 'aantal' ] = $row [ "count(`id`)" ];
			
		/* paging / accordeon /  make records unique / restore zoek/sort field */
		$limit_sql = $oFC->gsm_pagePosition ("sql", $oFC->page_content [ 'POSITION' ] , $oFC->page_content [ 'aantal' ], $oFC->setting [ 'qty_max' ]  );
		
		$query = "SELECT `" . $oFC->file_ref [ 98 ] . "`.*,
			`" . $oFC->file_ref [ 99 ] . "`.`id` as `user_id`,
			`" . $oFC->file_ref [ 99 ] . "`.`name` as `user_name`,
			`" . $oFC->file_ref [ 99 ] . "`.`adres` as `user_adres`,
			`" . $oFC->file_ref [ 99 ] . "`.`ref` as `user_referlist`,
			`" . $oFC->file_ref [ 99 ] . "`.`ref1` as `user_ref1`,
			`" . $oFC->file_ref [ 99 ] . "`.`email` as `user_email`
			FROM `" . $oFC->file_ref [ 98 ] . "` LEFT JOIN `" . $oFC->file_ref [ 99 ] . "`
			ON `" . $oFC->file_ref[ 99 ] . "`.`id` = `" . $oFC->file_ref [ 98 ] . "`.`adresid`";
		$query .= " WHERE `" . $oFC->file_ref[ 98 ] . "`.`active` = '1' AND `" . $oFC->file_ref[ 98 ] . "`.`type` = '1'";
		$query .= " ORDER BY `" . $oFC->file_ref[ 98 ] . "`.`type` ASC, 
			`" . $oFC->file_ref[ 98 ] . "`.`ref` DESC, 
			`" . $oFC->file_ref[ 98 ] . "`.`comment` ASC"; 

		// output
		$result = array();
		$displayOk = true;
		$database->execute_query( $query. $limit_sql, true, $result);
		if ( count($result) > 0){
			$PL0 = "=="; //ref
			$PLT = "="; //ref
			$cal = array ();
			$job = array ();
			foreach ($result as $row) {
				if ( $row[ 'active' ] == 1 ) {
					/* wissel */
					if ( $PL0 != $row [ 'type' ] . $row [ 'ref' ] ) {
						if (count ( $cal ) > 0 ) {
							$displayOk = true;
							if ( $displayOk ) $oFC->page_content  [ 'standen'][ ] = $cal;
							$cal = array();
						}
					}
					$hulpArr = array( );
					$PLT = $row [ 'type' ];
					$PL0 = $row [ 'type' ].$row [ 'ref' ];
					
					/* fill comment field */
					$LocalhulpA = $row ['comment'];
					$LocalhulpB = explode ( "|" , $LocalhulpA );
					$row ['comm'] = $LocalhulpB [1] ?? "0" ;
					$LocalhulpC = explode ( "|" , $row [ 'user_name' ]);
					if (!isset ($LocalhulpC [ 3 ] )) $LocalhulpC [ 3 ]  = "xxxx";
					$row ['comment'] = sprintf ( '%s|%s' , $LocalhulpC [ 3 ]  , $row ['comm'] );
					if ( $row [ 'comment' ] != $LocalhulpA ) $hulpArr [ 'comment' ] = $row [ 'comment' ];
					/* end fill comment field */
					
					$row ['name'] = $oFC->gsm_sanitizeStrings( $row ['name'], "s{CLEAN}");
					$row ['user_adres'] = $oFC->gsm_sanitizeStrings( $row ['user_adres'], "s{CLEAN}");
					$row ['statusopen'] = ( $row[ 'amtsoll'] > $row[ 'amtist'] ) ? true : false;
					$cal []= $row;
					if ( count ( $hulpArr ) ) $job [ ] = "UPDATE `" . $oFC->file_ref [ 98 ] . "` SET " . $oFC->gsm_accessSql( $hulpArr, 2 ) . " WHERE `id` = '" . $row['id'] . "'"; 
				}
			}
			if ( count ( $job ) ) {
				$m = count ( $job );
				foreach( $job as $query) $database->simple_query( $query);
				$oFC->description .= date('G:i:s'.substr((string)microtime(), 1, 8).' : ').'Standen records altered '.$m .NL;
			} 
			if (count ( $cal ) > 0 ) {
				$displayOk = true;
				if ( $displayOk ) $oFC->page_content  [ 'standen'][ ] = $cal;
			}
			
			
			
			
			
		} else {
			$oFC->description .= $oFC->language['TXT_ERROR_DATA'];
		}
		/* get selected */
		if ($oFC->page_content  [ 'GROUP' ] > 0 ) {
			$standArr = array ();
			$database->execute_query (
				"SELECT * FROM `" . $oFC->file_ref [ 98 ] . "` WHERE `id` = '".$oFC->page_content  [ "GROUP" ]."'",
				true, 
				$standArr,
				false ); 
			$standArr [ "ACTIVE" ] = $oFC->gsm_SelectOption ( $oFC->language [ 'active' ], $standArr [ "active" ] );
			$standArr [ "STANDTYPE" ] =  $oFC->gsm_SelectOption (  $oFC->language [ 'standen_type' ], $standArr [ "standtype" ] );
			/* for checks Futher we need refer and referlist  */
			$oFC->page_content  [ 'GROEP1' ] = $standArr;
			/* for checks Futher we need refer and referlist  */
			/* get referlist */
			$standArr = array ();
			$database->execute_query (
				"SELECT `referlist`, `name` FROM `" . $oFC->file_ref [ 99 ] . "` WHERE `id` = '".$oFC->page_content  [ 'GROEP1' ] [ 'adresid' ]."'",
				true, 
				$standArr,
				false ); 
			$oFC->page_content  [ 'GROEP1' ] [ 'referlist' ] = substr ( $standArr [ 'referlist' ], 0, 2 );	
			/* welke hebben het al*/
			$standArr = array ();
			$database->execute_query (
				"SELECT `adresid` FROM `" . $oFC->file_ref [ 98 ] . "` WHERE `refer` = '".$oFC->page_content  [ 'GROEP1' ] [ 'refer' ]."'",
				true, 
				$standArr ); 
			foreach ( $standArr as $row ) $exist_id [] = $row [ 'adresid' ] ;
			//create SQL
			$fields = array( "id", "name", "referlist", "contact", "note", "comp", "email", "adres", "dat1", "dat2", "ref0" );
			$query = "SELECT `" . implode ("`, `", $fields) . "` FROM `" . $oFC->file_ref[ 99 ] . "`";
			$query .= " WHERE `" . $oFC->file_ref[ 99 ] . "`.`id` >= '1' ";
			$query .= " AND `" . $oFC->file_ref[ 99 ] . "`.`dat1` > '1970-01-01' ";
			$query .= " ORDER BY  `" . $oFC->file_ref[ 99 ] . "`.`referlist`, `" . $oFC->file_ref[ 99 ] . "`.`email`";
			$cal = array();
			$results = array ()	;	
			$database->execute_query (
				$query,
				true, 
				$results,
				true); 
			foreach ( $results as $row ) {
				$row [ 'text' ] = "";
				$row [ 'keuze' ] = "sel";
				$row [ 'extra' ] = "";
				$row [ 'NOTE' ] = $oFC->language [ 'TXT_NOTE' ] [ $row [ 'note' ] ];
				$row [ 'name' ]	= $oFC->gsm_sanitizeStringS ( $row [ 'name' ], "s{CLEAN}" );
				$row [ 'adres' ] = $oFC->gsm_sanitizeStringS ( $row [ 'adres' ], "s{CLEAN}" );
				if ( $row [ 'dat1' ] > "1970-01-01" && $row [ 'dat1' ] < $oFC->page_content  [ 'DATE' ] ) {
					if ( $row [ 'dat1' ] > $row [ 'dat2' ] ) { 
						$row [ 'keuze' ] = "sel"; 
						$row [ 'text' ] = "lid /"; 
						$row [ 'extra' ] = sprintf ( "Lid vanaf: %s", $row ['dat1']); 
					} elseif ( $row [ 'dat2' ] < $oFC->page_content  [ 'DATE' ]) {
						$row [ 'keuze' ] = "not"; 
						$row [ 'text' ] = "ex-lid /"; 
						$row [ 'extra' ] = sprintf ( "Lid vanaf: %s  tot ", $row ['dat1'], $row ['dat2']);
					} else {
						$row [ 'keuze' ] = "sel"; 
						$row [ 'text' ] = "lid /"; 
						$row [ 'extra' ] = sprintf ( "Lid vanaf: %s  tot ", $row ['dat1'], $row ['dat2']);
				}	}
				if ( $row [ 'keuze'] != 'not' && substr ( $row [ 'referlist'], 0, 2) != $oFC->page_content  [ 'GROEP1' ] [ 'referlist' ]) $row [ 'keuze'] = 'des';
				if ( in_array ( $row [ 'id' ] , $exist_id ) ) $row [ 'keuze'] = 'oke';
				if ( $row [ 'keuze'] != 'oke' && $check) {
					if ( $check && in_array ( $row [ 'id' ] , $match ) ) { $row [ 'keuze'] = 'sel'; } else { $row [ 'keuze'] = 'des'; }
				}	
				if (isset ($row [ 'keuze'] ) ) $cal [] = $row;
			}	
			if (count ( $cal ) > 0 ) $oFC->page_content  [ 'GROEP2'] = $cal;
		}
		break;
}

switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		$oFC->page_content  [ 'SELECTION' ] = "";
		break;
	case 9:
	default: 
		$oFC->page_content  [ 'SELECTIONA' ] = $oFC->gsm_opmaakSel2 ( array ( 10 ), 
			( strlen ( $oFC->page_content  [ 'PARAMETER' ] ) >1 ) ?  $oFC->page_content  [ 'PARAMETER' ] : "-", 
			"-" ,
			"0", 
			"-", 
			"-" , 
			"printn, partn, all" );
		if ( $oFC->page_content [ 'aantal' ] > $oFC->setting [ 'qty_max' ] ) 
			$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel2 ( array ( 13), "-", "-", $oFC->page_content [ 'POSITION' ], $oFC->page_content [ 'aantal' ] , $oFC->setting [ 'qty_max' ]  );
		$oFC->page_content  [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel2 ( array ( 2,  6,  11) , '-', $pdflink ?? "-" );
		$oFC->page_content  [ 'SELECTIOND' ] = $oFC->gsm_opmaakSel2 ( array ( 7, 16 ) );
		$oFC->page_content  [ 'SELECTIONE' ] = $oFC->gsm_opmaakSel2 ( array ( 6, 9 ) );		
		break;
}
/* end selection options *

/* memory save * /
$oFC->page_content  [ 'MEMORY' ] = $oFC->gsm_memorySaved ( 2 ); 
/* end memory save */
	
/* output processing */
// als er boodschappen zijn deze tonen in een error blok
$oFC->page_content [ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content [ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content ['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content [ 'VERSIE' ] = $oFC->version; 
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: 
		if ( isset( $oFC->page_content [ "GSM_ID" ] ) ) $oFC->page_content [ "RECID" ] = $oFC->page_content [ "GSM_ID" ];
		break;
} 

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>