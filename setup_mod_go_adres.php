<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php


/* 1 The table involved */
$oFC->version [ 'setup_mod_go_adres' ] = "20240912"; 
$product = "adres";
$oFC->file_ref [ 0 ] = LOAD_DBBASE . "_" . $product;
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Create / Upgrade function started ' . $oFC->file_ref [ 0 ] .  NL; 

/* 1B database creation if database does not exist */
$oFC->description .= $oFC->gsm_existDb ( $oFC->file_ref  [ 0 ] );

/* 1C Modifications needed will be stored at this place */
$job = array ();
	
/* 1D which fields are present in the main file */	
$result = array ( );
$database->execute_query ( sprintf ( 'DESCRIBE %s', $oFC->file_ref [ 0 ] ),
	true, 
	$result );
if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ($result ), __LINE__ . $oFC->file_ref [ 0 ] );
	
/* 1E_add /change fields not present  */
$localHulpA = array();
foreach ( $result as $row ) $localHulpA [$row [ 'Field' ]] = $row [ 'Type' ];

/* 1F wijzigen */
if ( isset ( $localHulpA[ 'info' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `info` `content_short` text", $oFC->file_ref [ 0 ] );
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `content_short` `content_short`  VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT ''", $oFC->file_ref [ 0 ] );
	$localHulpA[ 'content_short' ] = true;
}	
if ( isset ( $localHulpA[ 'aant' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `aant` `content_long` text", $oFC->file_ref [ 0 ] );
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `content_long` `content_long`  VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT ''", $oFC->file_ref [ 0 ] );
	$localHulpA[ 'content_long' ] = true;
}
if ( isset ( $localHulpA[ 'note' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `note` `type` int(16) NOT NULL DEFAULT '0'", $oFC->file_ref [ 0 ] );
	$localHulpA[ 'type' ] = true;
}	
if ( isset ( $localHulpA[ 'referlist' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `referlist` `ref` varchar(16) NOT NULL DEFAULT ''", $oFC->file_ref [ 0 ] );
	$localHulpA [ 'ref' ] = true;
}
	
/* 1G toevoegen */
if ( !isset ( $localHulpA[ 'content_long' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_long` varchar(255) NOT NULL DEFAULT '' AFTER `content_short`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'ref2' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `ref2` varchar(255) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'ref1' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `ref1` varchar(255) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'ref0' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `ref0` varchar(255) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'dat2' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `dat2` date NOT NULL DEFAULT '0000-00-00' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'dat1' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `dat1` date NOT NULL DEFAULT '0000-00-00' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'dat0' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `dat0` date NOT NULL DEFAULT '0000-00-00' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'contact' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `contact` varchar(63) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'adres' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `adres` varchar(255) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'email' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `email` varchar(63) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'comp' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `comp` int(11) NOT NULL DEFAULT '0' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'adresid' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `adresid` int(11) NOT NULL DEFAULT '0' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'active' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `active` int(3) NOT NULL DEFAULT '1' AFTER `zoek`", $oFC->file_ref [ 0 ] );	}

/* 1H achtual upgrade */
if ( isset ( $job ) && count( $job ) > 0 ) {
	foreach( $job as $key => $query ) {
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
		$database->simple_query ( $query );
}	}
/* 1I upgraded */

/* 1J other entries needed ? */
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default settings checked' .  NL; 
$job = array ();

/* check if upload set for file types is present * /
$results = array ( );
$database->execute_query ( 
	sprintf ( "SELECT * FROM `%smod_go_taxonomy` WHERE `type` = 'setting' AND `ref` = '%s'", 
		TABLE_PREFIX, "allowed|gsmoffl|2" ) , 
	true, 
	$results );
if ( count ( $results ) == 0 ) { 
	$main_parameter = "jpg|html|zip|pdf|mp4";
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		TABLE_PREFIX , "allowed|gsmoffl|2", $main_parameter );
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' setting allowed  added  ' . NL;
	$oFC->setting [ 'allowed' ] = $main_parameter;
}
/* end */

if ( !isset ( $oFC->setting [ 'remove' ] ) ) {
	$main_parameter = 'recycle';
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		TABLE_PREFIX, 'remove', $main_parameter);
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' remove keyword setting added ' . $main_parameter . NL;
	$oFC->setting [ 'remove' ] = $main_parameter;
}

if ( !isset ( $oFC->setting [ 'mail' ]  ) ) {
	$main_parameter = 'mail';
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		TABLE_PREFIX, 'mail|gsmoffl' ,$main_parameter );
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' mail  setting added  ' . $main_parameter . NL;
	$oFC->setting [ 'mail' ] = $main_parameter;
}

if ( !isset ( $oFC->setting [ 'max_mail' ]  ) ) {
	$main_parameter = '200';
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		TABLE_PREFIX, 'max_mail|gsmoffl' ,$main_parameter );
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' max_mail  setting added  ' . $main_parameter . NL;
	$oFC->setting [ 'mail' ] = $main_parameter;
}

if (!isset ( $oFC->setting [ 'mediadir' ] ) ) {
	$main_parameter = '/media/leden';
	$job [] = sprintf ( "INSERT INTO  `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		TABLE_PREFIX, 'mediadir|gsmoffl', $main_parameter );
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'mediadir' . NL;
	$oFC->setting [ 'mediadir' ] = $main_parameter;
}

if (!isset ( $oFC->setting [ 'collectdir' ] ) ) {
	$main_parameter = '/media/logging';
	$job [] = sprintf ( "INSERT INTO  `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		TABLE_PREFIX, 'collectdir|gsmoffl', $main_parameter );
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' directory setting ' . 'mediadir' . NL;
	$oFC->setting [ 'collectdir' ] = $main_parameter;
}

if ( !isset ( $oFC->setting [ 'zoek' ] [ $product ] ) ) {
	$main_parameter = '|ref|id|adresid|name|email|';
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('zoek', '%s', '%s', '1' )",
		TABLE_PREFIX, $product, $main_parameter);
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' zoek schema toevoegd ' . $product . NL;
	$oFC->setting [ 'zoek' ] [ $product ] = $main_parameter;
}

if ( !isset ( $oFC->setting [ 'display' ]  ) ) {
	$main_parameter = "cont-curs-meld-stand-data-file";
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		TABLE_PREFIX, 'display',$main_parameter );
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' display setting  added  ' . $main_parameter . NL;
	$oFC->setting [ 'display' ] = $main_parameter;
}

/* 1K achtual upgrade */
if ( isset ( $job ) && count( $job ) > 0 ) {
	foreach( $job as $key => $query ) {
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
		$database->simple_query ( $query ); 
	} 
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default setting added' .  NL; 
}
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function completed ' . $oFC->file_ref [ 0 ] .  NL; 
?> 