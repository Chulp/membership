<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* module id*/ 
$module_name = 'vstand';
$version='20250210';
$main_file="standen";
$sub_file="adres";
$default_template = '/standen.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffl::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;
$project= sprintf ("%s %s " , "Overzicht " , $main_file ) ;

/* file references */
$oFC->file_ref [ 99 ] = LOAD_DBBASE . "_".$main_file;
$oFC->file_ref [ 98 ] = LOAD_DBBASE . "_" .$sub_file;
$oFC->file_ref [ 97 ] = TABLE_PREFIX . 'users';

/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet");

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* Gebruik limited door rechthebbenden */
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $oFC->page_content [ 'PAGE_ID' ], $oFC->setting [ 'owner' ] ) );
	if ($oFC->user[ 'privileged' ] > 2 ) {
		$oFC->page_content [ 'MODE' ] = 9;
		$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
	}
	/* niet genoeg rechten * /
	if ( $oFC->page_content [ 'MODE' ] == 0 ) {
		$oFC->description = $oFC->language [ 'TXT_NO_ACCESS' ];
		unset ( $_POST[ 'command' ] );
	}
	/* end genoeg rechten */
}

/* spif test */
$_SESSION[ 'page_h' ] = $oFC->page_content [ 'HASH' ]; 

// $oFC->setting [ 'debug' ] = "yes";

/* get memory values */
$oFC->gsm_memorySaved ( );

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ($_POST, $_GET ?? "", $oFC , $selection ), __LINE__ . __FUNCTION__ );  

/* 12 selection functions */
if (isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$selection = strtolower ( $selection );	
	foreach ( array( 
			"list" => "print", 
			"print" => "print" , 
			"lid" => "lid", 			
			"ref" => "ref",
			"debug" => "debug",
			"all" => "all") as $pay => $load ) {
		if ( strstr ( $selection, $pay ) ) {
			$xmode .= $load; 
			$selection = trim ( str_replace ( $pay, "", $selection  ) );
}	}	}

/* 13 selection */
$oFC->search_mysql = "";
if ( isset ( $selection ) && strlen ( $selection ) > 1 ) {
	$help = "%" . str_replace ( ' ', '%', str_replace ( "?", "", trim ( $selection ) ) ) . "%";
	$oFC->search_mysql .= " WHERE 	`lep_mod_go_standen`.`zoek` LIKE '" . $help . "'";
} else { 
	$selection = "";
}
$oFC->page_content  [ 'PARAMETER' ] = trim( $selection );
$oFC->page_content  [ 'SUB_HEADER' ]= strtoupper ( $oFC->page_content [ 'PARAMETER' ] );

/* Print function */
if ( strstr ( $xmode, "print") ) {
	$oFC->setting ['pdf_filename'] = $oFC->gsm_sanitizeStringS ( "Overzicht_stand_" .  $oFC->page_content  [ 'PARAMETER' ] . ".pdf" , "s{FILE}" );
	require_once( $place['includes'].'classes/' . 'pdf.inc' );
}

/* list of references */
$referArr = array();
$checkArr = array();
$database->execute_query( 
	"SELECT * FROM `" . $oFC->file_ref [ 99 ] . "` WHERE `active` = '1' ORDER BY `updated` DESC", 
	true, 
	$checkArr);
foreach ($checkArr as $row) {
	if ( !isset($referArr [ $row [ 'ref' ] ] ) )
	$referArr [ $row [ 'ref' ] ] = $oFC->gsm_sanitizeStrings( $row[ 'ref' ], "s{CLEAN}" ) . " - " . $oFC->gsm_sanitizeStrings ( $row [ 'content_long' ], "s{CLEAN}" ) ;
}
	/* debug * / Gsm_debug (array ($referArr ), __LINE__ . __FUNCTION__ ); /* end debug */ 

/* sips test before job */ 
if ( isset( $_POST[ 'command' ] ) && $oFC->sips) { 
	unset ($_POST); 
	$oFC->description .= $oFC->language [ 'TXT_ERROR_SIPS' ] . NL; 
}

/* some job to do */
if ( isset( $_POST[ 'command' ] ) ) {
	switch ( $_POST[ 'command' ] ) {
		case "up":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("up", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ],  $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "down":
			$oFC->page_content [ 'POSITION' ] = $oFC->gsm_pagePosition ("down", $oFC->page_content [ 'POSITION' ] , $_POST[ 'n2' ], $oFC->setting [ 'qty_max' ], $_POST[ 'n0' ], $_POST[ 'n1' ] );
			break;
		case "Save":
			$oFC->page_content [ 'MODE' ] = 9;
			$oFC->recid = $_POST [ 'gsm_id' ];
			$FieldArr = array ();
			$oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file, "gsm_", "adresid" );
			break;
		case "New":
			/* route group */
			if ( isset ( $_POST [ 'gsmc_vink' ] ) ) {
				foreach ( $_POST [ 'gsmc_vink' ] as $Selected )	{
					$FieldArr = array ( "adresid" => $Selected );
					$oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 2, $main_file, "gsm_", "adresid" );
				}
				unset ($_POST);
				break;
			}
			/* route individueel */
			$oFC->page_content [ 'MODE' ] = 9;
			$oFC->recid = $_POST [ 'gsm_id' ];
			$FieldArr = array ();
			$oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 2, $main_file, "gsm_", "adresid" );
			unset ($_POST);
			break;
		case "Remove":
			/* route individueel */
			$oFC->page_content [ 'MODE' ] = 9;
			$delete_switch=false;
			if ( $_POST [ 'gsm_active' ] == 0 ) $delete_switch = true;
			$oFC->recid = $_POST [ 'gsm_id' ];
			if ( $delete_switch ) { // delete function
				$FieldArr = array ();  
				$oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 3, $main_file ); 
			} else { // active 
				$FieldArr =  array ( "active" => "0" );
				$oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file, "gsm_", "active" );
			}
			/* route group */
			if ( isset ( $_POST [ 'gsmcr_vink' ] ) ) { 
				foreach ( $_POST as $pay => $load ) { 
					if ( substr ( $pay, 0, 4 ) == "gsm_"  && $pay != "gsm_active") unset ( $_POST [ $pay ] );
				}
				if ( $delete_switch ) { // delete function
					foreach ( $_POST [ 'gsmcr_vink' ] as $Selected ) {
						$FieldArr = array (  );
						$oFC->gsm_accessRec ( $FieldArr, $Selected, 3, $main_file ); 
					};
					unset ($_POST);
					break;
				}
				foreach ( $_POST [ 'gsmcr_vink' ] as $Selected )	{			
					$FieldArr = array ( "active" => "0" );
					$oFC->gsm_accessRec ( $FieldArr, $Selected, 1, $main_file, "gsm_", "active" );
				}
				unset ($_POST);
				break;
			}
			unset ($_POST);
			break;	
		case "Reset":
			$oFC->recid = '';
			$oFC->search_mysql = "";
			$selection= "";
			$oFC->page_content  [ 'PARAMETER' ] = $selection;
			$oFC->selection = $selection;
			$oFC->page_content  [ 'SUB_HEADER' ]= "____";
		case "Proces":
			if ($oFC->recid == "") { 
				$oFC->page_content [ 'MODE' ] = 9; 
			} else {
				$oFC->page_content [ 'MODE' ] = 8;
				$FieldArr = array ();
				// get  values 
				$LocalHulp = array ();
				$oFC->page_content  = array_merge ( $oFC->page_content , $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file ) );
				$oFC->page_content [ 'PROCES' ] = "extra";
				$oFC->page_content [ 'PERSOON' ] = $oFC->gsm_standenSelectie ( 1, 
					$_POST [ 'gsm_ref' ], 
					"",
					isset($_POST [ 'gsmc_vink' ]) ? $_POST [ 'gsmc_vink' ] : "",
					"",
					"" );
			}
			break;
		case "View":
		case "Select":
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} elseif ( isset( $_GET[ 'command' ] ) ) {
	switch ( $_GET[ 'command' ] ) {
		// is a record selected ? 
		case "select":
			if ($oFC->recid == "") { 
				$oFC->page_content [ 'MODE' ] = 9; 
			} else {
				$oFC->page_content [ 'MODE' ] = 8;
				$FieldArr = array ();
				// get  values 
				$LocalHulp = array ();
				$oFC->page_content  = array_merge ( $oFC->page_content , $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file ) );
			}
			break;
		case 'remove': //    
			/* 13_ Delete a record */
			if ($oFC->recid == "") { 
				$oFC->page_content [ 'MODE' ] = 9; 
			} else {
				$oFC->page_content [ 'MODE' ] = 9;
				$FieldArr = array ();
				$oFC->page_content [ 'RESULT'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 3, $main_file );
				$oFC->recid = "";
			}
			/* Delete a record */
			break;
		default:
			$oFC->page_content [ 'MODE' ] = 9;
			break;
	} 
} else { 

}

// at this point we are preparing the printout
 if ( isset($oFC->setting ['pdf_filename'] )  && strlen( $oFC->setting ['pdf_filename'] ) >5 ) 
	$pdflink = $oFC->gsm_print( $oFC->page_content  [ 'SUB_HEADER' ] , $project, $xmode ,1 );	

switch ( $oFC->page_content [ 'MODE' ] ) {
	case 9: // default list 
		$pageok = true;
		
		/* bepaal aantal records */
		$result = array ( );
		$database->execute_query(
			sprintf ( "SELECT count(`id`) FROM `%s` %s", $oFC->file_ref[99], $oFC->search_mysql), 
			true, 
			$result);
		$row = current ( $result );
		$oFC->page_content [ 'aantal' ] = $row [ "count(`id`)" ];

		/* paging / accordeon /  make records unique / restore zoek/sort field */
		$limit_sql = $oFC->gsm_pagePosition ("sql", $oFC->page_content [ 'POSITION' ] , $oFC->page_content [ 'aantal' ], $oFC->setting [ 'qty_max' ]  );
		
		$query = "SELECT 
			`" . $oFC->file_ref[ 99 ] . "`.*,
			`" . $oFC->file_ref[ 98 ] . "`.`name` AS `adresname`,
			`" . $oFC->file_ref[ 98 ] . "`.`ref` AS `adresref`
			FROM `" . $oFC->file_ref[ 99 ] . "`
			LEFT JOIN `" . $oFC->file_ref[ 98 ] . "`
			ON `" . $oFC->file_ref[ 98 ] . "`.`id` = `" . $oFC->file_ref[ 99 ] . "`.`adresid` " . 
			$oFC->search_mysql ;
		$query .= " ORDER BY 
			`" . $oFC->file_ref[ 99 ] . "`.`active` DESC, 
			`" . $oFC->file_ref[ 99 ] . "`.`ref` DESC, 
			`" . $oFC->file_ref[ 99 ] . "`.`datumist` ASC, 
			`" . $oFC->file_ref[ 99 ] . "`.`id` DESC ". $limit_sql;
		$result = array();
		$database->execute_query( $query, true, $result);
		/* debug * / Gsm_debug ($result , __LINE__ . __FUNCTION__ ); /* end debug */
		if (count ( $result ) == 0 && strlen ( $oFC->search_mysql ) < 11 ) {
			$updatearr = array(
				'name' => "initial",
				'ref' => "INIT",
				'zoek' => "initial",
				'comment' => "initial" );
			$database->build_and_execute ( 
				"insert",
				$oFC->file_ref[ 99 ],
				$updatearr );
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . $oFC->language [ 'TXT_DATABASE_NEW' ];
			$result = array();
			$database->execute_query( $query, true, $result);
		}
		if (count ( $result ) > 0 ) {
			$job = array();
			foreach ($result as $rec => $row) {
				$hulpArr = array( );
				$hulpArr[ 'name' ] = $row[ 'name' ];
				$hulpArr [ 'ref' ] = ucfirst ( strtolower( $row [ 'ref' ] ) );
				if ( isset ( $row [ 'adresname' ] ) && strlen ($row [ 'adresname' ] ) > 5) $hulpArr[ 'name' ] = $oFC->gsm_sanitizeStrings ($row[ 'adresname' ], "s{CLEAN|TRIM}" );
				if ( isset ( $row [ 'ref' ] ) && strlen ($row [ 'ref' ] ) > 5) {
					$LocalHulp ="";
					if ( isset ($referArr [ $row [ 'ref' ] ] ) ) 
						$LocalHulp = trim ( str_replace ( "-", "", str_replace ( $row [ 'ref' ], "", $referArr [ $row [ 'ref' ] ] ) ) );
					if ( isset ( $row [ 'content_long' ] ) && strlen ( $LocalHulp ) >5 && $row [ 'content_long' ] != $LocalHulp ) $hulpArr[ 'content_long' ] = $LocalHulp;
				}
				if ( $row [ 'active'] == "1" 
					&& $row [ 'type'] == "1" 
					&& $row [ 'amtist' ] < 0.01 
					&& $row [ 'amtist' ] < $row [ 'amtsoll' ] 
					&& $row [ 'datumist' ] != "0000-00-00" ) $hulpArr [ 'datumist' ] = "0000-00-00";
				$hulpArr[ 'zoek' ] = $oFC->setting [ 'zoek' ] [ 'standen' ];
				$hulpArr[ 'zoek' ] = str_replace ( "|name|", "|".$hulpArr[ 'name' ]."|" , $hulpArr[ 'zoek' ] );
				foreach ( $row as $key => $value) $hulpArr[ 'zoek' ] = str_replace ( "|". $key. "|", "|".$value."|", $hulpArr[ 'zoek' ] );
				$hulpArr[ 'zoek' ] = strtolower ( $hulpArr[ 'zoek' ]);
				if ( $row [ 'ref' ] == $hulpArr [ 'ref' ] ) unset ( $hulpArr[ 'ref' ] );
				if ( $row[ 'name' ] == $hulpArr[ 'name' ] ) unset ($hulpArr[ 'name' ] );
				if ( $row[ 'zoek' ] == $hulpArr[ 'zoek' ] ) unset ($hulpArr[ 'zoek' ] );
				if ( count ( $hulpArr ) ) $job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . $oFC->gsm_accessSql( $hulpArr, 2 ) . " WHERE `id` = '" . $row['id'] . "'"; 
			}
			if ( count ( $job ) ) {
				$m = count ( $job );
				foreach( $job as $query) $database->simple_query( $query);
				$oFC->description .= date('G:i:s'.substr((string)microtime(), 1, 8).' : ').'Standen records altered '.$m .NL;
			} 
		} else { 
			// geen standen
		}
		$oFC->page_content  [ 'RESULTS' ] = $result;
		//xx	$oFC->page_content  [ 'STANDEN' ] = $resultx;
		break;
}		
// at this point the database query for the relevant records prepared
	
// display preparation
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 8: // default list 
		/* debug * / Gsm_debug ($oFC->page_content , __LINE__ . __FUNCTION__ ); /* end debug */
		$oFC->page_content  [ "ACTIVE" ] = $oFC->gsm_SelectOption ( $oFC->language [ 'active' ], $oFC->page_content  [ "active" ] );
		$oFC->page_content  [ "TYPE" ] =  $oFC->gsm_SelectOption (  $oFC->language [ 'standen_type' ], $oFC->page_content  [ "type" ] );
		if ( !isset ($referArr) ) $referArr= array ();
		$oFC->page_content  [ "REF" ] = $oFC->gsm_SelectOption ( $referArr, $oFC->page_content   [ "ref" ] );
		break;
}

switch ( $oFC->page_content [ 'MODE' ] ) {
	case 0:
		break;
	case 8:
		$oFC->page_content  [ 'SELECTIONA' ] = $oFC->page_content  [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel2 ( array ( 2, 4, 6, 7, 8 ) );
		$oFC->page_content  [ 'SELECTIONB' ] = "";
		$oFC->page_content  [ 'SELECTIONC' ] = "";
		break;
	case 9:
	default: 
		$oFC->page_content  [ 'SELECTIONA' ] = $oFC->gsm_opmaakSel2 ( array ( 10 ), 
			( strlen ( $oFC->page_content  [ 'PARAMETER' ] ) >1 ) ?  $oFC->page_content  [ 'PARAMETER' ] : "-", 
			"-" ,
			"0", 
			"-", 
			"-" ,
			"printrefall" );
		$oFC->page_content  [ 'SELECTIONC' ] = $oFC->gsm_opmaakSel2 ( array ( 1, 6, 11) , '-', $pdflink  ?? "-"  );
		if ( $oFC->page_content [ 'aantal' ] > $oFC->setting [ 'qty_max' ] ) 
			$oFC->page_content [ 'SELECTIONB' ] = $oFC->gsm_opmaakSel2 ( array ( 13), "-", "-", $oFC->page_content [ 'POSITION' ], $oFC->page_content [ 'aantal' ] , $oFC->setting [ 'qty_max' ]  );
		break;
}
/* end selection options *

/* memory save * /
$oFC->page_content  [ 'MEMORY' ] = $oFC->gsm_memorySaved ( 3 ); 
	
/* output processing */
// als er boodschappen zijn deze tonen in een error blok
$oFC->page_content [ 'STATUS_MESSAGE' ] .= $oFC->description; 
if (strlen($oFC->page_content [ 'STATUS_MESSAGE' ])>4 ) $oFC->page_content ['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content [ 'VERSIE' ] = $oFC->version; 
$oFC->page_content  [ 'MODE' ] = $oFC->page_content [ 'MODE' ];
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

$oFC->page_content [ "RECID" ] = $oFC->recid;

switch ( $oFC->page_content [ 'MODE' ] ) {
	default: //list
		break;
}

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ( $oFC->setting [ 'debug' ] == "yes" ) {
	Gsm_debug ( $oFC->page_content, __LINE__ . $template_name );  
	if ( LOAD_MODE == "x" )  Gsm_debug ( $oFC->version, $template_name );
}
?>