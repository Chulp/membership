<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

$MOD_GSMOFFL = array(
	'OWN' => "MOD_GSMOFFL",
	'LANG' => "NL",
	'VERS' => "20250208",
		
	'active' => array ( 
		'0' => 'niet actief', 
		'1' => 'actief',
		'2' => 'extra',
		'3' => 'verhuur'),
		
	'DUMMY' => array (
		'0' => 'Geen functionaliteit. Database niet geinitialiseerd, geen rechten of foute instellingen',
		'1' => 'Dummy module zonder functionaliteit is gestart',
		'2' => 'Controleer of de initiele routines zijn uitgevoerd'),

	'layout' => array ( 
		'aanv0' =>	'<p>aanvulling %s voor %s ( %s ) door %s </p><p>%s</p><hr />',
		'show0' =>	'<div class="ui text container">', //<div class="ui segment">',
		'show9' =>	'</div>',
		'show1' =>	'',
		'castor0' =>	'--'),

	'pdf'	=> array ( 
		'0' => "Document created on : ", 
		'1' => "Aantal regels verwerkt: ",
		'2' => "Selected options: ",
		'3' => "Modules versions: " ),
		
	'PDF_TAIL' => array(
		'TOTAL' => 		"Total records : ",
		'MAILED' => 	"Records mailed : ",	
		'POSTED' => 	"Records to be posted : ",
		'UNSELECTED' => "Records unselected : ",
		'SELECTED' => 	"Shares selected : ",
		'NOT_SELECTED' => "Shares not selected : ",
		'MAILING' => 	"Mailing on : ",
		'SELECTION' => 	"Selection : ",
		'EVERYBODY' => 	" Iedereen ",
		'EVERY_MAIL' => " Iedereen met mailadres ",
		'ALL_MEMBERS' =>" Alle leden ",
		'REMINDER' => 	" Reminder ",
		'REFERENCED' => " Referenced ",
		'SHAREHOLDERS' => " Aandeelhouders ",
		'STANDEN' => 	" Afhankelijk van standen "),
		
	'sel_mode' => array(
		1 => 'iedereen', 
		2 => 'iedereen met mail adres', 
		3 => 'geregistreede leden', 
		4 => 'open betaling', 	
		5 => 'met aangegeven groep (kies groep)', 
		6 => 'partners', 
		7 => 'met aangegeven stand (kies stand)' ),
		
	'standen_type'  => array ( 
		1 => "Bijdrage", 
		2 => "Resultaat", 
		3 => "Stand" ),
		
	'tbl_icon' => array ( 
		1 =>'View', 
		2 =>'Return', 
		3 =>'Add',
		4 =>'Save',  
		5 =>'Save (as new)', 
		6 =>'Remove', 
//		7 =>'Calculate',
//		8 =>'Check',
		9 =>'Select', 
//		10 =>'+',
//		11 =>'Print', 
//		12 =>'Set',
//		13 =>'reserved',
//		14 =>'Next',
//		15 =>'Test',
//		16 =>'Mail',
		17 =>'Process', 
		18 =>'Invoicing', 
//		19 =>'Balans', 
//		20 =>'Result' ,
		21 =>'Verwerkt'
	), 
	
	'TXT_ADRES' => array (
		'DAT0'	=> 'dat0 (geb) :',
		'DAT1'	=> 'dat1 (van) :',
		'DAT2'	=> 'dat2 (tot) :',
		'REF0'	=> 'ref0 (deel) :',
		'REF1'	=> 'ref1 (bank) :',
		'REF2'	=> 'ref2 (card) :',
		'INFO'	=> '-- info --'	),
		
	'TXT_COMP' => array (
		'1' => 'Consumer/Private Person',
		'2' => 'VAT free organsiation',
		'3' => 'Company',
		'4' => 'Partnership',
		'5' => 'Limited Partnership',
		'6' => 'Cooperative',
		'7' => 'Private Company',
		'8' => 'Limited Company',
		'9' => 'Organisation under VAT regime'),
		
	'TXT_MEMBER' => array (
		0	=> '---',
		1	=> 'member',
		9	=> 'ex-member' ), 
		
	'TXT_TYPE'  => array (
		0	=> 'unknown',
		1	=> 'by post',
		2	=> 'by mail',
		3	=> 'on-line'),

	'TXT_ACTIVE_DATA'	=> ' Actief record gevonden' ,	
	'TXT_CONSISTENCY'	=> ' Oeps consistency controle',
	'TXT_DATABASE_NEW'	=> ' Initial record added ',
	'TXT_DIR_CREATION' 	=> ' Directory aangemaakt',
	'TXT_ERROR_ADRES'	=> ' Oeps naam en of adres informatie ontbreekt',
	'TXT_ERROR_DATA' 	=> ' Oeps geen data ',  
	'TXT_NO_DATA' 		=> ' Geen data ', 	
	'TXT_ERROR_INIT'	=> ' Oeps systeem niet geinitialiseerd en/of lege database ',
	'TXT_ERROR_SIPS'	=> ' Oeps sips actief ',
	'TXT_ERROR_PAGE'	=> ' Oeps unexpected situation ',	
	'TXT_LOGIN' 		=> ' Login ',
	'TXT_LOGIN_ERROR_SHORT' => ' Not a valid e-mail address or password. ',
	'TXT_LOGIN_NOW' 	=> ' Uw login data is aangepast. Login met uw nieuwe gegevens. ',
	'TXT_LOGIN_REGISTER' => ' Register / Change Password ',
	'TXT_LOGIN_SETT'	=> ' Correct Login Settings  ',
	'TXT_LOGIN_VERIFY' 	=> ' Verificatie ',
	'TXT_MAINTENANCE' 	=> ' Maintenance ', 	
	'TXT_NO_ACCESS'		=> '(Partner) Access not available ',
	'TXT_REC_CHANGE'	=> ' Aantal records aangepast : ',
	'TXT_REMOVE_REF'	=> 'weg',
	'TXT_REMOVE_KEYWORD'=> 'recycle',
	'TXT_SETUP' 		=> ' Setup ',  
	
	'TAFEL'  => array (
		0	=> 'Tafel : '),

);

?>