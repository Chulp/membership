<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
$this->version ['print1'] = "20250207";


/* welke standenrecords */
$prtype = array ();
if ( strstr ( $selection, "lid" ) )   {	
	$prtype [ ] = 1; // leden 
} elseif ( strstr ( $selection, "debug" ) )   {	
	$prtype [ ] = 1; // leden
	$prtype [ ] = 2; // niet leden
	$prtype [ ] = 3; // ex leden
	$prtype [ ] = 4; // partners
	$prtype [ ] = 5; // overige	
} else { 
	$prtype [ ] = 1; // leden
	$prtype [ ] = 2; // niet leden 
}

$prmode = array (); 
$prmode [ ] = 0; // basis gegevens
if ( strstr ( $selection, "lid" ) ) {	
	$prmode [ ] = 1;
	$prmode [ ] = 2;
	$prmode [ ] = 5; }
if ( strstr ( $selection, "all" ) ) {
	$prmode [ ] = 1; 
	$prmode [ ] = 5; }
if ( strstr ( $selection, "ref" ) ) {	
//	$prmode [ ] = 1; 
	$prmode [ ] = 5; 
	$prmode [ ] = 6; }
if ( strstr ( $selection, "old" ) ) {	
	$prmode [ ] = 1; 
	$prmode [ ] = 2; 
	$prmode [ ] = 3; 	
	$prmode [ ] = 5; 
	$prmode [ ] = 6; }
if ( strstr ( $selection, "debug" ) ) {
	$prmode [ ] = 1; 
	$prmode [ ] = 2; 
	$prmode [ ] = 3; 
	$prmode [ ] = 4;
	$prmode [ ] = 5; 
	$prmode [ ] = 6; }

/* debug * /  gsm_debug (array (
	'query' => $query,
	'project' => $project,
	'type' => $prtype,
	'mode' => $prmode,
	'selection' => $selection,
	'func' => $func,
	'run' => $run,
	'this' => $this ), __LINE__ . __FUNCTION__ ); /* debug */ 

$printOK = false;
$title = ucfirst ( $project );
$regelcount  = 0;
$chaptercount = 0; //amount of chapters
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf_data   = array( );
$pdf_text   = '';

/* processing query */
$sql1 = "SELECT `" . $this->file_ref [ 99 ] . "`.*,
	`" . $this->file_ref [ 98 ] . "`.`id` as `user_id`,
	`" . $this->file_ref [ 98 ] . "`.`name` as `user_name`,
	`" . $this->file_ref [ 98 ] . "`.`adres` as `user_adres`,
	`" . $this->file_ref [ 98 ] . "`.`contact` as `user_tel`,		
	`" . $this->file_ref [ 98 ] . "`.`ref` as `user_referlist`,
	`" . $this->file_ref [ 98 ] . "`.`ref0` as `user_ref0`,	
	`" . $this->file_ref [ 98 ] . "`.`ref1` as `user_ref1`,
	`" . $this->file_ref [ 98 ] . "`.`ref2` as `user_ref2`,
	`" . $this->file_ref [ 98 ] . "`.`email` as `user_email`,
	`" . $this->file_ref [ 98 ] . "`.`dat0` as `user_geb`,
	`" . $this->file_ref [ 98 ] . "`.`dat1` as `user_start`,
	`" . $this->file_ref [ 98 ] . "`.`dat2` as `user_eind`
FROM `" . $this->file_ref [ 99 ] . "` LEFT JOIN `" . $this->file_ref [ 98 ] . "`
ON `" . $this->file_ref[ 98 ] . "`.`id` = `" . $this->file_ref [ 99 ] . "`.`adresid`";
$sql2 = "";
$sql3 = "";

/* selection */
$sql2 = sprintf ( " WHERE `%s`.`active`='1' ", $this->file_ref [ 98] );
if ( strlen ( $query ) > 1 ) {
	if ( in_array ( 1, $prmode ) ) {
		$sql2 = sprintf ( " WHERE `%s`.`active`='1' AND `%s`.`zoek` LIKE '%s' ",
				$this->file_ref [ 99], 
				$this->file_ref [ 99], 
				"%". strtolower( $query ). "%" ) ;
	} elseif ( in_array ( 2, $prmode ) ) {
		$sql2 = sprintf ( " WHERE `%s`.`active`='1' AND `%s`.`ref` LIKE '%s' ",
				$this->file_ref [ 99], 
				$this->file_ref [ 99], 
				"%".ucfirst ( strtolower ( $query ) ). "%" );
	} elseif ( in_array ( 3, $prmode ) ) {
		$sql2 = sprintf ( " WHERE `%s`.`ref` LIKE '%s' ",
				$this->file_ref [ 99], 
				"%". strtolower( $query ). "%" );
	} 
}
/* volgorde */
$sql3 = sprintf ( " ORDER BY `%s`.`type`, `%s`.`ref`, `%s`.`datumist`", 
	$this->file_ref [ 99], 
	$this->file_ref [ 99],
	$this->file_ref [ 99]);
		
$sql = $sql1 . $sql2 . $sql3;

$tekst1 = array (
	'1' => array ( "id", "naam", "" , "", "",""),	
	'2' => array ( "niet leden", "status 1" , "status 2", "", "",""),	
	'3' => array ( "melding", "aanwezig", "verwijderd", "", "",""),	
	'4' => array ( "melding", "aanwezig", "verwijderd", "", "",""),	
	'5' => array ( "melding", "aanwezig", "verwijderd", "", "",""),	
);
$titel1 = array (
	'1' => "leden",	
	'2' => "niet leden",
	'3' => "ex leden",
	'4' => "partners",
	'5' => "overige",
);

$results = array();	
$database->execute_query( 
	$sql, 
	true, 
	$results);
$regelcount = count ($results);
$printOK = false;
/* debug * /  gsm_debug (array (	'results' => $results ), __LINE__ . __FUNCTION__ ); /* debug */ 
foreach ( $prtype as $type) {
	$AryOk = array ();
	if ( $regelcount > 0 ) $printOK = true;
	if ( $printOK ) {
		$levelbreak = "--";
		foreach	( $results as $row ) {
			$show = false;
			$lid = "--";
			/* bepaling lid */	
			if ( $row [ 'user_start' ] > "1970-01-01" ) { 
				$lid = "lid";
				if ( $row [ 'user_eind' ] > $row [ 'user_start' ] ) { 
					$lid = "ex-lid";
					if ( $row [ 'user_eind' ] > $run ) $lid = "opgezegd";
				}
			}
			$row ['lid'] = $lid;
			/* bepaling partner */	
			$partner = "-";
			if ( strlen ( $row [ 'user_ref0' ]) > 2 ) $partner = $row [ 'user_ref0' ];
			$row ['partner'] = $partner;
			if ( $lid == "lid" && $type == '1' ) { 
			} elseif ( $lid == "--" && $type == '2' ){  
			} elseif ( in_array ( '3', $prtype) && $lid != "lid" && $type == '2' ) { 
			} elseif ( $lid == "ex-lid" && $type == '3' ){  
			} elseif ( $partner != "-" && $type == '4' ) { 
			} elseif ( $partner == "-" && $type == '5' ) {  
			} else { 
			continue;}
			/* nieuwe persoon */
			if ( $levelbreak != $row [ 'user_referlist' ] ) {  
				$levelbreak = $row [ 'user_referlist' ];
/* debug * /  gsm_debug (array (	'row' => $row ), __LINE__ . __FUNCTION__ ); /* debug */ 
				/* korte naam */
				$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
					$row [ 'user_referlist' ],
					str_replace ( "|", " ", $row [ 'user_name' ]),
					$row ['partner'], 
					"",
					$row ['lid'], 
					"") ) );
				if ( in_array ( 1, $prmode) ) {
					/* lange naam */
					if (strlen ( $row [ 'user_adres' ] ) > 10 || strlen ( $row [ 'user_email' ] ) > 8)
						$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
							"",
							str_replace ( "|", " ", $row [ 'user_adres' ]),
							"",
							"",
							$row [ 'user_id' ],
							"" ) ) );
					if (strlen ( $row [ 'user_tel' ] ) > 8 
						|| strlen ( $row [ 'user_email' ] ) > 8)			
						$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
							"",
							$row [ 'user_email' ],
							"",
							"",
							$row [ 'user_tel' ],
							"" ) ) );
				}
				if ( in_array ( 2, $prmode) ) {
					/* details naam */	
					if ( $row [ 'user_geb' ]  != "0000-00-00" 
						&& 	$row [ 'user_geb' ]  != "1970-01-01" )	
						$pdf_data [] = explode ( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
							"",
							$row [ 'user_geb' ],
							$row [ 'user_ref1' ],
							"",
							"",
							"" ) ) );
				}
				if ( in_array ( 3, $prmode) ) {
					/* datum vanaf */	
					if ( $row [ 'user_start' ]  != "0000-00-00" 
						&& 	$row [ 'user_start' ]  != "1970-01-01" )	
						$pdf_data [] = explode ( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
							"",
							"start : ".$row [ 'user_start' ] ,
							($row [ 'user_eind' ] > $row [ 'user_start' ]) ? " tot : " . $row [ 'user_eind' ] : "",
							"",
							"",
							"" ) ) );
				}
				if ( in_array ( 4, $prmode) ) {
					/* datum vanaf */	
					if ( strlen ($row [ 'user_ref2' ] ) >3  )	
						$pdf_data [] = explode ( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
							"",
							$row [ 'user_ref2' ] ,
							"",
							"",
							"",
							"" ) ) );
				}
			}	
			/* standen */	
			if ( $row [ 'type' ] == "1" ) {
				if ( in_array ( 5, $prmode) ) {
					$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
						$row [ 'ref' ],
						$row [ 'content_long' ],
						"Bijdrage",
						"",
						"",
						$row [ 'type' ] . " / " . $row [ 'id' ],
						"" ) ) );	
				}
				if ( in_array ( 6, $prmode) ) {
					$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
						"",
						$row [ 'content_short' ],
						$row [ 'datumsoll' ],
						"",
						"",
						$this->gsm_sanitizeStrings ($row [ 'amtsoll' ], 's{EURT|KOMMA}'),
						"" ) ) );
					$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
						"",
						$row [ 'comment' ],
						($row [ 'datumist' ] == "0000-00-00") 
							? "" 
							: $row [ 'datumist' ],
						"",
						"",
						( $row [ 'amtist' ] == 0 ) 
							?  "" 
							: $this->gsm_sanitizeStrings ($row [ 'amtist' ], 's{EURT|KOMMA}'),
						"" ) ) );
					if 	( $row [ 'amtsoll' ] - $row [ 'amtist' ] > 0 )
					$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
						"",
						'',
						"open",
						"",
						"",
						$this->gsm_sanitizeStrings ( $row [ 'amtsoll' ] - $row [ 'amtist' ], 's{EURT|KOMMA}'),
						"" ) ) );
				}
			}
						
			if ( $row [ 'type' ] == "2" ) {
				if ( in_array ( 5, $prmode) ) {
					$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
						$row [ 'ref' ],
						$row [ 'content_long' ],
						"Resultaat",
						"",
						"",
						$row [ 'id' ],
						"" ) ) );	
				}

				$oordeel = 	"--";	
				/* oordeel */
				if ($row['datumist'] > "1970-01-01" 
					&& $row['datumsoll'] > "1970-01-01") {
					if ( $row['datumsoll'] > date ( "Y-m-d", time() ) ) { 
						$row [ 'datumist' ] = 	"einde geldigheid"; 
					} else { 
						$row [ 'datumist' ] = 	"verlopen" ;
					}
				}
				if 	($row['datumist'] > "1970-01-01" 
					&& $row['datumsoll'] == "0000-00-00") $oordeel = "geldig" ;
			
				if ( in_array ( 6, $prmode) ) {
					$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
						"",
						$row [ 'content_short' ],
						($row [ 'datumsoll' ] == "0000-00-00") 
							? $oordeel 
							: $row [ 'datumsoll' ],
						"",
						"",
						( $row [ 'amtsoll' ] == 0 ) 
							?  "" 
							:  $this->gsm_sanitizeStrings ($row [ 'amtsoll' ], 's{WHOLE}'),
						"" ) ) );
						
					$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
						"",
						$row [ 'comment' ],
						($row [ 'datumist' ] == "0000-00-00") 
							? "uitgenodigd" 
							: $row [ 'datumist' ],
						"",
						"",
						( $row [ 'amtist' ] == 0 ) 
							?  "" 
							: $this->gsm_sanitizeStrings ($row [ 'amtist' ], 's{WHOLE}'),
						"" ) ) );
				}
			}
			if ( $row [ 'type' ] == "3" ) {
				if ( in_array ( 5, $prmode) ) {
					$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
						$row [ 'ref' ],
						$row [ 'content_long' ],
						"Stand",
						"",
						"",
						$row [ 'type' ] . " / " . $row [ 'id' ],
						"" ) ) );	
				}
				if ( in_array ( 6, $prmode) ) {
					$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
						"",
						$row [ 'content_short' ],
						($row [ 'datumsoll' ] == "0000-00-00") 
							? "" 
							: $row [ 'datumsoll' ],
						"",
						"",
						( $row [ 'amtsoll' ] == 0 ) 
							?  "" 
							: $this->gsm_sanitizeStrings ($row [ 'amtsoll' ], 's{WHOLE}'),
						"" ) ) );
					$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
						"",
						$row [ 'comment' ],
						($row [ 'datumist' ] == "0000-00-00") 
							? "melding" 
							: $row [ 'datumist' ],
						"",
						"",
						( $row [ 'amtist' ] == 0 ) 
							?  "" 
							: $this->gsm_sanitizeStrings ($row [ 'amtist' ], 's{WHOLE}'),
						"" ) ) );
						if ( $row [ 'datumsoll' ] > "1970-01-01" && $row [ 'datumist' ] > $row [ 'datumsoll' ] )
							$pdf_data [] = explode( ';', trim( sprintf( " %s;%s;%s;%s;%s;%s",
								"",
								"",
								"afmelding",
								"",
								"",
								"",
								"" ) ) );
				}
			}
		}	 
	}
	$chaptercount++;
	$pdf_cols = array( 25, 120, 30, 10, 0, 0 ); 
	$pdf_header = $tekst1 [ $type ] ?? array ("","","","","","");
	$Chapter_string = $titel1 [ $type ] ?? "--";
	if ( $chaptercount > 1 && count ( $pdf_data ) >0) $pdf->AddPage();
	$pdf->ChapterTitle( $chaptercount, $Chapter_string );
	if ( count ( $pdf_data ) >0 ) $pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
	$pdf_data   = array( );
	$pdf->ChapterBody( $pdf_text );
	$pdf_text   = '';
}

if ( count ( $pdf_data ) >0 ) $pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
$pdf_data   = array( );
if ($this->setting [ 'debug' ] == "yes"){
	
	/* stop */
	$Chapter_string = "Afsluiting" ;
	$chaptercount++;
	if ( $chaptercount > 1 ) $pdf->AddPage();
	$pdf->ChapterTitle( "--", $Chapter_string );

	if ( count ( $pdf_data ) >0 ) $pdf->DataTable( $pdf_header, $pdf_data, $pdf_cols );
	$pdf_data   = array( );

	// footer
	$pdf_text .= "\n\n" . $this->setting [ 'droplet' ] [ LANGUAGE . '0' ];
	$pdf_text .= "\n\n" . $this->setting [ 'pdf_filename' ] . "\n";
	$pdf_text .= $this->language [ 'pdf' ][0] . str_replace( "_", " ", $run ) . "\n";
	$pdf_text .= sprintf ( "\n %s %s\n", $this->language [ 'pdf' ][ 1 ] , $regelcount ) ;
	if ( strlen( $query ) > 1 ) $pdf_text .= sprintf ( "\n %s %s :\n %s %s" . "\n"  ,  $this->language [ 'pdf' ][ 2 ], $project, $selection , ucfirst ( strtolower ( $query ) ) );
	$pdf_text .= sprintf ( "\n %s \n", $this->language [ 'pdf' ][ 3 ]) ;
	foreach ($this->version as $key => $value) $pdf_text .= $key . "_" . $value . "\n";
}
// pdf output
if ($pdf_text != '' ) $pdf->ChapterBody ( $pdf_text );

?>