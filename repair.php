<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
// function to remove old directories
function gsm_rmdir( $dir )	{
	if (empty($dir)) return false;
	if ( is_dir( $dir ) )  {
 		$files = scandir( $dir );
  			foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) gsm_rmdir( "$dir/$file" ); }
    		rmdir( $dir );
 	} else if ( file_exists( $dir ) ) {
    	unlink( $dir );
}	}

// functions to copy files and non-empty directories  
function gsm_xcopy( $src, $dst ) {
	// 	if ( file_exists( $dst ) ) { gsm_xrmdir( $dst ); }
 	if ( is_dir( $src ) )  {
   		if ( !file_exists( $dst ) ) mkdir( $dst, 0777);
    	$files = scandir( $src );
    	foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) xcopy( "$src/$file", "$dst/$file" ); }
  	} else if ( file_exists( $src ) ) { copy( $src, $dst ); }	
} 
 
if (LOAD_MODE == "x" && isset ( $xmode ) && strlen ( $xmode ) >3 ) {  
	if (strstr($xmode, "DETAIL")) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Detail function is selected'.NL;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' This module version  : 20250207';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' selection :'. $xmode;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Functions detected: ';
		
		if (strstr ( $xmode, "DETAIL" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' DETAIL.';
		if (strstr ( $xmode, "IMAGE" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' IMAGE.';
		if (strstr ( $xmode, "INSTAL" ) )	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' INSTALL.';
		if (strstr ( $xmode, "REPAIR" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' REPAIR.';
		if (strstr ( $xmode, "LOGGING" ) ) 	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' LOGGING.';
		if (strstr ( $xmode, "REMOVE" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' REMOVE.';
		if (strstr ( $xmode, "EXPL" ) )  	$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' EXPL.';

		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' company :'. $oFC->setting [ 'droplet' ] [ LANGUAGE . '0' ];
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' webmaster : '. $oFC->setting [ 'droplet' ] [ LANGUAGE . '4' ] ;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' mailadres : '. $oFC->setting [ 'droplet' ] [ LANGUAGE . '10' ] ;
		
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Directories involved :';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->setting [ 'collectdir' ]; 
//		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->setting [ 'datadir' ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->setting [ 'mediadir' ] ;
		foreach ( $oFC->setting [ 'entity' ] as $key => $value ) 
			$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' entity : '. $key . " | " .$value ;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' owner :'. $oFC->setting [ 'owner' ];
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' allowed :'. implode("|", $oFC->setting [ 'allowed' ] ) ;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' remove keyword :'. $oFC->setting [ 'remove' ] ;	
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' menu :'. $FC_SET [ 'SET_menu' ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' function :' . $FC_SET [ 'SET_function' ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' zoek adres :' .  $oFC->setting [ 'zoek' ] [ 'adres' ];
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' zoek standen :' .  $oFC->setting [ 'zoek' ] [ 'standen' ];
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' aantal regels : ' . $oFC->setting [ 'qty_max' ] ;	
		
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Tables involved :';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->file_ref [ 99 ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->file_ref [ 98 ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . " - " . $oFC->file_ref [ 97 ]; 
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' end settings :'.NL;
	} 
	
if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ( "this"=>$oFC ), __LINE__ . "repair" ); 


/* install */
	if (strstr ( $xmode, "INSTAL" ) ) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start Install function: Frontend for this module will be created / re-created';
		// determine directories to be used.
		$dir_from = LEPTON_PATH.'/modules/'.LOAD_MODULE.LOAD_SUFFIX.'/frontend/' .LOAD_MODULE.LOAD_SUFFIX;
		$dir_to = LEPTON_PATH.'/templates/'.DEFAULT_TEMPLATE.'/frontend/'.LOAD_MODULE.LOAD_SUFFIX;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Frontend files (from install package) will be moved to default template.';
		// copy and replace function
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' from : '.$dir_from;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' to : '.$dir_to;
		if ( file_exists ( $dir_to ) ) {
			$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' save previous to  : '.$dir_to."_bck";
			$oFC->gsm_copyFile( $dir_to, 1, $dir_to."_bck" );
		}
		$oFC->gsm_copyFile( $dir_from, 1, $dir_to );
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' End Install function'.NL;
	} 	
		
/* images */		
	if (strstr ( $xmode, "IMAGE" ) ) {	
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start Image function';
		$dir_from = LEPTON_PATH.'/templates/'.DEFAULT_TEMPLATE.'/frontend/img/';
		$dir_to = LEPTON_PATH.'/modules/'.LOAD_MODULE.LOAD_SUFFIX.'/img/';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Frontend image directory will be moved to image directory (to overwrite install package).';
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' from : '.$dir_from;
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' to : '.$dir_to;
		$oFC->gsm_copyFile( $dir_from, 3, $dir_to );
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' End Image function'.NL;
	}  
	
/* remove data */
	if (strstr ( $xmode, "REMOVE" ) ) {
		$nremoved=0;
		$job=array();
		$oFC->description .= NL.date ( "H:i:s " ) . __LINE__  . ' Start remove function' . NL;
		$results = array();
		$database->execute_query( 
			sprintf ("SELECT * FROM `%s`", $oFC->file_ref [ 99 ] ), 
			true, 
			$results);
		if ( count($results) == 0) $oFC->description .= $oFC->language [ 'TXT_ERROR_DATA' ]. NL; 
		foreach ($results as $row) {
			$adresarr= array();
			if ( strstr ( $row [ 'email' ], $oFC->setting [ 'remove' ] ) ) {
				$job [ ] = "DELETE FROM `" . $oFC->file_ref [ 99 ] . "` WHERE `id` = '". $row[ 'id' ] . "'";
				if ( $row ['adresid'] > 0 ) 
					$job [ ] = "DELETE FROM `" . $oFC->file_ref [ 97 ] . "` WHERE `user_id` = '". $row['adresid'] . "'";
		}	}	
		$n=0;
		if ( isset ( $job ) && count( $job ) > 0 ) {
			foreach( $job as $key => $query ) {
				$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' .$query.NL;
				$database->simple_query ( $query);
				$n++;
		} 	}
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Database entries removed : '.$n.NL; 		
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' End remove function' . NL;
		$xmode .= "REPAIR";
	} 
	
/*repair database */
	if (strstr($xmode, "REPAIR")) {
		$oFC->description .= NL . date ( "H:i:s " ) . __LINE__  . ' Start repair function' . NL;
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Check user records where there is more as one address record'.NL;
		$query = "SELECT *  FROM `" . $oFC->file_ref [ 97] . "`";
		$results = array();
		$database->execute_query( $query, true, $results);
		$update_go=false;
		$job= array();
		// loop langs userfile als er meer adres records zijn zet de link in het adres bestand beide op 0 en frustrate e-mail
		foreach ( $results as $row) {
			$checks = array();
			$database->execute_query( 
				sprintf ( "SELECT *  FROM `%s` WHERE `adresid` = '%s' ORDER BY `email`, `id`", $oFC->file_ref [ 99 ], $row [ 'user_id' ] ),
				true, 
				$checks);
			if ( count($checks) > 1){
				$localMailsave = "";
				foreach ( $checks as $chk) {
					if ( $localMailsave == "" ) {
						$repairarr = array ( );
						$repairarr [ 'adresid' ] = 0; 
						$localMailsave = $chk [ 'email' ];
						$job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . $oFC->gsm_accessSql ( $repairarr, 2 ) . " WHERE `id` = '" . $chk [ 'id' ] . "'";
						$update_go=true;
					} else {
						$repairarr = array ( );
						$repairarr [ 'adresid' ] = 0;
						if ( $localMailsave == $chk [ 'email' ] ) $repairarr [ 'email' ] = $chk[ 'id' ] . ".ecycle." . $chk[ 'email' ];
						$localMailsave = $chk [ 'email' ];
						$job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . $oFC->gsm_accessSql( $repairarr, 2 ) . " WHERE `id` = '" . $chk['id'] . "'";
						$update_go=true;
		}	}	}	}
		if ($update_go) {
			$m=0;
			foreach( $job as $key => $query) {
				if ( strstr ( $xmode, "DETAIL" ) ) $oFC->description .= $query . NL;
				$database->simple_query ( $query );
				$m++; }
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Check performed, records involved : '.$m.NL; 
		}
		$job = array();
		//loop langs de combinatie user/adres op basis van id als het e-mail adres in beide files niet identiek is corrigeer dat in de adresfile en de userfile 
		$results = array();
		$update_go=false;
		$query = "SELECT 
			`" . $oFC->file_ref [ 99 ] . "`.*, 
			`" . $oFC->file_ref [ 97 ] . "`.`user_id` ,
			`" . $oFC->file_ref [ 97 ] . "`.`email` AS `mail`,
			`" . $oFC->file_ref [ 97 ] . "`.`password`,
			`" . $oFC->file_ref [ 97 ] . "`.`username`
			FROM `" . $oFC->file_ref [ 97 ] . "`
			RIGHT JOIN `" . $oFC->file_ref [ 99 ] . "`
			ON `" . $oFC->file_ref [97] . "`.`user_id` = `" . $oFC->file_ref [ 99 ] . "`.`adresid`";
		$database->execute_query( $query, 
			true, 
			$results);
		foreach ( $results as $row ) {
			if ( $row [ 'adresid' ] > 0 
				&& isset ( $row [ 'mail' ] ) 
				&& strlen ( $row [ 'mail' ] ) >8  
				&&  $row [ 'email' ] != $row[ 'mail' ] ) {
				$repairarr99 = array (); 
				$repairarr97 = array (); 
				$repairarr99 [ 'email' ] = trim ( strtolower (  $row [ 'mail' ] ) );
				$repairarr97 [ 'email' ] = trim ( strtolower (  $row [ 'mail' ] ) );	
				$repairarr97 [ 'username' ] = trim ( strtolower ( $row [ 'mail' ] ) );	
				$job [ ] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . $oFC->gsm_accessSql ( $repairarr99, 2 ) . " WHERE `id` = '" . $row['id'] . "'";
				$job [ ] = "UPDATE `" . $oFC->file_ref [ 97 ] . "` SET " . $oFC->gsm_accessSql ( $repairarr97, 2 ) . " WHERE `user_id` = '" . $row['user_id'] . "'";				
				$update_go = true;
		}	}
		if ($update_go) {
			$errors = array( );
			$m=0;
			foreach( $job as $key => $query) {
				if (strstr ( $xmode, "DETAIL" ) ) $oFC->description .= $query.NL;
				$database->simple_query( $query);
				$m++; }
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Check performed, records involved : '.$m.NL; 
		} 
		$update_go=false;
		$job= array();
		$oFC->description .= NL . date ( "H:i:s " ) . __LINE__  . ' Check user records with corrupted passwords'.NL;
		$query = "SELECT *  FROM `" . $oFC->file_ref [ 97 ] . "`";
		$results = array();
		$database->execute_query( $query, true, $results);
		$update_go=false;
		$job= array();
		$linklayout = array ( 0=>12, 1=>5 , 2=>3 ); // layout link 3 delen 
		/* loop langs de userfile. als het password te kort lijkt verwijder het wachtwoord */
		foreach ( $results as $row ) {
			if ( strlen ( $row ['password']) < $linklayout [ 0 ] )  {
				$repairarr = array ( "password" => "obstructedpassword" );
				$job[] = "UPDATE `" . $oFC->file_ref [ 97 ] . "` SET " . $oFC->gsm_accessSql ( $repairarr, 2 ) . " WHERE `user_id` = '" . $row [ 'user_id'] . "'";
				$update_go = true;
		}	}
		if ( $update_go ) {
			$errors = array( );
			$m=0;
			foreach( $job as $key => $query) {
				if (strstr($xmode, "DETAIL")) $oFC->description .= $query.NL;
				$database->simple_query( $query);
				$m++; }
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Check performed, records involved : '.$m.NL; 
		}
		$oFC->description .= NL . date ( "H:i:s " ) . __LINE__  . ' Check user records where there is no address record'.NL;
		$results = array();
		$database->execute_query( 
			sprintf ( "SELECT *  FROM `%s`", $oFC->file_ref [ 97 ] ), 
			true, 
			$results);
		$update_go=false;
		$job = array();
		// looplangs de user records en check adresrecord aanwezigheid. voeg toe of pas aan als er eenzelfde e-mail adres is 
		foreach ( $results as $row) {
			$checks = array();
			$database->execute_query ( 
				sprintf ( "SELECT *  FROM `%s` WHERE `adresid` = '%s'" , $oFC->file_ref [ 99 ] ,$row['user_id'] ), 
				true, 
				$checks );
			if ( count ( $checks ) == 0) {
				$checks2 = array();
				$database->execute_query ( 
					sprintf ( "SELECT * FROM `%s` WHERE `email`='%s'" , $oFC->file_ref [ 99 ], $row[ 'email' ] ),
					true, 
					$checks2 );
				if ( count( $checks2 ) == 0 ){
					$hulpArr = array( );
					$hulpArr [ 'name' ] = $row[ 'display_name' ];
					$hulpArr [ 'email' ] = trim($row[ 'email' ]);
					$hulpArr [ 'adresid' ] = $row[ 'user_id' ];
					$job [ ] = "INSERT INTO `" . $oFC->file_ref [ 99 ] . "` " .  $oFC->gsm_accessSql ( $hulpArr, 1 );
					$update_go=true;
				} elseif ( count ( $checks2 ) > 0 ) {
					foreach ( $checks2 as $chk ) {
						$repairarr = array ( "adresid" =>$row[ 'user_id' ]); 
						$job[] = "UPDATE `" . $oFC->file_ref [ 99 ] . "` SET " . $oFC->gsm_accessSql ( $repairarr, 2 ) . " WHERE `id` = '" . $chk[ 'id' ] . "'"; 
						$update_go=true;
		}	}	} 	}
		if ($update_go) {
			$errors = array( );
			$m=0;
			foreach( $job as $key => $query) {
				if (strstr($xmode, "DETAIL")) $oFC->description .= $query.NL;
				$database->simple_query( $query);
				$m++;
			}
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Check performed, records involved : '.$m.NL;
		} 
		$oFC->description .=NL . date ( "H:i:s " ) . __LINE__  . ' Check standen records where there is no address record en update'.NL;
		$query= "SELECT *  FROM `" . $oFC->file_ref [ 98 ] . "`";
		$results = array();
		$database->execute_query( 
			sprintf ( "SELECT *  FROM `%s`", $oFC->file_ref [ 98 ]),
			true, 
			$results);
		$update_go=false;
		$job= array();
		foreach ( $results as $row) {	
			$check= "SELECT * FROM `" . $oFC->file_ref [ 99 ] . "` WHERE `id` = '". $row['adresid'] . "'";
			$checks = array();
			$database->execute_query( 
				sprintf ( "SELECT * FROM `%s` WHERE `id` = '%s'", $oFC->file_ref [ 99 ], $row['adresid' ] ),
				true, 
				$checks);
			if ( count($checks) < 1 ) {
				$job[] = sprintf ( "DELETE FROM `%s` WHERE `id` = '%s'", $oFC->file_ref [ 98 ], $row['id'] );
				$update_go=true;
			} else {
				$chk = current ($checks);
				$hulpArr = array( );
				if ( $chk [ 'name' ] != $row [ 'name' ] ) $hulpArr[ 'name' ] = $chk[ 'name' ];
				if ( count ( $hulpArr ) ) {
					$job [ ] = "UPDATE `" . $oFC->file_ref [ 98 ] . "` SET " . $oFC->gsm_accessSql ( $hulpArr, 2 ) . " WHERE `id` = '" . $row['id'] . "'";
					$update_go=true;
		}	}	}
		if ($update_go) {
			$errors = array( );
			$p=0;
			foreach( $job as $key => $query) {
				if (strstr($xmode, "DETAIL")) $oFC->description .= $query.NL;
				$database->simple_query( $query);
				$p++; }
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Check performed, records involved : '.$p.NL; 
		}
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' End repair function' . NL;
	}
	
/* remove logging */
	if ( strstr ( $xmode, "LOG" ) ) {
		$oFC->description .= NL. date ( "H:i:s " ) . __LINE__  . ' Start logging function' . NL;
		$dir_to_remove = sprintf ( "%s%s/" , LEPTON_PATH, $oFC->setting [ 'collectdir' ] ?? "/geen data/"  );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' remove attempt ' .$dir_to_remove. NL;
		if ( file_exists ( $dir_to_remove ) ) {
			$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' remove ' . $dir_to_remove . NL;
			gsm_rmdir ( $dir_to_remove );
		}
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' create empty directory' . NL;
		if ( !file_exists ( $dir_to_remove ) ) mkdir ( $dir_to_remove, 0777 );
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' empty directry creation completed' . NL;
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' End logging function' . NL;
	} 

	if ( strstr ( $xmode, "EXPL" ) ) {
		$oFC->page_content [ 'TOEGIFT' ] .= '<table class="ui very basic collapsing celled table">';
		$oFC->page_content [ 'TOEGIFT' ] .= '<tr class="active"><td>d_xxxx_</td><td><strong>function</strong></td></tr>';
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>DETAIL </td><td> detailed data</td></tr>";	
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>REMOVE </td><td> remove data (e_mail = " .  $oFC->setting [ 'remove' ]. " ).</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>LOGGING </td><td> empty the logging</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>REPAIR </td><td> repair database</td></tr>";	
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>INSTALL </td><td> frontend files are created: customized values are overwritten bij default values !!</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "<tr><td>IMAGE </td><td> image directory copied from frontend</td></tr>";
		$oFC->page_content [ 'TOEGIFT' ] .= "</table>";		
	}

}
?>
