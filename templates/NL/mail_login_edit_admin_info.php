<?php
/*
 * @module     mail template
 */
// include class.secure.php to protect this file and the whole CMS!
if ( defined( 'LEPTON_PATH' ) ) {
  include( LEPTON_PATH . '/framework/class.secure.php' );
} //defined( 'LEPTON_PATH' )
else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while ( ( $level < 10 ) && ( !file_exists( $root . '/framework/class.secure.php' ) ) ) {
    $root .= $oneback;
    $level += 1;
  } //( $level < 10 ) && ( !file_exists( $root . '/framework/class.secure.php' ) )
  if ( file_exists( $root . '/framework/class.secure.php' ) ) {
    include( $root . '/framework/class.secure.php' );
  } //file_exists( $root . '/framework/class.secure.php' )
  else {
    trigger_error( sprintf( "[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER[ 'SCRIPT_NAME' ] ), E_USER_ERROR );
  }
}
// end include class.secure.php
/* change history
 * v20170903 
 * 
 */
    $mail_subject = 'voor actie: Gewijzigde informatie {GSM_EMAIL}';
    $mail_content = 
	'Hallo {WEB_WEBMASTER},
	<br/><br/>{GSM_NAME} met e-mail adres {GSM_EMAIL} heeft {WEB_TIMESTAMP} verzocht om zijn/haar gegevens als volgt te wijzigen. 
	<br/><br/>Nu zijn deze als volgt: 
	<br/>Naam: {GSM_NAME}
	<br/>Adres: {GSM_ADRES}
	<br/>E-mail adres: {GSM_EMAIL}	
	<br/><br/>Na deze wijzigingen zijn deze als volgt: 
	<br/>Naam: {GSM_NWNAME}
	<br/>Adres: {GSM_NWADRES}
	<br/>E-mail adres: {GSM_NWEMAIL}
	<br/><br/>{GSM_NAME} is geinformeerd dat de {WEB_WEBMASTER} deze informatie aangepast. 
	<br/><br/>Eventueel overige gegevens zijn reeds aangepast.
	<br/><br/>Activatie link: <a href = "{GSM_LINK}">{GSM_LINK}</a>
	<br/><br/><br/><i> {WEB_WEBMASTER} <br />e-mail {WEB_EMAIL}</i>';
?>