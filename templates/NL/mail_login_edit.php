<?php
/*
 * @module     mail template
 */
// include class.secure.php to protect this file and the whole CMS!
if ( defined( 'LEPTON_PATH' ) ) {
  include( LEPTON_PATH . '/framework/class.secure.php' );
} //defined( 'LEPTON_PATH' )
else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while ( ( $level < 10 ) && ( !file_exists( $root . '/framework/class.secure.php' ) ) ) {
    $root .= $oneback;
    $level += 1;
  } //( $level < 10 ) && ( !file_exists( $root . '/framework/class.secure.php' ) )
  if ( file_exists( $root . '/framework/class.secure.php' ) ) {
    include( $root . '/framework/class.secure.php' );
  } //file_exists( $root . '/framework/class.secure.php' )
  else {
    trigger_error( sprintf( "[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER[ 'SCRIPT_NAME' ] ), E_USER_ERROR );
  }
}
// end include class.secure.php
/* change history
 * v20170903 
 * 
 */
    $mail_subject = 'Gewijzigde informatie';
    $mail_content = 
	'Hallo {GSM_NAME},
	<br/><br/>U heeft {WEB_TIMESTAMP} verzocht om uw gegevens te wijzigen. Na deze wijzigingen zijn deze als volgt: 
	<br/>Naam: {GSM_NWNAME}
	<br/>Adres: {GSM_NWADRES}
	<br/>E-mail adres: {GSM_NWEMAIL}
	<br/><br/>Activatielink om de wijziging door te voeren:
	<br/><a href = "{GSM_LINK}">{GSM_LINK}</a><br/><br/><br/>Indien u niet gevraagd heeft om uw gegevens te wijzigen, gebruik de activatielink niet en neem onmiddelijk contact op met de {WEB_WEBMASTER}.
	<br/><br/>Met vriendelijke groet, <i> {WEB_WEBMASTER} <br />e-mail {WEB_EMAIL}</i>';
?>