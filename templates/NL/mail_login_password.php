<?php
/*
 * @module     mail template
 */
// include class.secure.php to protect this file and the whole CMS!
if ( defined( 'LEPTON_PATH' ) ) {
  include( LEPTON_PATH . '/framework/class.secure.php' );
} //defined( 'LEPTON_PATH' )
else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while ( ( $level < 10 ) && ( !file_exists( $root . '/framework/class.secure.php' ) ) ) {
    $root .= $oneback;
    $level += 1;
  } //( $level < 10 ) && ( !file_exists( $root . '/framework/class.secure.php' ) )
  if ( file_exists( $root . '/framework/class.secure.php' ) ) {
    include( $root . '/framework/class.secure.php' );
  } //file_exists( $root . '/framework/class.secure.php' )
  else {
    trigger_error( sprintf( "[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER[ 'SCRIPT_NAME' ] ), E_USER_ERROR );
  }
}
// end include class.secure.php
/* change history
 * v20170903 
 * 
 */
    $mail_subject = 'Registration/ Password change';
    $mail_content = 
	'Aan: {GSM_NAME},
	<br/><br/>U heeft {WEB_TIMESTAMP} verzocht om een registratie of een wijziging van Uw wachtwoord.
	<br/><br/>Activatielink om deze registratie te complementeren / wijziging door te voeren:
	<br/><a href = "{GSM_LINK}">{GSM_LINK}</a>
	<br/>
    Via een handmatig process worden, indien daar afspraken over gemaakt zijn, extra rechten toegekend. 
	<br/>Gelieve hiervoor na het inloggen Uw gegevens zoals naam en adres informatie en eventueel het veld extra informatie verder aan te vullen. 
	<br/><br/>Voeg de afzender van deze mail toe aan uw adresboek om een behandeling als ongewenste mail te voorkomen.
	<br/><br/>Indien U geen registratie gevraagd hebt of niet hebt gevraagd om Uw wachtwoord te wijzigen, gebruik de activatielink niet.
	<br/><br/>Met vriendelijke groet, <i> {WEB_WEBMASTER} <br />e-mail {WEB_EMAIL}</i>';
?>