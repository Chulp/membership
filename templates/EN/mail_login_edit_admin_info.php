<?php
/*
 *  @module         Office module Login
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2010-2020 Contracthulp B.V.
 *  @license        see info.php of this module
 *  @platform       see info.php of this module
 */

// include class.secure.php to protect this file and the whole CMS!
if ( defined( 'LEPTON_PATH' ) ) { include( LEPTON_PATH . '/framework/class.secure.php' );
} else { $oneback = "../"; $root = $oneback; $level = 1;
  while ( ( $level < 10 ) && ( !file_exists( $root . '/framework/class.secure.php' ) ) ) {
    $root .= $oneback; $level += 1; } 
  if ( file_exists( $root . '/framework/class.secure.php' ) ) { include( $root . '/framework/class.secure.php' );
  } else { trigger_error( sprintf( "[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER[ 'SCRIPT_NAME' ] ), E_USER_ERROR ); }
}
// end include class.secure.php
/* change history
 * v20200101 version for gsmoffl 4
 */
    $mail_subject = 'for action : changed information {GSM_EMAIL}';
    $mail_content = 
	'Hi {WEB_WEBMASTER},
	<br/><br/>{GSM_NAME} with e-mail address {GSM_EMAIL} has {WEB_TIMESTAMP} requested changes in his/her data. 
	<br/><br/>Currently they are as follows: 
	<br/>Name: {GSM_NAME}
	<br/>Address: {GSM_ADRES}
	<br/>E-mail address: {GSM_EMAIL}	
	<br/><br/>After the change they are as follows: 
	<br/>Name: {GSM_NWNAME}
	<br/>Address: {GSM_NWADRES}
	<br/>E-mail address: {GSM_NWEMAIL}
	<br/><br/>{GSM_NAME} is informed that that the changes will be implementedby {WEB_WEBMASTER}. 
	<br/><br/>Possibly other data has already been changed.
	<br/><br/>Activation link: <a href = "{GSM_LINK}">{GSM_LINK}</a>
	<br/><br/><br/><i> {WEB_WEBMASTER} <br />e-mail {WEB_EMAIL}</i>';
?>