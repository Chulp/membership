<?php
/*
 *  @module         Mail template Office module Login
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2010-2020 Contracthulp B.V.
 *  @license        see info.php of this module
 *  @platform       see info.php of this module
 */

// include class.secure.php to protect this file and the whole CMS!
if ( defined( 'LEPTON_PATH' ) ) { include( LEPTON_PATH . '/framework/class.secure.php' );
} else { $oneback = "../"; $root = $oneback; $level = 1;
  while ( ( $level < 10 ) && ( !file_exists( $root . '/framework/class.secure.php' ) ) ) {
    $root .= $oneback; $level += 1; } 
  if ( file_exists( $root . '/framework/class.secure.php' ) ) { include( $root . '/framework/class.secure.php' );
  } else { trigger_error( sprintf( "[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER[ 'SCRIPT_NAME' ] ), E_USER_ERROR ); }
}
// end include class.secure.php
/* change history
 * v20200101 version for gsmoffl 4
 */
    $mail_subject = 'Registration/ Password change';
    $mail_content = 
	'To : {GSM_NAME},
	<br/><br/>you have {WEB_TIMESTAMP} requested a registration or a password change.
	<br/><br/>Activation link to effectively make the registration/change:
	<br/><a href = "{GSM_LINK}">{GSM_LINK}</a>
	<br/>
    With a manual process, when it has been agreed or you are within the traget group, additional rights are to be assigned. 
	<br/>Please after logging in, provide your details such as name, address and other details for the judgement in such a manual process. 
	<br/><br/>Add the sender of this mail to your addressbook. It may prevent blocking handlings by SPAM filters.
	<br/><br/>If you did not ask for a registration or password change: do not use the activation link.
	<br/><br/>Kind regards, <i> {WEB_WEBMASTER} <br />e-mail {WEB_EMAIL}</i>';
?>