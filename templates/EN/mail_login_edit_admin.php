<?php
/*
 *  @module         Office module Login
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2010-2020 Contracthulp B.V.
 *  @license        see info.php of this module
 *  @platform       see info.php of this module
 */

// include class.secure.php to protect this file and the whole CMS!
if ( defined( 'LEPTON_PATH' ) ) { include( LEPTON_PATH . '/framework/class.secure.php' );
} else { $oneback = "../"; $root = $oneback; $level = 1;
  while ( ( $level < 10 ) && ( !file_exists( $root . '/framework/class.secure.php' ) ) ) {
    $root .= $oneback; $level += 1; } 
  if ( file_exists( $root . '/framework/class.secure.php' ) ) { include( $root . '/framework/class.secure.php' );
  } else { trigger_error( sprintf( "[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER[ 'SCRIPT_NAME' ] ), E_USER_ERROR ); }
}
// end include class.secure.php
/* change history
 * v20200101 version for gsmoffl 4
 */
    $mail_subject = 'Changed information';
    $mail_content = 
	'Aan {GSM_NAME},
	<br/><br/>You have  {WEB_TIMESTAMP} asked to change your data: 
	<br/><br/>Currently they are as follows: 
	<br/>Name: {GSM_NAME}
	<br/>Address: {GSM_ADRES}
	<br/>E-mail address: {GSM_EMAIL}	
	<br/><br/>After the change they are as follows:
	<br/>Name: {GSM_NWNAME}
	<br/>Address: {GSM_NWADRES}
	<br/>E-mail address: {GSM_NWEMAIL}
	<br/><br/>This change will be carried out after some checks by {WEB_WEBMASTER}. You may receive additional questions to confirm the correctness of the data.
	<br/><br/>Eventueel overige gegevens zijn reeds aangepast.
	<br/><br/>If you did not ask to correct your data please contact {WEB_WEBMASTER} 
	<br/><br/>Kind Regards, <i> {WEB_WEBMASTER} <br />e-mail {WEB_EMAIL}</i>';
?>