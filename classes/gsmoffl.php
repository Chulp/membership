<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
class GeneralRoutines extends LEPTON_abstract {
/* * / 		public LEPTON_database $database;
/* * / 		public LEPTON_admin $admin;
/* B  * / 	public $cal 			= array();	/* just for calculation */
/* B * / 	public $cols 			= array();	/* aray with displayable tekst */
/* L */ 	public $field_content 	= array();	/* field contents */
			public $file_ref		= array();	/* array of file references */
			public $memory		 	= array();	/* memory parameters */
			public $page_content 	= array();	/* output parameters for the twig output. */
			public $paging 			= array();	/* paging for more as one datapage */
/* B * / 	public $pdf_data 		= array();	/* pdf data */
			public $setting 		= array();	/* settings array */
			public $user 			= array();	/* usr data array */
			public $version 		= array();	/* array of version numbers of the used modules */
			public $description 	= '';	/* aray with displayable tekst */
			public $nodata 			= 0; 	/* function switch */
/* B * /	public $pdf_header 		= '';	/* */
/* B * /	public $pdf_text		= '';  	/* pdf data */
/* * / 		public $protocol 		= '';  	/* https or http */
			public $recid 			= 0;	/* record id */
			public $search_mysql	= '';	/* selection converted to search parameter */
/* * / 		public $search_mysql9 	= '';	/* selection converted to search parameter */
			public $selection 		= '';  	/* input field selection */
			public $sips 			= false;	/* sips conclusion */

	static $instance;
		
    /* Initialize */
    public function initialize() {
		$this->version ['class'] = "20250210";	
		// !! alternative main functions gsm_XopmaakSel
		/* $this->database = LEPTON_database::getInstance();	
		
		 /* input processing */
		if ( !isset ( $_SESSION [ 'page_h' ] )
			|| !isset ( $_POST[ 'sips' ] )
			|| ( $_SESSION [ 'page_h' ] <> $_POST [ 'sips' ] ) ) $this->sips = true;
			
		/* record id */ 
		if ( isset ( $_POST [ 'recid' ] ) && $_POST [ 'recid' ] > 0 ) { 
			$this->recid = $_POST [ 'recid' ]; 
			unset ( $_GET[ 'recid' ] );
		}
		if ( isset ( $_GET [ 'recid' ] ) && $_GET [ 'recid' ] > 0 ) $this->recid = $_GET [ 'recid' ];
		
		/* system state */ 
		$this->setting [ 'protocol'] = ( isset ( $_SERVER[ 'HTTPS' ] ) && ( $_SERVER [ 'HTTPS' ] == 'on' ) ) ? "https" : "http";
		
		/* system state */ 
		$this->user [ 'ip' ] = $_SERVER[ 'REMOTE_ADDR' ];
		$this->user [ 'device'] = $_SERVER [ 'HTTP_USER_AGENT' ];
		$this->user [ 'privileged' ] = ( isset ( $_SESSION [ 'USER_ID' ] ) && is_numeric ( $_SESSION [ 'USER_ID' ] ) ) ? 1 : 0;
		
		/* reference to taxonomy data */
		$this->file_ref [ 1 ] = LOAD_DBBASE . "_". "taxonomy";
		
		$this->gsm_initTool ();	
	
		/* 	$this->admin = LEPTON_admin::getInstance('Pages','Start',false,false); */	
    }
	
	public function gsm_accessRec ( 
		// ============================	
		&$fieldArr,			/* update fields */
		&$recid,			/* record id */
		$func = 	1, 		/* 1= update (remove unchanged fields) 2= new */
		$name = 	"-",	/* name dB table */
		$detect = 	"gsm_", /* prefix  relevant post fields */
		$mandatory = "id"	/* verplicht veld voor update */
		/* return de fields van het record */
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20250208";  
		if ($this->setting [ 'debug' ] == "---" ) Gsm_debug (array ($fieldArr, $recid, $func , $name, $detect, $mandatory ), __LINE__ . __FUNCTION__ );  	
		$DATfilter  = sprintf ("y{%s;%s;%s}", 
			"0000-00-00", 
			date( "Y", time() )-100 . "-01-01", 
			date( "Y", time() ) +2  . "-12-31"); 
		$AMTfilter = sprintf ("v{%s;%s;%s}", 
			0, 
			-1000000, 
			1000000) ;
		$AANfilter = sprintf ("a{%s;%s;%s}", 
			0, 
			-1000000, 
			1000000) ;
		$TIMfilter = "t{12:00}";	
		$fileref = LOAD_DBBASE . "_" . str_replace ( LOAD_DBBASE . "_" , '',$name);  //gsm 20230101 change
		$okeDel = false; $okeUpd = false; $okeIns = false;
		if ( isset ( $_POST [ $detect . $mandatory ] ) || count ( $fieldArr ) > 2 ) { $okeUpd = true; $okeIns = true; }
		if ( $func == 1 ) { $okeDel = false; $okeUpd = true; $okeIns = false; }
		if ( $func == 2 ) { $okeDel = false; $okeUpd = false; $okeIns = true; }
		if ( $func == 3 ) { $okeDel = true; $okeUpd = false; $okeIns = false; }
		if ( !is_numeric ( $recid ) || strlen ( $recid ) < 1 ) { $okeDel = false; $okeUpd = false; } //	recid leeg
		/* this field must be there in the input to have update or insert */
		if ( !isset ( $_POST [ $detect . $mandatory ] ) ) { $okeUpd = false; }
		$local_array = array ();
		if ( $this->setting [ 'debug' ] == "---" )  Gsm_debug ( array ( 'data' => $fieldArr, 'recid' => $recid, 'func' => $func,  'DB table' => $fileref, 'detect' => $detect, 'verplicht' => $mandatory, 'POST' => $_POST , 'upd' =>$okeUpd, 'ins' =>$okeIns, 'del' =>$okeDel), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );
		/* valid id read record */
		$result = array ( );
		$sql 	= "SELECT * FROM `" . $fileref . "` WHERE `id` = '" . $recid  . "'";
		$sql2 	= "SELECT * FROM `" . $fileref . "` ORDER BY `updated` DESC, `id` DESC LIMIT 1";
		$database->execute_query ( 
			$sql, 
			true, 
			$result );
		if ( count ( $result ) == 0 ) { 
			/*  recid niet ok */
			$okeDel = false; $okeUpd = false; 
			$database->execute_query ( 
				$sql2, 
				true, 
				$result );
		} 
		if ( count ( $result ) == 0 ) return $local_array;
		$local_array = current ( $result );	
		// verwerking input
		if ( !$okeDel ) { 
			foreach ( $_POST as $pay => $load ) { 
				if ( substr ( $pay, 0, 4 ) == $detect && !isset ( $fieldArr [ substr ( $pay, 4 ) ] ) ) {
					$fieldArr [ substr ( $pay, 4 ) ] = htmlentities ( trim ( $load ?? '' ) );
		}	} 	}
		if ( $okeUpd ) { 
			// case of update something changed / remove not changed
			foreach ( $fieldArr as $pay => $load ) { 
				if ( isset ( $local_array [ $pay ] ) && $local_array [ $pay ] == $fieldArr [ $pay] ) unset ( $fieldArr [ $pay ] ); 
		}	}
		// check numeric fields
		$change = array ();
		foreach ( $fieldArr as $pay => $load ) { 
			if ( substr ( $pay, 0, 3 ) == "amt") $change [ $pay ] = $this->gsm_sanitizeStringV ( $load, $AMTfilter );
			if ( substr ( $pay, 0, 3 ) == "aan") $change [ $pay ] = $this->gsm_sanitizeStringV ( $load, $AANfilter );
			if ( substr ( $pay, 0, 3 ) == "dat") $change [ $pay ] = $this->gsm_sanitizeStringD ( $load, $DATfilter );
			if ( substr ( $pay, 0, 3 ) == "tim") $change [ $pay ] = $this->gsm_sanitizeStringD ( $load, "t{12:00}" );
		}
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array ( 'data' => $fieldArr, 'up' => $change ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$fieldArr = array_merge ( $fieldArr, $change );
		$local_array = array_merge ( $local_array, $fieldArr );
		if ( $okeIns )  $fieldArr = $local_array; 
		if ( isset ( $fieldArr [ 'id' ] ) ) unset ( $fieldArr [ 'id' ] );
		if ( !empty ( $fieldArr ) && ( $okeUpd || $okeIns ) ) {	// Is er wat te schrijven
			/* unique test */
			if ( $okeIns ) $recid = 0;
			if ( $okeUpd ) { // update
				$database->build_and_execute ( 
					"update",
					$fileref,
					$fieldArr,
					"`id` = '" . $recid . "'" );
				$result = array ( );
				$database->execute_query ( $sql2, true, $result );
				$local_array = current ( $result );
			}
			if ( $okeIns ) { // insert
				unset ( $fieldArr [ 'updated' ] ); // gsm 20230102 corr 
				$database->build_and_execute ( 
					"insert",
					$fileref,
					$fieldArr );
				// readback
				$result = array ( );
				$database->execute_query ( $sql2, true, $result );
				if ( count ( $result ) > 0 ) {  
					$hulp = current ( $result );
					if ( $mandatory  == 'id' || $fieldArr [ $mandatory ] == $hulp [ $mandatory ] ) $local_array = $hulp; 
					$recid = $local_array [ 'id' ];
			}	}	
		} elseif ( empty ( $fieldArr ) && $okeDel  ) {	
			$database->simple_query ( sprintf ( "DELETE FROM `%s` WHERE `id`= '%s'" , $fileref, $recid ) );
		}
		return $local_array;
	}
	
	public function gsm_accessSql ( 
		// ============================	
		$input, 	// array in, returnvalue in a database Insert or Update. 
		$func = 2   // 1 = for "INSERT INTO `".$table1."` ".$content;  2 =  for "UPDATE `".$table3."` SET ".$content." WHERE .....
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20230808";  
		$part1 = '';
		$part2 = '';
		$first = true;
		$TEMP0 = ' ( %s ) VALUES ( %s ) ';
		$TEMP1 = ' %s ';
		switch ( $func ) { 
			case 1:
				foreach ( $input as $key => $value ) { 
					if ( $first ) { 
						$first = false; $part1 .= "`" . $key . "`";
						if ( $value === "NULL" ) { $part2 .= "NULL"; } else { $part2 .= "'" . $value . "'"; } 
					 } else { 
						$part1 .= ", `" . $key . "`";
						if ( $value === "NULL" ) { $part2 .= ", NULL"; } else { $part2 .= ", '" . $value . "'"; } 
				}	} 
				$local_content = sprintf ( $TEMP0, $part1, $part2 );
				break;
			default:
				foreach ( $input as $key => $value ) { 
					if ( $first ) { 
						$first = false; $part1 .= "`" . $key . "` = ";
						if ( $value === "NULL" ) { $part1 .= "NULL"; } else { $part1 .= "'" . $value . "'";	 } 
					 } else { $part1 .= ", `" . $key . "` = ";
						if ( $value === "NULL" ) { $part1 .= "NULL"; } else { $part1 .= "'" . $value . "'"; } 
				}	 } 
				$local_content = sprintf ( $TEMP1, $part1 );
				break;
		 } 
		return $local_content;
	 } 

	public function gsm_adresDet ( 
		// ============================
		$page_id , 	//	to populate user array, output in $this->user { } 
		$owner	= "XX" 	//	privileged 0 not logged in, 1 logged in, 2 access rights, 3 edit right
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20240207";  
		$this->user [ 'ref' ] = $owner;
		$this->user [ 'id' ] = 0;
		if ( $this->user [ 'privileged' ] > 0 ) { 
			$this->user [ 'id' ] = $_SESSION [ 'USER_ID' ];
			$this->user [ 'group' ] = $_SESSION [ 'GROUPS_ID' ];
			$this->user [ 'groep' ] = $_SESSION [ 'GROUP_NAME' ] [ $_SESSION [ 'GROUPS_ID' ] ];
			/* persoonsgegevens */
			$results = array ( ); // the output buffer
			$TEMPLATE = "SELECT * FROM " . TABLE_PREFIX . "pages WHERE page_id = '%s'";  
			/* check rechten */
			$database->execute_query ( 
				sprintf ( $TEMPLATE, $page_id ), 
				true, 
				$results );
			if ( count ( $results ) > 0 ) { 
				$row = current ( $results );
				$help_admin_groups = explode ( ',', $row [ 'admin_groups' ] );
				$help_viewing_groups = explode ( ',', $row [ 'viewing_groups' ] );
				foreach ( $help_viewing_groups as $key => $value ) { if ( $value == $this->user [ 'group' ] ) $this->user [ 'privileged' ] = 2; } 
				foreach ( $help_admin_groups as $key => $value ) { if ( $value == $this->user [ 'group' ] ) $this->user [ 'privileged' ] = 3; } 
				$results = array ( );
				$TEMPLATE = "SHOW TABLE STATUS LIKE '%s'";
				/* is er extended adres data lees  adresbestand */
				$database->execute_query ( 
					sprintf ( $TEMPLATE, LOAD_DBBASE . '_adres' ), 
					true, $results );
				if ( count ( $results ) > 0 ) { /* login module existing */
					$results = array ( );
					$TEMPLATE = "SELECT * FROM " . LOAD_DBBASE . '_adres' . " WHERE adresid = '%s'";
					// haal extended adresdata op 
					$database->execute_query ( 
						sprintf ( $TEMPLATE, $this->user [ 'id' ] ) , 
						true, 
						$results );
					if ( count ( $results ) > 0 ) { 
						$row = current ( $results );
						if (isset ($this->field_content ) )$this->field_content = $row; 
						$this->user [ 'name' ] = str_replace ( "|", " ", $row [ 'name' ] );
						$this->user [ 'email' ] = $row [ 'email' ];
						$this->user [ 'adres' ] = str_replace ( "|", ", ", str_replace ( ",", " ", $row [ 'adres' ] ) );
						$this->user [ 'ref' ] =	( isset ( $row [ 'ref' ] ) 
							&& isset ( $this->setting [ 'entity' ] [ substr ( $row [ 'ref' ] , 0, 2 ) ] ) )
							? substr ( $row [ 'ref' ] , 0, 2 ) . $row [ 'id' ] 
							: $this->setting [ 'owner' ]. $row [ 'id' ];					
						$this->user [ 'tel' ] = str_replace ( "|", " / ", $row [ 'contact' ] );
						$this->user [ 'comp' ] = $row [ 'comp' ];
					 } 
				 } else { 
					$results = array ( );
					/* haal beperkte adresdata op */
					$TEMPLATE = "SELECT * FROM " . TABLE_PREFIX . "users  WHERE user_id = '%s'";
					$database->execute_query ( 
						sprintf ( $TEMPLATE, $this->user [ 'id' ] ) , 
						true, 
						$results );
					if ( count ( $results ) > 0 ) { 
						$row = current ( $results );
						$this->user [ 'name' ] = $row [ 'display_name' ];
						$this->user [ 'email' ] = $row [ 'email' ];
						$this->user [ 'adres' ] = '';
						$this->user [ 'ref' ] = $this->setting [ 'owner' ];
						$this->user [ 'tel' ] = '';
						$this->user [ 'comp' ] = '';
		}	}	}	} 
		$this->page_content [ 'PRIVILEGED' ] = $this->user [ 'privileged' ];
		return $this->user;
	 } 

	public function gsm_copyFile ( 
		// ============================
		$dir, 
		$func=1,  //1 copy files 2 remove files 3 move files
		$dir_to = '' 
		// ============================
		) {
		$this->version [ __FUNCTION__ ] = "20230808";  
		/* function to remove old directories */
		if ( $func  == 2 ) { 
			if ( is_dir ( $dir ) ) { 
				$files = scandir ( $dir );
				foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) $this->gsm_copyFile ( $dir . "/" . $file, 2 ); } 
				rmdir ( $dir );
			} elseif ( 
				file_exists ( $dir ) ) { unlink ( $dir ); 
			} 
		/* move files */
		} elseif ( $func == 3 ) { 
			if ( !file_exists ( $dir_to ) ) mkdir ( $dir_to, 0777 );
			if ( is_dir ( $dir ) ) { 
				$files = scandir ( $dir );
				foreach ( $files as $file ) { 
					if ( $file != "." && $file != ".." ) {
						if (is_file ( $dir_to . $file ) ) unlink ( $dir_to . $file );
						if (is_file ( $dir . $file ) ) copy ( $dir . $file, $dir_to . $file );
			}	 }	 }	
		} else { 
			/* function to copy files remove all old files */
			if ( file_exists ( $dir_to ) ) { $this->gsm_copyFile ( "$dir_to", 2 ); } // remove old directories if present
			if ( is_dir ( $dir ) ) { 
				mkdir ( $dir_to, 0777 );
				$files = scandir ( $dir );
				foreach ( $files as $file ) { if ( $file != "." && $file != ".." ) $this->gsm_copyFile ( $dir."/".$file, 1, $dir_to. "/". $file ); } 
			} elseif ( file_exists ( $dir ) ) { copy ( $dir, $dir_to ); } 
	} 	} 
	 	
	public function gsm_existDb ( 
		// ============================
		/* create db table table if the file does not yet exist  default or from sql file */
		$project, 		// name sql file
		$name = "-",	// name dB table 
		$prefix = "-"	// default LOAD_DBBASE . "_".
		// ============================
		) { 
		global $database; 
		$returnvalue = '';
		$this->version [ __FUNCTION__ ] = "20240426";   
		$LocalHulpA = ( $name != "-" ) ? $name : $project;
		$fileref = ( $prefix != "-" ) 
			? $prefix . "_" . $LocalHulp 
			: LOAD_DBBASE . "_" . str_replace ( LOAD_DBBASE . "_" , '', $LocalHulpA );
		/* debug * /  Gsm_debug ( array(  $project, $name, $prefix, $returnvalue, $fileref ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$results = array ( );
		// does table exist
		$database->execute_query ( sprintf ( "SHOW TABLE STATUS LIKE '%s'", $fileref ), true, $results );
		if ( count ( $results ) < 1 ) { 
			/* actual addition of DB needed */
			$FileDirS = sprintf ("%s%s%s.sql" , $this->setting [ 'includes' ] , "install/" ,  $fileref);
			if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $FileDirS ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
			if ( file_exists ( $FileDirS ) ) {
				$sql = file_get_contents( $FileDirS );
				$database->simple_query ( $sql );
				$returnvalue .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . __FUNCTION__ .
					' upload '.  $project . ' completed'  . NL;
			} else {
				$sql = "CREATE TABLE IF NOT EXISTS `" . $fileref . "` ( 
					`id` int(11) NOT NULL auto_increment,
					`type` varchar(63) NOT NULL DEFAULT '',
					`ref` varchar(63) NOT NULL DEFAULT '',
					`name` varchar(255) NOT NULL default '',
					`content_short` varchar(255) NOT NULL default '',
					`zoek` varchar (255) NOT NULL default '',
					`active` int(3) NOT NULL DEFAULT '0',			
					`updated` timestamp NOT NULL DEFAULT current_timestamp ON UPDATE current_timestamp,
					PRIMARY KEY (`id`))
					ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
				$database->simple_query ( $sql );	
				$returnvalue .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . __FUNCTION__ .
					' default '.  $fileref . ' installed' . NL;
		}	} 
		return $returnvalue;
	}
	
	public function gsm_existDir ( 
		// ============================
		$directory, // directory to be checked for existence
		$index = false // index to be copied
		// ============================
		) { 
		$returnvalue = "";
		$this->version [ __FUNCTION__ ] = "20240426";  
		$dir = LEPTON_PATH . str_replace ( array( LEPTON_PATH, LEPTON_URL) , '', $directory );
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array( $directory, $dir ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		if ( !file_exists ( $dir ) ) {
			$LocalHulpA = explode ( "/", str_replace ( array( LEPTON_PATH, LEPTON_URL) , '', $directory ) ) ;
			$FileDirS = LEPTON_PATH;
			foreach( $LocalHulpA as $payload ) {
				if ( strlen ( $payload) >0 ) {
					$FileDirS .= "/". $payload;
					if ( !file_exists ( $FileDirS ) ) {
						mkdir ( $FileDirS, 0777 ); 
						$returnvalue .= "<br />" . date ( "H:i:s " ) . __LINE__  . ' dir: '.  $FileDirS . ' created' ;
					}
					if ( $index && !file_exists ( $FileDirS. "index.php" ) ) {
						copy  ( ( dirname ( __FILE__ ) ) . '/index.php' , $FileDirS. "/index.php");
						$returnvalue .= "<br />" .  date ( "H:i:s " ) . __LINE__  . ' index: ' .  $FileDirS . ' created' ;
		}	}	}	}
		return $returnvalue;
	}
	
	public function gsm_guid (
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20230808";  
		if ( function_exists ( 'com_create_guid' ) === true ) { return trim ( com_create_guid ( ) , ' { } ' ); } 
		return sprintf ( '%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand ( 0, 65535 ) , mt_rand ( 0, 65535 ) , mt_rand ( 0, 65535 ) , mt_rand ( 16384, 20479 ) , mt_rand ( 32768, 49151 ) , mt_rand ( 0, 65535 ) , mt_rand ( 0, 65535 ) , mt_rand ( 0, 65535 ) );
	} 
	
	public function gsm_initTaxo ( 
		// ============================	
		// create a set of settings values from taxonomy file
		// format ref
		// key
		// key|gsmoff_
		// key|gsmoff_|n
		$application,
		$privileged,
		$Set_file,
		$group = 'setting'
		// ============================
		) {  
		global $database;
		$this->version [ __FUNCTION__ ] = "20240426";  
		// 20230727 addition of group parameter
		$arrayARY = array( "entity", "allowed" );
		foreach ( $Set_file as $pay => $load ) $this->setting [ str_replace ( "SET_" , "", $pay) ] = $load ;
		$this->gsm_existDb ( $this->file_ref  [ 1 ] );
		/* database present */
		$TEMP1  = "SELECT `ref`, `name` FROM `%s` WHERE `type` LIKE '%s' AND `active` = 1 ORDER BY `zoek` ASC ";
		$results = array ( );
		$database->execute_query ( 
			sprintf ($TEMP1, $this->file_ref  [ 1 ], $group ), 
			true, 
			$results );
		foreach ( $results as $row ) {
			$SET = false;
			$LocalHulp = explode ( "|" , $row [ 'ref' ] );
			if ( isset ( $LocalHulp [ 2 ] ) ) {
				$key = $LocalHulp [ 0 ];
				$app = $LocalHulp [ 1 ];
				$prv = $LocalHulp [ 1 ];
				if ($group == 'setting' ) {
					if ( $LocalHulp [ 1 ] == $application && $LocalHulp [ 2 ] < $privileged ) $this->setting  [ $key ]= $row [ 'name' ];
				} else {
				}
			} elseif ( isset ( $LocalHulp [ 1 ] ) ){
				$key = $LocalHulp [ 0 ];
				$app = $LocalHulp [ 1 ];
				if ($group == 'setting' ) {
					if ( $LocalHulp [ 1 ] == $application ) $this->setting [ $key ] = $row [ 'name' ];
				} else {
					if ( $LocalHulp [ 1 ] == $application ) $this->setting [ $group ] [ $key ] = $row [ 'name' ];
				}
			} else {
				$key = $LocalHulp [ 0 ];
				if ($group == 'setting' ) {
					if ( !isset ( $this->setting [ $key ] ) ) $this->setting [ $key ] = $row [ 'name' ];
				} else {
					if ( !isset ( $this->setting [ $group ] [ $key ] ) ) $this->setting [ $group ] [ $key ]= $row [ 'name' ];
		}	}	}
		foreach ( $arrayARY as $payload) {
			if ( isset ( $this->setting [ $payload ] ) ) {
				if ( !is_array ( $this->setting [ $payload ] ) ) {
					$this->setting [ $payload ] = explode ( "|",$this->setting [ $payload ] );
	}	}	}	}
	
		public function gsm_initTool ( 
		// ============================	
		// create a set of inital values for the twig output
		// ============================
		) {  
//		global $database;
		$this->version [ __FUNCTION__ ] = "20240424";  
		$this->page_content [ 'COLOR' ] = 'blue';
		$this->page_content [ 'ICON' ] = 'settings icon';
		$this->page_content [ 'SUB_HEADER' ] = "____";
		$this->page_content [ 'MESSAGE_CLASS' ] = '';
		$this->page_content [ 'STATUS_MESSAGE' ] = '';
		$this->page_content [ 'MESSAGE' ] = '';
		$this->page_content [ 'FORM_CLASS' ] = '';
		$this->page_content [ 'RETURN' ] = LOAD_RETURN;
		$this->page_content [ 'MODE' ] = 0;
		$this->page_content [ 'DATE' ] = date ( "Y-m-d", time ( ) );
		$this->page_content [ 'TIME' ] = date ( "H:i", time ( ) );
		$this->page_content [ 'HASH' ] = sha1( MICROTIME() . $_SERVER[ 'HTTP_USER_AGENT' ] );
		$this->page_content [ 'RECID' ] = $this->recid;
		$this->page_content [ 'MEMORY' ] = '';
		$this->page_content [ 'DESCRIPTION' ] = '';
		$this->page_content [ 'SELECTION' ] = '';
		$this->page_content [ 'RAPPORTAGE' ] = '';
		$this->page_content [ 'TOEGIFT' ] = '';
		$this->page_content [ 'SIPS' ] = $this->sips;
		$this->page_content [ 'PARAMETER' ] = ( isset( $_POST[ 'selection' ] ) ) ? $_POST[ 'selection' ] : "";
		$this->page_content [ 'PRIVILEGED' ] = $this->user [ 'privileged' ];
		$this->page_content [ 'P1' ] = false;
		$this->page_content [ 'OPTIONAL' ] = '';
		$this->page_content [ 'LOAD' ] = LOAD_MODE;
		
		/* paging */	
		$this->page_content [ 'POSITION' ] = 0;
	}
	
	public function gsm_mail ( 
		// ============================
		$template, 
		$toArr, 
		$parseArr = '', 
		$func=1, 
		$subject='', 
		$content='', 
		$attachments=array ( )
		// ============================
		) { 
		// func 1= mail to persons in toArr
		// func 2= mail to master bcc toArr	
		// tw0 calls to this function adds up the to addreses warning
		$this->version [ __FUNCTION__ ] = "20240426";  
		if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array (	'template' => $template, 'toArr' => $toArr, 'parseArr' => $parseArr, 'func'=> $func, 'subject' => $subject,	'content' => LOAD_OLINE, 'attachments' => $attachments), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$returnvalue='<div class="ui icon message"><i class="inbox icon"></i><div class="content">';
		$oke=true;
		if ( $toArr =='' ) { 
			$oke=false;
			$returnvalue .= '<div class="header">Mail adres missing</div>';
		 } 
		$myMail = new LEPTON_mailer ( ); //lepton version 3 code 
		if ( !LOAD_OLINE ) $returnvalue .= '<h3 class="ui header">Start Mail content</h3>';
		// From
		$myMail->CharSet = DEFAULT_CHARSET;	
		$myMail->setFrom ( $this->setting [ 'droplet' ] [ LANGUAGE. '10' ], $this->setting [ 'droplet' ] [ LANGUAGE. '4' ] );
		$returnvalue .= '<h3 class="ui header">from: '. $this->setting [ 'droplet' ] [ LANGUAGE. '10' ] .'</h3>';
		$myMail->addReplyTo ( $this->setting [ 'droplet' ] [ LANGUAGE. '10' ], $this->setting [ 'droplet' ] [ LANGUAGE. '4' ] );
		// To
		$temp = is_array ( $toArr ) ? $toArr : array ( $toArr ); // ensure data is always array
		switch ( $func ) { 
			case 1:
				//to persons in toArr
				foreach ( $temp as $key => $value ) $myMail->addAddress ( $value );
				$returnvalue .= '<h3 class="ui header">to: '. $value.'</h3>';
				break;
			case 2: //to master bcc to persons in toArr
				$myMail->IsHTML ( true );
			default:
				$myMail->addAddress ( $this->setting [ 'email' ], $this->setting [ 'master' ] );
				foreach ( $temp as $key => $value ) $myMail->addBCC ( $value );
				$returnvalue .= '<h3 class="ui header">to: '. $this->setting [ 'email' ].'</h3>';
				$returnvalue .= '<h3 class="ui header">bcc: '. $value.'</h3>';
				break;
		 } 
		// template ophalen 
		if ( $subject=='' ) { // template is a php file with two fields $mail_subject and $mail_content
			include ( $this->setting [ 'frontend' ]."templates/" . LANGUAGE ."/". $template );
		 } else { 
			$mail_subject = $subject;
			$mail_content = $content;
		 } 
		$mail_subject = Gsm_prout ( $mail_subject , $parseArr ); // The Subject of the message.
		$myMail->Subject =  $mail_subject;
		$mail_content = Gsm_prout ( $mail_content , $parseArr );
		$myMail->Body = $mail_content; // Clients that can read HTML will view the normal Body.
		$textbody = wordwrap ( $mail_content, 60, '<br />', true );
		$textbody = strip_tags ( $textbody  ); 
		$textbody = str_replace ( "\t","",$textbody ); 
		while ( strpos ( $textbody,"\n\n\n" ) !== false ) $textbody = str_replace ( "\n\n\n","\n\n",$textbody ); 
		while ( strpos ( $textbody,"\r\n\r\n\r\n" ) !== false ) $textbody = str_replace ( "\r\n\r\n\r\n","\r\n\r\n",$textbody ); 
		$myMail->AltBody = $textbody; // This body can be read by mail clients that do not have HTML email
		$returnvalue .= '<h3 class="header">re: '. $mail_subject.'</h3>';
		$returnvalue .= '<h3 class="ui header">tx: </h3>'. $mail_content;	
		$myAtt = is_array ( $attachments ) ? $attachments : array ( $attachments ); // ensure data is always array
		// Attachments 
		if ( count ( $myAtt ) ) { 
			foreach ( $myAtt as $key => $value ) { 
				$mail->addAttachment ( $key, $value ); 
				$returnvalue .= '<h3 class="ui header">att: '. $value. '</h3>';
		 }	 } 
		if ( !LOAD_OLINE || !$oke ) { 
			$returnvalue .= '<div class="header"> Not mailed !! </div></div></div>';
			if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array (	$returnvalue, $oke ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		 } else { 
			if ( !$myMail->send ( ) ) { 
				$returnvalue .= sprintf ( '<div class="ui icon message"><i class="notched circle loading icon"></i><div class="content"><div class="header">Not mailed !! </div><p>%s</p></div></div>', $myMail->ErrorInfo );
			}	else  {
				$returnvalue = "";
			}
		} 
		return $returnvalue; 
	 }	
			 
	public function gsm_memorySaved (
		// ============================
		$func = 1 		// 1= retrieve  2 pack and save  3 pack for post
		// ============================
		) { 
		/* *
		 *  Read an save data $this->memory [ 1 ] ~ $this->memory [ 5 ]
		 *	location collectdir
		 *  func 1 retrieve from post or session relevant directory
		 *  func 2 compact the data and transfer via $Post command
		 *  func 3 compact the data save for the module during the session
		 *  
		 */
		$this->version [ __FUNCTION__ ] = "20240510";  
		$FileDirS = sprintf ( "%s%s/%s/%s_%s" , LEPTON_PATH, $this->setting [ 'collectdir' ], date ( "Y-m-d", time ( ) ), $this->setting [ 'owner' ], session_id ( ) );
		$FileMEMORY = sprintf ( "%s/%s.html", $FileDirS , $this->page_content [ 'MODULE' ] );
		if ($func == 1) {
			if ( isset ( $_POST [ 'memory' ] ) &&  substr ( $_POST [ 'memory' ], 0, 5 ) == "Begin" ) { 
				$this->memory = explode ("|" , $_POST[ 'memory' ] );
			} else {
				if ( file_exists ( $FileMEMORY ) ) { 
					$FileData = file ( $FileMEMORY ); 
					$this->memory = explode ( "|" , $FileData [ 0 ] );
				} else { 
					// case after midnight
					$FileDirS = sprintf ( "%s%s/%s/%s_%s" , LEPTON_PATH, $this->setting [ 'collectdir' ], date ( "Y-m-d", strtotime ( "yesterday" ) ), $this->setting [ 'owner' ], session_id ( ) );
					$FileMEMORY = sprintf ( "%s/%s.html", $FileDirS , $this->page_content [ 'MODULE' ] );
					if ( file_exists ( $FileMEMORY ) ) { 
						$FileData = file ( $FileMEMORY ); 
						$this->memory = explode ( "|" , $FileData [ 0 ] );
			}	}	}
			if ( isset ( $this->memory [ 6 ] ) && $this->memory [ 6 ] != "Einde" ) $this->description .= $this->language [ 'TXT_ERROR_MEMORY' ] ;
			for ( $i = 1; $i < 6 ; $i++ ) { if ( !isset ( $this->memory [ $i ] ) ) $this->memory [ $i ] = 0; }
			$FileData = $FileData [ 0 ] ?? '';
		} else {
			$FileData = sprintf ( "Begin|%s|%s|%s|%s|%s|Einde", $this->memory [ 1 ] ?? '', $this->memory [ 2 ] ?? '', $this->memory [ 3 ] ?? '', $this->memory [ 4 ] ?? '', $this->memory [ 5 ] ?? ''); 
			if ( $func == 3) {
				$this->gsm_existDir ( $FileDirS );
				file_put_contents ( $FileMEMORY, $FileData );
		}	}
		return $FileData;
	}
	
	public function gsm_opmaakSel ( 
		// ============================
		$funcArr,  			// view save add delete print reset Select
		$parameter = "-",	// parameter bij select
		$pdf = "-",  		// file name pdf file
		$pos = "-",  		// position in de file
		$aantal = "-",  	// afmeting van de file 
		$max = "-", 		// max per pagina
		$text = "-",   		// test bij select / parameter invoer
		$dir = "-"   		// pdf directory
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20240426";  
		$temp = is_array ( $funcArr ) ? $funcArr : array ( $funcArr );
		$temp_para	= ( $parameter != "-" ) ? $parameter : "";
		$temp_pdf	= ( strlen ($pdf) > 3 ) ? $pdf : "";
		$temp_pos	= ( $pos != "-" ) ? intval ( $pos ) : 0;
		$temp_aant	= ( $aantal != "-" ) ? intval ( $aantal ) : 0;
		$temp_max	= ( $max != "-" ) ? intval ( $max ) : 0;
		$temp_text	= ( $text != "-" ) ? $text : "select";
		$temp_dir	= ( $dir != "-" ) 
			? LEPTON_URL . $dir . $temp_pdf 
			: sprintf ( "%s%s/%s/pdf/%s", LEPTON_URL, MEDIA_DIRECTORY, LOAD_MODULE, $pdf );
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $temp, $parameter, $pdf, $aantal, $max, $text, $temp_dir ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$TEMPL [ 0 ] = '<button class="%1$s ui button" name="command" value="%2$s" type="submit">%3$s</button>';
		$TEMPL [ 1 ] = '<div class="ui action input">
						<button class="%1$s ui button" name="command" type="submit" value="%2$s" type="submit">%3$s</button>
						<input type="text" name="selection" value="%4$s" placeholder="Parameter..." /><i class="info icon" data-tooltip="%5$s"><i class="info circle icon"></i></i>
					</div>';
		$TEMPL [ 2 ] = '<a target="125" href="%2$s"><img src="%3$s" alt="pdf document">%1$s</a>';
		$TEMPL [ 3 ] = '<div class="fields">
						<div class="field">
							<button class="ui button" name="command" value="down"><i class="angle left icon"></i></button>
						</div>
						<div class="field">
							<input type="hidden" name="n0" value="%1$s" />
							<input type="text" name="n1" size="3" value="%1$s" />
							<input type="hidden" name="n2" value="%3$s" />
						</div>
						<div class="field">
							<button class="ui basic button">tot %2$s van %3$s</button>
						</div>
						<div class="field">
							<button class="ui button" name="command" value="up"><i class="angle right icon"></i></button>
						</div>
					</div>';
		$temp_pos = $temp_pos + 1;
		$temp_end = $temp_pos + $temp_max -1 ;
		if ( $temp_end > $temp_aant ) $temp_end = $temp_aant;
		$returnvalue = '';
		if (in_array( 1, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "primary",	"View" , 	$this->language [ 'tbl_icon' ] [ 1 ] ); 
		if (in_array( 2, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "primary",	"Save" , 	$this->language [ 'tbl_icon' ] [ 4] ); 
//		if (in_array( 3, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "positive",	"Add" , 	$this->language [ 'tbl_icon' ] [ 3 ] ); 
//		if (in_array( 4, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "negative",	"Delete" , 	$this->language [ 'tbl_icon' ] [ 2 ] );
		if (in_array( 5, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "basic",		"Print" , 	$this->language [ 'tbl_icon' ] [ 11 ] );
		if (in_array( 6, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "negative",	"Reset" , 	$this->language [ 'tbl_icon' ] [ 2 ] );
		if (in_array( 7, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "primary",	"Proces" , 	$this->language [ 'tbl_icon' ] [ 21 ] );
		if (in_array( 8, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "positive",	"New" , 	$this->language [ 'tbl_icon' ] [ 5 ] );
		if (in_array( 9, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "",			"Set" , 	$this->language [ 'tbl_icon' ] [ 18 ] );
		if (in_array( 10,$temp ) ) $returnvalue .= sprintf ( $TEMPL [ 1 ], "secondary",	"Select" , 	$this->language [ 'tbl_icon' ] [9], 
			$temp_para , 
			$temp_text ).NL.NL;
		if (in_array( 11, $temp ) && strlen ( $temp_pdf ) > 3 ) 
			$returnvalue .= sprintf ( $TEMPL [ 2 ], 
				$temp_pdf, 
				$temp_dir, 
				sprintf ( "%s/modules/%s%s/img/pdf_16.png", LEPTON_URL, LOAD_MODULE, LOAD_SUFFIX ) );
		if (in_array( 13, $temp ) && $temp_aant > $temp_max ) 
			$returnvalue .= sprintf ( $TEMPL [ 3 ], 
				$temp_pos, 
				$temp_end, 
				$temp_aant ).NL.NL; 
		return $returnvalue;
	}

	public function gsm_pagePosition ( 
		// ============================	
		// handling the paging $position = $this->page_content [ 'position' ]
		$func,			// up of down of sql
		$position, 		// position
		$aantal = 0,	// aantal records
		$max = 60,		// max setting
		$n0 = 0,		// return
		$n1 = 0			// return
		// ============================
		) {  
		global $database; 
		$this->version [ __FUNCTION__ ] = "20250208"; 
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  "func" => $func, "pos" => $position, "aantal" => $aantal, "max" => $max, "oud pos" => $n0, "new pos" => $n1 ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
 		$returnvalue = 0;
		switch ( $func ) {
			case "up":
				if ( $_POST[ 'n0' ] == $_POST[ 'n1' ]) {
					$position = $_POST[ 'n0' ] + $max  -1;
				} else {
					$position = $_POST[ 'n1' ] -1;
				}
				if ( $aantal > 0 && $position > ( $aantal + 1) ) $position  = 0;
				$returnvalue = $position;
				break;
			case "down":
				if ( $_POST[ 'n0' ] == $_POST[ 'n1' ]) {
					$position = $_POST[ 'n0' ] - $max -1;
				} else {
					$position = $_POST[ 'n1' ] -1;
				}
				if ( $position < 0 ) $position  = 0;
				$returnvalue = $position;
				break;
			case "sql":
				$returnvalue = "";
				if ( $position > $aantal ) $position = 0;
				if ( $aantal > $max ) $returnvalue = sprintf (" LIMIT %s, %s ", $position, $max );
				break;
		}
		return $returnvalue;
	}
		
	public function	gsm_pinCode ( 
		// ============================
		$input , 
		$func = 1, 
		$update = array ( )
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20240510";  
		if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array(  $input, $func, $update, $this->setting [ 'collectdir' ], $this->setting [ 'owner' ], session_id ( ) ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		// toegangs controle tfa 
		$returnvalue = false;
		$UserVolg = is_array ( $input ) ? $input : array ( $input );  // eerste parameter is mailadres tweede parameter is the name
		$job = is_array ( $update ) ? $update : array ( $update ); // jobs to execute when verification code is entered
		$FileDirS = sprintf ( "%s%s/%s/%s_%s" , LEPTON_PATH, $this->setting [ 'collectdir' ], date ( "Y-m-d", time ( ) ), $this->setting [ 'owner' ], session_id ( ) );
		$FileTFA = sprintf ( "%s/tfa.html", $FileDirS);
		$FilePAY = sprintf ( "%s/payload.html", $FileDirS);
		$FileINDto = sprintf ( "%s/index.php", $FileDirS);
		$FileINDfrom = sprintf ( "%s%s/index.php" , LEPTON_PATH, MEDIA_DIRECTORY );
		$Verif_string = "verificatiecode %s";
		switch ( $func ) { 
			case 1: // create file return true;
				/* create directory collectdir : owner sessionID
				 * if tfa file present get back the seq of verification number (first element 0) end
				 * create tfa seq , key, time, email, name
				 * add index file
				 * is er een job ? create payload file 
				 * send mail to gsm_mail en web_mail
				 */
				 $this->gsm_existDir ( $FileDirS );
				if ( file_exists ( $FileTFA ) ) { 
					// existing file pick up file return part 1 = seq number
					$tfa = file ( $FileTFA );
					$LhulpD = explode ( "|", current ( $tfa ) );
					$returnvalue = $LhulpD [ 0 ];
					// the input is the verification code ?? 
					if ( $UserVolg [ 0 ] == $LhulpD [ 1 ] ) {
						$returnvalue = true;
						// get he payload fle and execute the jobs
						if ( file_exists ( $FilePAY ) ) { 
							$job = file ( $FilePAY );
							if ( isset ( $job ) && count ( $job ) > 0 ) { 
								foreach ( $job as $key => $query ) $database->simple_query ( $query );
							 } 
							unlink ( $FilePAY );
						} 
						// remove blocking item and job
						if ( file_exists ( $FileTFA ) ) unlink ( $FileTFA );
						if ( file_exists ( $FileINDto ) ) unlink ( $FileINDto );
					}
				 } else { 
					// email mandatory
					$UserVolg [ 0 ] = $this->gsm_sanitizeStringS ( $UserVolg [ 0 ] , "s{EMAIL}" );
					// create file and mail
					if ( $UserVolg [ 0 ] !== false ) {
						$LhulpD = date ( 'U' );
						$LhulpE = implode ( "|", $UserVolg ); 
						$returnvalue = sprintf ( $Verif_string, substr ( $LhulpD, -2 ) );
						$payload = sprintf ( "%s|%s|%s|%s" , $returnvalue , substr ( $LhulpD, -6,5 ) , $LhulpD, $LhulpE );
						file_put_contents ( $FileTFA , $payload );
						// index file for extra safety
						if ( file_exists ( $FileINDfrom ) ) copy ( $FileINDfrom , $FileINDto );
						// write job
						if ( count ( $job ) > 0 ) foreach ( $job as $pay => $load ) file_put_contents ( $FilePAY, $load . "\r\n" , FILE_APPEND | LOCK_EX );
						// mail
						$LhulpF = explode ( "|", $payload );
						$parseArr = array ( );
						$parseArr [ 'GSM_EMAIL' ] = $LhulpF [ 3 ];
						$parseArr [ 'GSM_NAME' ] = $LhulpF [ 4 ] ?? $LhulpF [ 3 ];
						$parseArr [ 'WEB_MASTER' ] = $this->setting [ 'master' ];
						$parseArr [ 'WEB_EMAIL' ] = $this->setting [ 'email' ];
						$parseArr [ 'WEB_SITE' ] = LEPTON_URL;
						$parseArr [ 'GSM_LINK' ] = sprintf ( "%s : %s " , $LhulpF [ 0 ] , $LhulpF [ 1 ] );
						$parseArr [ 'WEB_TIMESTAMP' ] = date ( "Y-m-d H:i" );
						$template="mail_verificatie.php";
						$this->description .= $this->gsm_mail ( $template, $parseArr [ 'GSM_EMAIL' ] , $parseArr );
						$this->description .= $this->gsm_mail ( $template, $parseArr [ 'WEB_EMAIL' ] , $parseArr );
					}
				} 
				break;
			case 2: // existing file pick up file return part 1 = seq number
				$returnvalue = '';
				if ( file_exists ( $FileTFA ) ) { 
					$tfa = file ( $FileTFA );
					$LhulpF = explode ( "|", current ( $tfa ) );
					$returnvalue = $LhulpF [ 0 ];
				 } 
				break;
			case 3: // get payload
				if ( file_exists ( $FileTFA ) ) { 
					$tfa = file ( $FileTFA );
					$LhulpF = explode ( "|", current ( $tfa ) );
					$returnvalue = $LhulpF;
				}
				break;
			case 4: // mail again
				if ( file_exists ( $FileTFA ) ) { 
					$tfa = file ( $FileTFA );
					$LhulpF = explode ( "|", $tfa );
					$parseArr = array ( );
					$parseArr [ 'GSM_EMAIL' ] = $LhulpF [ 3 ];
					$parseArr [ 'GSM_NAME' ] = $LhulpF [ 4 ] ?? $LhulpF [ 3 ];
					$parseArr [ 'WEB_master' ] = $this->master;
					$parseArr [ 'WEB_EMAIL' ] = $this->standard_email;
					$parseArr [ 'WEB_SITE' ] = LEPTON_URL;
					$parseArr [ 'GSM_LINK' ] = sprintf ( "%s : %s " , $LhulpF [ 0 ] , $LhulpF [ 1 ] );
					$parseArr [ 'WEB_TIMESTAMP' ] = date ( "Y-m-d H:i" );
					$template="mail verificatie.php";
					$this->description .= $this->gsm_mail ( $template, $parseArr [ 'GSM_EMAIL' ] , $parseArr );
					$this->description .= $this->gsm_mail ( $template, $parseArr [ 'WEB_EMAIL' ] , $parseArr );
				}
				break;	
			case 9: // remove files
				$returnvalue = '';
				if ( file_exists ( $FilePAY ) )	unlink ( $FilePAY );
				if ( file_exists ( $FileTFA ) )	unlink ( $FileTFA );
				if ( file_exists ( $FileINDto ) ) unlink ( $FileINDto );
				break;
		}
		return $returnvalue;
	}
	
	public function gsm_print ( 
		// ============================	
		$query = "-", 
		$project = "-", 
		$selection = "-", 
		$func = 1, 
		$loc ="-"
		// ============================
		) { 
		global $database;
		$returnvalue ='';
		$this->version [ __FUNCTION__ ] = "20240810";  
		if ( $func > 4 ) {
			require_once ( $this->setting [ 'includes' ] . 'classes/' . 'pdf2.inc' );
		} else {
			require_once ( $this->setting [ 'includes' ] . 'classes/' . 'pdf.inc' );
		}
		$pdf = new PDF ( );
		global $owner;
		$owner = $this->setting [ 'owner' ];
		global $title;
		$title = ucfirst ( $project );
		$location = ( $loc == '-' )
			? sprintf ( '%s/%s/pdf' , MEDIA_DIRECTORY, LOAD_MODULE )
			: $loc; // gsm 20230501
		$run = date ( DATE_FORMAT, time ( ) ) . " " . date ( TIME_FORMAT, time ( ) );
		$check = $this->setting [ 'includes' ] . $this->page_content [ 'MODULE' ] . '_print' . $func . '.php';
		$checkv = $this->setting [ 'includes' ] . "v" . substr( $this->page_content [ 'MODULE' ],1 ) . '_print' . $func . '.php';
		$check_legacy = $this->setting [ 'includes' ] . $this->page_content [ 'MODULE' ] . '_data' . $func . '.inc';
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $query, $project, $selection, $func, $loc, $owner,$title, LEPTON_PATH, MEDIA_DIRECTORY , LOAD_MODULE), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );
		if ( file_exists ( $check ) ) { 
			require_once ( $check ); 
			$pdflink = sprintf ( "%s%s/%s", LEPTON_PATH, $location, $this->setting [ 'pdf_filename' ] );  
			$pdf->Output ( $pdflink, 'F' );
			return $this->setting [ 'pdf_filename' ];
		/* support backend */
		} elseif ( file_exists ( $checkv ) ) { 
			require_once ( $checkv ); 
			$pdflink = sprintf ( "%s%s/%s", LEPTON_PATH, $location, $this->setting [ 'pdf_filename' ] ); 
			$pdf->Output ( $pdflink, 'F' );
			return $this->setting [ 'pdf_filename' ];
		/* legacy */
		} elseif ( file_exists ( $check_legacy ) ) { 
			require_once ( $check_legacy ); 
			$this->description .= date ( "H:i:s " ) . __LINE__  . ' legacy : '.  $check_legacy  . NL;	
			$pdflink = sprintf ( "%s%s/%s", LEPTON_PATH, $location, $this->setting [ 'pdf_filename' ] );  // gsm 20230501
			$pdf->Output ( $pdflink, 'F' );
			return $this->setting [ 'pdf_filename' ];
		/* end legacy */
		} else {
			$this->description .= date ( "H:i:s " ) . __LINE__  . ' missing : '.  $check  . NL;	
			return $returnvalue;
		}	
	}

	public function gsm_sanitizeStringD ( 
		// ============================
		$input, 	// gsm_sanitizeStringD ( $value, 'y{1970-01-01;1940-01-01;2018-03-11}' )
		$filter 	// gsm_sanitizeStringD ( $value, "t{12:00}" )
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20240426"; 
		if ( $this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $input, $filter ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		if ( !preg_match ( '#(y|t|f)#i', $filter, $match ) ) { 
			$message = $this->language [ 'TXT_ERROR_DATA' ] . "Filter: <b>" . htmlentities ( $filter ) . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
			die ( $message );
		 } 
 
		$temp = is_array ( $input ) ? $input : array ( $input );
		$filter_type = strtolower ( $match [ 1 ] );
		switch ( $filter_type ) { 
			case 'y':// date filter ( $input can be single value or array ) // standard 20200202
				// gsm_sanitizeStringD ( "10-3-2018", 'y{1970-01-01;1940-01-01;2018-03-11}' ) 
				// gsm_sanitizeStringD ( 00:00, "t{12:00}" );
				// check if optional filter values are supplied ( default, min, max ) 
				// dates > 100 years ago are not supported
				$advanced_filter = ( preg_match ( '#(y)\{([-0-9]+);([-0-9]+);([-0-9 ]+)\}#i', $filter, $match ) );
				$datephp8byp = -1600000000; $datephp8bypYMD = date ( "Y-m-d", $datephp8byp );
				if ( $advanced_filter ) { 
					if ($match [ 2 ] < date ( $datephp8bypYMD ) ) $match [ 2 ] = $datephp8bypYMD;
					$default =	strtotime ( $match [ 2 ] );
					$lowerlimit = strtotime ( $match [ 3 ] );
					$upperlimit = strtotime ( $match [ 4 ] );
				} 
				// loop over input values
				foreach ( $temp as $key => $value ) { 
					// force date conversion
					if ($temp [ $key ] < $datephp8bypYMD ) 	$temp [ $key ] = $datephp8bypYMD;
					$originalDate = strtotime ( $temp [ $key ] );		
					// check if value is within min/max range, if not use default value
					if ( $advanced_filter ) { 
						$originalDate = ( $originalDate >= $lowerlimit && $originalDate <= $upperlimit ) ? $originalDate : $default;			
					 } 
					// to required format
					$temp [ $key ] = ( $originalDate > $datephp8byp ) ? date ( "Y-m-d", $originalDate ) : "0000-00-00";	
				} 
				break;
			case 't':// time filter ( $input can be single value or array ) // standard 20200202
				// gsm_sanitizeStringD ( 00:00, "t{12:00}" );
				// loop over input values
				$advanced_filter = ( preg_match ( '#(t)\{([-0-9]+):([-0-9]+)\}#i', $filter, $match ) );
				foreach ( $temp as $key => $value ) { 
					// force date conversion split based on 
					$temp [ $key ] = preg_replace( '/[^0-9h:.,]/i', '', $temp [ $key ]);	
					$temp [ $key ] = preg_replace( '/[h:.,]/i', ':', $temp [ $key ]);
					$lcHulp = explode ( ":" , $temp [ $key ] );
					if ( isset ($lcHulp [0]) && is_numeric ($lcHulp [0])) {
						$lcHulp [0] = Substr ( "00" . $lcHulp [0] % 24, -2);
					} else {
						$lcHulp [0] = $match [2] ;
					}	
					if ( isset ($lcHulp [1]) && is_numeric ($lcHulp [1])) {
						$lcHulp [1] = Substr ( "00" . $lcHulp [1] % 60, -2);
					} else {
						$lcHulp [1] = $match [3] ;
					}	
					$temp [ $key ] = sprintf ("%s:%s", $lcHulp [0], $lcHulp [1] );
				} 
				break;
			case 'f':// date opmaak 
				if ( !preg_match ( '#(f)\{(.+)\}#i', $filter, $match ) ) {
					$message =  $this->language [ 'TXT_ERROR_DATA' ] . "String filter: <b>" . htmlentities ( $filter ) . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
					die ( $message );
				} 
				// get filter options from regular expression
				$filter_options = strtoupper ( $match [ 2 ] );
				// strings
				$monthUK = array ( "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" );
				$dayUK =  array ("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" );
				$monthNL = array ( "jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "oct", "nov", "dec" );
				$dayNL = array ("Zondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag" );
				// loop over input values
				foreach ( $temp as $key => $value ) { 
					if ( strpos ( $filter_options, 'MAAND' ) !== false ) { 
						$temp [ $key ] = date ("d M Y" ,strtotime ( $temp [ $key ] ) ); 
						$temp [ $key ] = str_replace( $monthUK, $monthNL, $temp [ $key ] );
					}
					if ( strpos ( $filter_options, 'DAG' ) !== false ) { 
						$temp [ $key ] = date ("D, d M Y" ,strtotime ( $temp [ $key ] ) ); 
						$temp [ $key ] = str_replace( $monthUK, $monthNL, $temp [ $key ] );
						$temp [ $key ] = str_replace( $dayUK, $dayNL, $temp [ $key ] );
					}
					if ( strpos ( $filter_options, 'D1' ) !== false ) { 
						$temp [ $key ] = date ("D" ,strtotime ( $temp [ $key ] ) ); 
						$temp [ $key ] = str_replace( $dayUK, $dayNL, $temp [ $key ] );
					}
				}
				break;
		} 
		$returnvalue = is_array ( $input ) ? $temp : $temp [ 0 ];
		return $returnvalue;
	}	

	public function gsm_sanitizeStringNA (
		// ============================
		$func=1, 
		$input='', 
		$input0='', 
		$input1='', 
		$input2='', 
		$input3=''
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20230808";  
		switch ( $func 	) { 
			case '1': //default val initial field for name and adres
				$returnvalue="|||";
				break;	
			case '2': //string naar array
				$temp = explode ( "|" , $input0 );
				$returnvalue = array ( );
				for ( $x = 0; $x < 4; $x++ ) { $y=$x+1; $returnvalue [ $input.$y ] = isset ( $temp [ $x ] ) ? $temp [ $x ] : ''; } 
				break;
			case '3': //string naar string
				$returnvalue = sprintf ( "%s|%s|%s|%s", $input0, $input1, $input2, $input3 );
				break;
			case '4': //remove separator
				$returnvalue = str_replace ( " ", " ", str_replace ( "|"," ", $input ) );
				break;		
			default: 
				$message = $this->language [ 'TXT_ERROR_DATA' ] . "Func: <b>" . $func . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
				die ( $message );
				break;	
		 } 
		return $returnvalue;
	 } 
	
	public function gsm_sanitizeStringS ( 
		// ============================
		$input,		// gsm_sanitizeStringS ( $value, "s{TAGS|TOASC|VINP|VOUT|CLEAN|TRIM|EMAIL|NAME|FILE|PATH|SCHEMA|STRIP|DATUM|KOMMA|STOP|WHOLE|FULL|EURT|EURO|E128|STRONG}" )
		$filter 
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20250108";
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $input, $filter ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		if ( !preg_match ( '#(s)#i', $filter, $match ) ) { 
			$message = $this->language [ 'TXT_ERROR_DATA' ] . "Filter: <b>" . htmlentities ( $filter ) . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
			die ( $message );
		}  
		$temp = is_array ( $input ) ? $input  : array ( $input );
		$filter_type = strtolower ( $match [ 1 ] );
		
		switch ( $filter_type ) { 
			case 's': // string filter 
				if ( !preg_match ( '#(s)\{(.+)\}#i', $filter, $match ) ) {
					$message =  $this->language [ 'TXT_ERROR_DATA' ] . "String filter: <b>" . htmlentities ( $filter ) . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
					die ( $message );
				} 
				// get filter options from regular expression
				$filter_options = strtoupper ( $match [ 2 ] );
				// loop over input values
				foreach ( $temp as $key => $value ) { 
					if ( $temp [ $key ] === NULL ) $temp [ $key ] = '';
					if ( strpos ( $filter_options, 'TAGS' ) !== false ) $temp [ $key ] = str_replace ( "<br />", "\n", $temp [ $key ] );
					if ( strpos ( $filter_options, 'STRIX' ) !== false ) $temp [ $key ] = html_entity_decode ( $temp [ $key ] );				
					if ( strpos ( $filter_options, 'STRIP' ) !== false ) $temp [ $key ] = strip_tags ( html_entity_decode ( $temp [ $key ] ) );	
					if ( strpos ( $filter_options, 'TOASC' ) !== false ) { 
						$bad = array ( chr ( 160 ), ';', '"', "'" , "|" ,"^", "`");
						$temp [ $key ] = str_replace ( $bad, '', $temp [ $key ] );
						ini_set('mbstring.substitute_character', "none");
						$temp [ $key ] = mb_convert_encoding ( $temp [ $key ], 'UTF-8', 'UTF-8');
						$temp [ $key ]  = preg_replace('/[^a-zA-Z0-9\?\/-_.,!@#{}$%&*<=>:() ]/', '', $temp [ $key ]);	
						$temp [ $key ] = iconv("UTF-8", "ASCII", $temp [ $key ] ); 
					} 	
/* 	
					if ( strpos ( $filter_options, 'VINP' ) !== false ) 
						$temp [ $key ] = htmlspecialchars ( nl2br ( $temp [ $key ] ) );
					if ( strpos ( $filter_options, 'VOUT' ) !== false ) 
						$temp [ $key ] = strip_tags ( htmlspecialchars_decode ( $temp [ $key ] ) );
 */
					if ( strpos ( $filter_options, 'LOWER' ) !== false ) 
						$temp [ $key ] = strtolower ( trim ( $temp [ $key ] ) );				
					if ( strpos ( $filter_options, 'CLEAN' ) !== false ) 
						$temp [ $key ] = str_replace ( '  ', ' ', str_replace ( array ( '|', ';', '"', "'" ) , " ", $temp [ $key ] ) );
					if ( strpos ( $filter_options, 'TRIM' ) !== false ) $temp [ $key ] = trim ( $temp [ $key ] );
					if ( strpos ( $filter_options, 'EMAIL' ) !== false ) { 
						$temp [ $key ] = strtolower ( trim ( $temp [ $key ] ) );
						if ( empty ( $temp [ $key ] ) ) { $temp [ $key ] = false;
						 } elseif ( !filter_var ( $temp [ $key ] , FILTER_VALIDATE_EMAIL ) ) { $temp [ $key ] = false; } } 
					if ( strpos ( $filter_options, 'NAME' ) !== false ) { 
						$hulp = explode ( "@", $temp [ $key ] );
						$hulp2 = explode ( ".", $hulp [ 0 ] );
						$temp [ $key ] ='';
						foreach ( $hulp2 as $val1a => $val1b ) $temp [ $key ] .= ucfirst ( $val1b ) . " "; } 
					if ( strpos ( $filter_options, 'FILE' ) !== false ) { 
						$bad = array_merge ( array_map ( 'chr', range ( 0,31 ) ) , array ( " ", "&", ";", "'", "<", ">", ":", '"', "/", "\\", "|", "?", "*" ) );
						$temp [ $key ] = str_replace ( $bad, "_", $temp [ $key ] ); } 
					if ( strpos ( $filter_options, 'PATH' ) !== false ) { 
						$temp [ $key ] = str_replace ( '\\', '/', $temp [ $key ] ); } 
					if ( strpos ( $filter_options, 'SCHEMA' ) !== false ) { 
						$hlp1 = round ( $temp [ $key ] * 100 );
						if ( $hlp1 % 100 > 0 ) { 
							if ( $hlp1 % 10 > 0 ) { 
								$temp [ $key ] = number_format ( $temp [ $key ] , 2, '.', '' );
							 } else { $temp [ $key ] = number_format ( $temp [ $key ] , 1, '.', '' ); } 
						 } else { $temp [ $key ] = number_format ( $temp [ $key ] , 0, '.', '' ); } 
					 } 
					if (strpos ($filter_options, 'KLASSE' ) !== false) { 
						$temp [ $key ] = str_replace ("0", '', $temp [ $key ] );
					$temp [ $key ] = substr ( $temp [ $key ], 0, 1 );	}	
					if ( strpos ( $filter_options, 'DATUM' ) !== false ) { 
						$temp [ $key ] = date ("d M Y" ,strtotime ( $temp [ $key ] ) ); 
						$monthUK = array ( "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" );
						$monthNL = array ( "januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "october", "november", "december" );
						$temp [ $key ] = str_replace( $monthUK, $monthNL, $temp [ $key ] );}
					if ( strpos ( $filter_options, 'KOMMA' ) !== false ) { $temp [ $key ] = number_format ( $temp [ $key ] , 2, ',', '' ); } 
					if ( strpos ( $filter_options, 'KOM1' ) !== false ) { $temp [ $key ] = number_format ( $temp [ $key ] , 1, ',', '' ); } 
					if ( strpos ( $filter_options, 'STOP' ) !== false ) { $temp [ $key ] = number_format ( $temp [ $key ] , 2, '.', '' ); } 
					if ( strpos ( $filter_options, 'WHOLE' ) !== false ) $temp [ $key ] = floor ( intval ( $temp [ $key ] ) ); 
					if ( strpos ( $filter_options, 'FULL' ) !== false ) { $temp [ $key ] = number_format ( $temp [ $key ] , 2, ',', '.' ); } 
					if ( strpos ( $filter_options, 'EURT' ) !== false ) { $temp [ $key ] = "Euro " . $temp [ $key ]; } 
					if ( strpos ( $filter_options, 'EURO' ) !== false ) { $temp [ $key ] = "€ " . $temp [ $key ]; } 
					if ( strpos ( $filter_options, 'E128' ) !== false ) { $temp [ $key ] = chr(128)." " . $temp [ $key ]; } 
					if ( strpos ( $filter_options, 'STRONG' ) !== false ) { $temp [ $key ] = "<strong>" . $temp [ $key ] . "</strong>"; } 

				}		
				break;	
		 } 
		$returnvalue = is_array ( $input ) ? $temp : $temp [ 0 ];
		return $returnvalue;
	}
	
	public function gsm_sanitizeStringV ( 
		// ============================
		$input, 	// gsm_sanitizeStringV ( $value, "v{0;-1000;10000}" )
		$filter 	// gsm_sanitizeStringV ( $value, "a{0;-1000;10000}" )
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20240426";
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array(  $input, $filter ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		if ( !preg_match ( '#(v|a)#i', $filter, $match ) ) {
			$message = $this->language [ 'TXT_ERROR_DATA' ] . "Filter: <b>" . htmlentities ( $filter ) . "</b> is invalid.". " ( " . __LINE__ . __FUNCTION__ . $this->version." ) </br>";
			die ( $message );
		} 
		$temp = is_array ( $input ) ? $input : array ( $input );
		$filter_type = strtolower ( $match [ 1 ] );
		switch ( $filter_type ) { 
			case 'v': // amount filter ( always 2 decimals with separator ) ( $input can be single value or array ) 
				// check if optional filter values are supplied ( default, min, max ) 
				$advanced_filter = ( preg_match ( '#(v)\{([-.0-9]+);([-.0-9]+);([-.0-9]+)\}#i', $filter, $match ) ); 
				// loop over input values
				foreach ( $temp as $key => $value ) { 
					// decimal komma replaces decimale punt
					$plus = true;
					if ( strpos ( $value, '-' ) !== false ) $plus = false;
					$dotPos = strrpos ( $value, '.' );
					if ( $dotPos === false ) $dotPos = strrpos ( $value, '_' );
					$commaPos = strrpos ( $value, ',' );
					$sep = ( ( $dotPos > $commaPos ) && $dotPos ) 
						? $dotPos 
						: ( ( ( $commaPos > $dotPos ) && $commaPos ) 
							? $commaPos 
							: false ); 
					if ( !$sep ) { 
						$temp [ $key ] = floatval ( preg_replace ( "/[^0-9]/", '', $value ) ); 
					} else {
						$temp[$key] = preg_replace ( "/[^0-9]/", '', substr ( $value, 0, $sep ) ) . '.' . preg_replace ( "/[^0-9]/", '', substr ($value, $sep+1, strlen ( $value ) ) );
					}
					if ( !$plus ) $temp [ $key ] = $temp [ $key ] * -1;				
					if ( $advanced_filter ) { 
						$temp [ $key ] = ( $temp [ $key ] >= $match [ 3 ] && $temp [ $key ] <= $match [ 4 ] ) ? $temp [ $key ] : $match [ 2 ];
					 } 
					$temp [ $key ] = number_format ( round ( $temp [ $key ] , 2 ) , 2, '.' , '' ); 
				} 
				break;
			case 'a': // amount filter ( ) ( $input can be single value or array ) 
				// check if optional filter values are supplied ( default, min, max ) 
				$advanced_filter = ( preg_match ( '#(a)\{([-.0-9]+);([-.0-9]+);([-.0-9]+)\}#i', $filter, $match ) ); 
				// loop over input values
				foreach ( $temp as $key => $value ) { 
					// decimal komma replaces decimale punt
					$plus = true;
					if ( strpos ( $value, '-' ) !== false ) $plus = false;
					$dotPos = strrpos ( $value, '.' );
					if ( $dotPos === false ) $dotPos = strrpos ( $value, '_' );
					$commaPos = strrpos ( $value, ',' );
					$sep = ( ( $dotPos > $commaPos ) && $dotPos ) 
						? $dotPos 
						: ( ( ( $commaPos > $dotPos ) && $commaPos ) 
							? $commaPos 
							: false ); 
					if ( !$sep ) { 
						$temp [ $key ] = floatval ( preg_replace ( "/[^0-9]/", '', $value ) ); 
					} else {
						$temp[$key] = preg_replace ( "/[^0-9]/", '', substr ( $value, 0, $sep ) ) . '.' . preg_replace ( "/[^0-9]/", '', substr ($value, $sep+1, strlen ( $value ) ) );
					}
					if ( !$plus ) $temp [ $key ] = $temp [ $key ] * -1;				
					if ( $advanced_filter ) { 
						$temp [ $key ] = ( $temp [ $key ] >= $match [ 3 ] && $temp [ $key ] <= $match [ 4 ] ) ? $temp [ $key ] : $match [ 2 ];
					 } 
					$temp [ $key ] = number_format ( round ( $temp [ $key ] , 0 ) , 0, '.' , '' ); 
				} 
				break;
		 } 
		$returnvalue = is_array ( $input ) ? $temp : $temp [ 0 ];
		return $returnvalue;
	}

	public function gsm_scanDir ( 
		// ============================
		$dir_from, 		// 
		$func = 1, 		// 
		$qty = 50, 		// 
		$allow_sub = 'all', 	//
		$allow_ext = 'all', 	//
		$ref = '' 		//
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20240426";  
		$fileArray = array ( );
		$exept_all = 0;
		$dir_from = LEPTON_PATH. str_replace ( LEPTON_PATH, "", $dir_from);
		if ( !is_array ( $allow_sub ) || $allow_sub == 'all' ) $exept_all = 1;
		if ( !is_array ( $allow_sub ) || $allow_sub != 'all' ) $exept_all = 2;
		if ( is_array ( $allow_sub ) ) $exept_all = 0;
		if  ($allow_ext == 'all' || $allow_ext == '') { $allow_ext = $this->setting [ 'allowed' ];
		} else { $allow_ext = is_array ( $allow_ext ) ? $allow_ext : array ( $allow_ext ); }
		if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array ( "func" => $func, "dir" => $dir_from, "qty" => $qty, "allow_sub" => $allow_sub, "allow_ext" =>  $allow_ext, 	"except_all" => $exept_all, "ref" => $ref ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$n = 0;
		if ( is_dir ( $dir_from ) ) { 
			$dirs = array ( $dir_from );
			while ( NULL !== ( $dir = array_pop ( $dirs ) ) ) { 
				if ( $handle = opendir ( $dir ) ) { 
					while ( false !== ( $file = readdir ( $handle ) ) ) { 
						if ( $file == '.' || $file == '..' ) continue;
							$path = $dir . '/' . $file;
							$ext = strtolower ( substr ( $file, strrpos ( $file, '.' ) + 1 ) );
							if ( is_dir ( $path ) && is_readable ( $path ) && $exept_all == 1 ) { array_unshift ( $dirs, $path );
							 } elseif ( is_dir ( $path ) && is_readable ( $path ) && $exept_all == 2 ) { // do nothing
							 } elseif ( is_dir ( $path ) && is_readable ( $path ) && in_array ( $file, $allow_sub ) ) { array_unshift ( $dirs, $path );
							 } elseif ( in_array ( $ext, $allow_ext ) ) { 
								if ( $qty > 0 && $n < $qty ) { $fileArray [ $file ] = $path; $n++; 
								 } else { 
									$hulpref=explode ( "_", $file ); 
									$h1 = strtoupper ( $hulpref [ 0 ] );
									if ( is_array ( $ref )){
										if (in_array ( substr ( $h1, 0, 2 ) , $ref )) { $fileArray [ $file ] = $path; $n++; }
									} else {
										if ( strtoupper ( $ref ) == $h1 ) { $fileArray [ $file ] = $path; $n++; }
										if ( strtoupper ( $ref ) == ( substr ( $h1, 0, 2 ) ) ) { $fileArray [ $file ] = $path; $n++; 
										} elseif ( strtoupper ( $ref ) == strtoupper( substr ( $file, 0, strlen ( $ref ) ) ) ) { $fileArray [ $file ] = $path; $n++; } 
									}// 2021-06-09
					 }	 }	 }	 } 
					closedir ( $handle );
			}	 } 
			if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array ( $fileArray ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
			if ( $func == 1 ) return $fileArray;
			krsort ( $fileArray );
			if ( $func == 2 ) return $fileArray;
			if ( $func == 3 ) { 
				$returnvalue = '';
				$nodisplay = true;
				$height = " ";
				$prev = '';
				$clear = $this->language [ 'layout' ] [ 'show1' ] ?: '';
				$full = false;
				if ( count ( $fileArray ) >0 ) $returnvalue .= $this->language [ 'layout' ] [ 'show0' ] ?: '';
				if ( $this->setting [ 'jpgwidth' ] < 201 ) $full = true;
				foreach ( $fileArray as $key => $value ) { 
					$ext = strtolower ( substr ( $key, strrpos ( $key, '.' ) ) );
					if ( $ext == ".pdf" ) { 
						if ( $nodisplay || $prev == ".jpg" ) $returnvalue .= $clear;
						$keyh = str_replace ( ".pdf", "", $key );
						$returnvalue .= '<p><a class="pdf" href="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' " target= "_124"> ' . $keyh . ' </a></p>';
						$nodisplay = false;
						$prev = ".pdf"; 
					 } elseif ( $ext == ".zip" ) { 
						if ( $nodisplay || $prev == ".jpg" ) $returnvalue .= $clear;
						$keyh = str_replace ( ".zip", "", $key );
						$returnvalue .= '<p><a class="zip" href="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' "> ' . $keyh . '</a></p>';
						$nodisplay = false;
						$prev = ".zip"; 
					 } elseif ( $ext == ".mp4" ) { 
						if ( $nodisplay || $prev == ".jpg" ) $returnvalue .= $clear;
						$keyh = str_replace ( ".mp4", "", $key );
						$returnvalue .= '<p><a class="mp4" href="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' "> ' . $keyh . '</a></p>';
						$nodisplay = false;
						$prev = ".mp4"; 
					 } elseif ( $ext == ".html" ) { 
						$returnvalue .= $clear;
						ob_start ( );
						include $value;
						$returnvalue .= ob_get_clean ( );
						$returnvalue .= $clear;
						$nodisplay = false;
						$prev = ".html"; // skip no data message
					 } elseif ( $ext == ".jpg" ) { 
						if ( $nodisplay || $prev != ".jpg" ) $returnvalue .= $clear;
						$keyh = str_replace ( ".jpg", "", $key );
						if ( $full ) { $returnvalue .= '<a href="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' "><img src="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' " width="' . $this->setting [ 'jpgwidth' ] . '" height="' . $height . '" target="_124"/></a>';
					 } else { $returnvalue .= '<p><img src="' . str_replace ( LEPTON_PATH, LEPTON_URL, $value ) . ' " width="' . $this->setting [ 'jpgwidth' ] . '" height="' . $height . '" vspace="5"/></p>'; } 
					$nodisplay = false;
					$prev = ".jpg"; 
				 }	
			 } 
			if ( count ( $fileArray ) >0 ) $returnvalue .= $this->language [ 'layout' ] [ 'show9' ] ?: '';
			return $returnvalue;
	}	 } 
	
	public function gsm_selectOption ( 
		// ============================
		$inputArr, 		// array in optionally select one return is the result array
		$select = '-', 
		$func = 1 , 	// func = 1 key value, 2 = part of value 3 = value value empty = 1 add / 2 add selected 4 dropdownvalue  5 dropdown key
		$empty = '' 
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20240426";  // function 4 en 5 added
		$input = is_array ( $inputArr ) ? $inputArr : explode ( "|" ,$inputArr );
		if ($this->setting [ 'debug' ] == "---" )  Gsm_debug ( array ( "func" => $func, "input" => $input, "select" => $select, "empty" => $empty ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		if ( $select == '' ) $select = '-';
		$layOut [ 0 ] = '<option value="%s" >%s</option>';
		$layOut [ 1 ] = '<option value="%s" selected >%s</option>';
		$layOut [ 4 ] = '<div class="menu"><div class="item">';
		$layOut [ 5 ] = '</div><div class="item">';
		$layOut [ 6 ] = '</div></div>';
		$returnvalue = '';
		switch ( $func ) { 
			case 1:
			case 2:
			case 3:
				$tobeDone = true;
				foreach ( $input as $key => $value ) { 
					$pos = strpos ( $value, sprintf ( "%s", $select ) );
					if ( $tobeDone && $func == 1 && ( $select == $key || $select == $value ) ) { 
						$returnvalue .= sprintf ( $layOut [ 1 ] , $key, $value ); $tobeDone = false;
					 } elseif ( $tobeDone && $func == 2 && $pos !== false ) { 
						$returnvalue .= sprintf ( $layOut [ 1 ] , $key, $value ); $tobeDone = false; 
					 } elseif ( $tobeDone && $func == 3 && $pos !== false ) { 
						$returnvalue .= sprintf ( $layOut [ 1 ] , $value, $value ); $tobeDone = false; 
					 } else { $returnvalue .= sprintf ( $layOut [ 0 ] , ( $func == 3 ) ? $value : $key, $value ); } 
				} 
				if ( $tobeDone && strlen ( $empty ) > 1 ) 
					$returnvalue = sprintf ( $layOut [ 1 ], "-", $empty ) . $returnvalue;
				break;
			case 4:
				$returnvalue = $layOut [ 4 ]. implode ( $layOut [ 5 ] , $input ) . $layOut [ 6 ];
				break;
			case 5:
				$returnvalue = $layOut [ 4 ]. implode ( $layOut [ 5 ] , array_keys ( $input ) ) . $layOut [ 6 ];
				break;
		}
		return $returnvalue;
	}
		
	public function gsm_uploadFile ( 
		// ============================	
		$location = "-",   	// location 
		$format = "-", 		// filename / format
		$descr = "-", 		// filename
		$recid = "-", 		// volgnummer / recid
		$type = "-", 		// parameter in filenaam
		$user = false 		// prefix van user
		// ============================
		) { 
		$returnvalue = false;
		$this->version [ __FUNCTION__ ] = "2024042";  
		if ($this->setting [ 'debug' ] == "yes" )  Gsm_debug ( array ( $location, $format, $descr, $recid, $type, $user, $this->setting ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		
		/* controle velden */
		$location = ( $location == "-" ) ? $location = LEPTON_PATH . '/media' : LEPTON_PATH . str_replace ( LEPTON_PATH, "", $location );
		if ( $format == "-" ) $format = '%2$s%5$s';
		if ( $recid == "-" ) $recid = $this->recid ?? 0;
		$recid = ( $recid != '' && is_numeric ( $recid ) && $recid > 0 ) ? $recid : '';
		$prefix = ( $user && strlen ( $this->user [ 'ref' ] ) > 1 ) ? substr ( $this->user [ 'ref' ] ,0 ,2 ) : $this->setting [ 'owner' ] ;
		$this->gsm_existDir ( $location , true );
		$oke=false;
		if ( is_numeric( $recid ) && isset ( $_FILES [ 'doc_uploaded' ][ 'error' ] ) && $_FILES[ 'doc_uploaded' ][ 'error' ] == 0 ) {
			/* there is an attachment and we know how to store it */
			$help = explode( ".", $_FILES[ 'doc_uploaded' ][ 'name' ] );
			$extension = end ( $help );
			if ( in_array ( $extension, $this->setting [ 'allowed' ] ) && $_FILES[ 'doc_uploaded' ][ 'size' ] < $this->setting [ 'size' ] ) $oke = true; // file test
			if ( $oke ) {
				if ( $type == "-" ) $type ='';
				if ( $descr == "-" ) $descr = $help[0];
				if ( $descr == "-" && isset ( $_POST[ 'bijl_oms' ] ) ) $descr = strtolower( $_POST[ 'bijl_oms' ] );
				$guid6	= strtolower( substr( $this->gsm_guid(), 0, 6 ) );	
				$filename = sprintf ( $format, 
/* 1 */				$prefix, 
/* 2 */				$recid, 
/* 3 */				"_".date( "Y-m-d"). $guid6 ,
/* 4 */				$type ,
/* 5 */				$descr);
				$filename = str_replace( "-", "_",str_replace( " ", "_", $filename . "." . $extension ));
				$filename = $this->gsm_sanitizeStringS ( $filename , "s{FILE}" ); 
				move_uploaded_file( $_FILES[ 'doc_uploaded' ][ 'tmp_name' ], $location . $filename ) ;
				$returnvalue = date ( "H:i:s " ) . __LINE__  . " Uploaded : ". $filename.NL;
				unset ( $_FILES[ 'doc_uploaded' ][ 'error' ]);
		}  } 
		return $returnvalue;
	}
	
}

class gsmoffl extends GeneralRoutines {

	public function gsm_opmaakSel2 ( 
		// ============================
		$funcArr,  			// view save add delete print reset Select
		$parameter = "-",	// parameter bij select
		$pdf = "-",  		// file name pdf file
		$pos = "-",  		// position in de file
		$aantal = "-",  	// afmeting van de file 
		$max = "-", 		// max per pagina
		$text = "-"   		// test bij select / parameter invoer
		// ============================
		) { 
		$this->version [ __FUNCTION__ ] = "20250208";  
		$temp = is_array ( $funcArr ) ? $funcArr : array ( $funcArr );
		$temp_para	= ( $parameter != "-" ) ? $parameter : "";
		$temp_pdf	= ( strlen ($pdf) > 3 ) ? $pdf : "";
		$temp_pos	= ( $pos != "-" ) ? intval ( $pos ) : 0;
		$temp_aant	= ( $aantal != "-" ) ? intval ( $aantal ) : 0;
		$temp_max	= ( $max != "-" ) ? intval ( $max ) : 0;
		$temp_text	= ( $text != "-" ) ? $text : "select";
/* debug * / Gsm_debug ( array(  $temp, $parameter, $pdf, $aantal, $max, $text ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$TEMPL [ 0 ] = '<button class="%1$s ui button" name="command" value="%2$s" type="submit">%3$s</button>';
		$TEMPL [ 1 ] = '<div class="ui action input">
						<button class="%1$s ui button" name="command" type="submit" value="%2$s" type="submit">%3$s</button>
						<input type="text" name="selection" value="%4$s" placeholder="Parameter..." /><i class="info icon" data-tooltip="%5$s"><i class="info circle icon"></i></i>
					</div>';
		$TEMPL [ 2 ] = '<a target="_blank" href="%2$s"><img src="%3$s" alt="pdf document">%1$s</a>';
		$TEMPL [ 3 ] = '<div class="fields">
						<div class="field">
							<button class="ui button" name="command" value="down"><i class="angle left icon"></i></button>
						</div>
						<div class="field">
							<input type="hidden" name="n0" value="%1$s" />
							<input type="text" name="n1" size="3" value="%1$s" />
							<input type="hidden" name="n2" value="%3$s" />
						</div>
						<div class="field">
							<button class="ui basic button">tot %2$s van %3$s</button>
						</div>
						<div class="field">
							<button class="ui button" name="command" value="up"><i class="angle right icon"></i></button>
						</div>
					</div>';
		$temp_pos = $temp_pos + 1;
		$temp_end = $temp_pos + $temp_max -1 ;
		if ( $temp_end > $temp_aant ) $temp_end = $temp_aant;
		$returnvalue = '';
		/* mail specifiek */
		if (in_array( 23, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "primary",	"View" , 	$this->language [ 'tbl_icon' ] [ 17 ] );	
		/* end specifiek */
		if (in_array( 1, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "primary",	"View" , 	$this->language [ 'tbl_icon' ] [ 1 ] ); 
		if (in_array( 2, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "primary",	"Save" , 	$this->language [ 'tbl_icon' ] [ 4] ); 
//		if (in_array( 3, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "positive",	"Add" , 	$this->language [ 'tbl_icon' ] [ 3 ] ); 
		if (in_array( 4, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "negative",	"Remove" , 	$this->language [ 'tbl_icon' ] [ 6 ] );
		if (in_array( 5, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "basic",		"Print" , 	$this->language [ 'tbl_icon' ] [ 11 ] );
		if (in_array( 6, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "negative",	"Reset" , 	$this->language [ 'tbl_icon' ] [ 2 ] );
		/* mail specifiek */
		if (in_array( 22, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "basic",	"Test" , 	"Test" );
		if (in_array( 21, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "positive",	"Mail" , 	"Mail" );	
		/* end specifiek */
		if (in_array( 7, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "primary",	"Proces" , 	$this->language [ 'tbl_icon' ] [ 17 ] );
		if (in_array( 8, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "positive",	"New" , 	$this->language [ 'tbl_icon' ] [ 5 ] );
		if (in_array( 9, $temp ) ) $returnvalue .= sprintf ( $TEMPL [ 0 ], "",			"Set" , 	$this->language [ 'tbl_icon' ] [ 18 ] );
		if (in_array( 10,$temp ) ) $returnvalue .= sprintf ( $TEMPL [ 1 ], "secondary",	"Select" , 	$this->language [ 'tbl_icon' ] [ 9 ], 
			$temp_para , 
			$temp_text ).NL.NL;
		if (in_array( 11,$temp ) && strlen ( $temp_pdf ) > 3 ) 
			$returnvalue .= sprintf ( $TEMPL [ 2 ], 
				$temp_pdf, 
				sprintf ( "%s%s/%s/pdf/%s", LEPTON_URL, MEDIA_DIRECTORY, LOAD_MODULE, $pdf ), 
				sprintf ( "%s/modules/%s%s/img/pdf_16.png", LEPTON_URL, LOAD_MODULE, LOAD_SUFFIX ) );
		if (in_array( 13,$temp ) && $temp_aant > $temp_max ) 
			$returnvalue .= sprintf ( $TEMPL [ 3 ], 
				$temp_pos, 
				$temp_end, 
				$temp_aant ).NL.NL; 
		return $returnvalue;
	}
	
	public function gsm_persoonInfo ( 
		// ===========================
		$filter, 
		$input_fields = '',
		$user_fields = '' 
		// ============================
		) { 
		$temp = '';
		$this->version [ __FUNCTION__ ] = "20230808";   
		if ( count ( $input_fields ) < 2 ||  count ( $this->field_content )<2 ) {
			$this->page_content [ 'MODE' ] = 0;
			return $temp;
		}
		$input = ( is_array ( $input_fields ) ) ? $input_fields : $this->field_content;
		$user = ( is_array ( $user_fields ) ) ? $user_fields : $this->user;	
/* debug * / Gsm_debug ( array ( 'filter' => $filter, 'input' => $input, 'user' => $user ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		switch ($filter) {	
			case 'a': 
				$temp = '<h4 class="ui dividing header">ADRES LABEL</h4>';
				$lHulpA = explode ("|" , $input [ 'adres' ] );
				if (strlen ( $input [ 'adres' ] ) < 8 ) $lHulpA [0] .= $this->language [ 'TXT_ERROR_ADRES' ];
				$lHulpB = str_replace ("|" , " ", $input [ 'name' ] );
				if (strlen ( $lHulpB ) < 8 ) $lHulpB [0] .= $this->language [ 'TXT_ERROR_ADRES' ];
				if ( strlen ( $lHulpA [0] ) > 1 ) { 
					$temp .= ( isset ( $lHulpA [0] ) ) ?  $lHulpA [0] : '';
					$temp .= NL . "f.a.o. " .  $lHulpB ;
					$temp .= ( isset ( $input [ 'contact' ] ) ) ? " (tel:" . str_replace ( "tel", '', $input [ 'contact' ] ) . ")" : '' ;
					$temp .= ( isset ( $lHulpA [1] ) ) ? NL . $lHulpA [1] : '';
					$temp .= ( isset ( $lHulpA [2] ) ) ? NL . $lHulpA [2] : '';
					$temp .= ( isset ( $lHulpA [3] ) && strlen ( $lHulpA [3] ) >1 ) ? NL . $lHulpA [3] : '';
					$temp .= ( isset ( $lHulpA [4] ) && strlen ( $lHulpA [4] ) >1 ) ? NL . $lHulpA [4] : '';
				} else {
					$temp .= $lHulpB;
					$temp .= ( isset ( $lHulpA [1] ) ) ? NL . $lHulpA [1] : '';
					$temp .= ( isset ( $lHulpA [2] ) ) ? NL . $lHulpA [2] : '';
					$temp .= ( isset ( $lHulpA [3] ) && strlen ( $lHulpA [3] ) >1 ) ? NL . $lHulpA [3] : '';
					$temp .= ( isset ( $lHulpA [4] ) && strlen ( $lHulpA [4] ) >1 ) ? NL . $lHulpA [4] : '';
					$temp .= ( isset ( $input [ 'contact' ] ) ) ? NL. "Tel:" . str_replace ( "tel", '', $input [ 'contact' ] ) : '';
				}
				break;
			case 'b': 
				// Aantekening
				$temp = '<h4 class="ui dividing header">DETAILS</h4>';
				/* debug * / Gsm_debug ( array(  $this->field_content ), __LINE__ . __FUNCTION__ ); /* debug */
				$temp .= ( strlen( $input [ 'ref0' ] ) > 2 
					&& isset ( $this->language [ 'TXT_ADRES' ] [ 'REF0' ] )) 
					? NL .$this->language [ 'TXT_ADRES' ] [ 'REF0' ]. $input [ 'ref0' ] 
					: '';
				$temp .= ( $input [ 'dat1' ] != "1970-01-01" 
					&& $input [ 'dat1' ] != "0000-00-00" 
					&& isset ( $this->language [ 'TXT_ADRES' ] [ 'DAT1' ] ) ) 
					? NL . $this->language [ 'TXT_ADRES' ] [ 'DAT1' ] . $input ['dat1' ] 
					: '';
				$temp .= ( $input [ 'dat1' ] != "1970-01-01" 
					&& $input [ 'dat2' ] > $this->field_content [ 'dat1' ] 
					&& isset ( $this->language [ 'TXT_ADRES' ] [ 'DAT2' ] ) )  
					? NL . $this->language [ 'TXT_ADRES' ] [ 'DAT2' ]. $input ['dat2' ] 
					: '';
				$temp .= ( $input [ 'dat0' ] != "1970-01-01" 
					&& $input [ 'dat0' ] != "0000-00-00" 
					&& isset ( $this->language [ 'TXT_ADRES' ] [ 'DAT0' ] ) ) 
					? NL . $this->language [ 'TXT_ADRES' ] [ 'DAT0' ] .  $input [ 'dat0' ] 
					: '';
				$temp .= ( $input [ 'ref1' ] != NULL 
					&& strlen( $input [ 'ref1' ] )  > 2  
					&& isset ( $this->language [ 'TXT_ADRES' ] [ 'REF1' ] ) )
					? NL . $this->language [ 'TXT_ADRES' ] [ 'REF1' ] .   $input [ 'ref1' ] 
					: '';
				$temp .= ( $input [ 'ref2' ] != NULL 
					&&  strlen( $input [ 'ref2' ] ) > 2  
					&& isset ( $this->language [ 'TXT_ADRES' ] [ 'REF2' ] ) ) 
					? NL . $this->language [ 'TXT_ADRES' ] [ 'REF2' ] .   $input [ 'ref2' ] 
					: '';
				$temp .= ( $input [ 'content_short' ] != NULL 
					&& strlen( $input [ 'content_short' ] ) > 2 
					&& isset ( $this->language [ 'TXT_ADRES' ] [ 'INFO' ] ) ) 
					? NL . $this->language [ 'TXT_ADRES' ] [ 'INFO' ] .  NL . $this->gsm_sanitizeStringS ( $input [ 'content_short' ], "s{STRIP}" )
					: '';
				if ( strlen ( $temp ) < 10 ) $temp = '';
				break;	
			case 'c': 
			// References
				$temp = '<h4 class="ui dividing header">REFERENCES</h4>';
				if ( $user [ 'privileged' ] > 0  && $user [ 'ref' ] ==  $input [ 'ref' ] ) $temp .= $this->user [ 'groep' ] . " (". $user [ 'group' ] . ")". NL;
				$temp .= ' Ref : ' . $input [ 'ref' ] . ' ( ' . $input [ 'id' ] . ' - ' . $input [ 'adresid' ] . ')';
				$temp .= ( isset (  $this->language [ 'TXT_NOTE' ][ $input ['type'] ] ) ) 
					? NL . "Contact method : " . $this->language [ 'TXT_NOTE' ][ $input ['type'] ] 
					: '';
				$temp .= ( $input [ 'comp' ] > 1 ) 
					? NL . $this->language [ 'TXT_COMP' ][ $input [ 'comp' ] ] 
					: '';
				$temp .= NL . "Device : " . $this->user [ 'device' ];
				$temp .= NL. "IP : " . $this->user [ 'ip' ];
				if ( strlen ( $temp ) < 100 ) {
					$temp = '';
					$temp .= NL . 'Ref : ' . $input [ 'ref' ];
				}
				break;
		}
		return $temp;
	}
	
	public function gsm_preloadDataL ( 
		// ============================
		$filter, 
		$input=''		
		// ============================
		) {  
		global $database;
		$this->version [ __FUNCTION__ ] = "20230818";  
		/* debug * / Gsm_debug ( array ( $filter, $input ,$this->file_ref ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		// check if a valid filter was supplied
		if (!preg_match('#(t)#i', $filter, $match)) {
			die ( sprintf ( 'Oeps unexpected case. Filter: <b>%s</b> is invalid ( %s-%s )</br>', 
				htmlentities($filter), 
				__LINE__, 
				$this->version ) );
		}
		if ($input == ''){ $temp=array(); } else { $temp = is_array($input) ? $input : array($input);}
		$filter_type = strtolower($match[1]);
		switch ($filter_type) {
		case 't': 
			// check for correct string filter: s{key}
			if (!preg_match('#(t)\{(.+)\}#i', $filter, $match)) {
				die ( sprintf ( 'Oeps unexpected case. String filter: <b>%s</b> is invalid ( %s-%s )</br>', 
					htmlentities($filter), 
					__LINE__, 
					$this->version ) );
			}
			// create array with list values from regular expression
			$filter_options = strtoupper ( $match [ 2 ] );
			// t{GROUP}
			// $groupArr = array( '0'=> "onbekend");
			// $groupArr = $gsmclass->gsm_preloadTableT ( "t{GROUP}", $groupArr );
			if (strpos($filter_options, 'GROUP') !== false) {
				$results = array();
				$database->execute_query (
					"SELECT * FROM `".TABLE_PREFIX."groups`", 
					true,
					$results );
				if ( empty ( $results ) )
					die ( sprintf ( 'Oeps unexpected case. String option: <b>%s</b> is invalid ( %s-%s )</br>', 
					$filter_options, 
					__LINE__, 
					$this->version ) );
				foreach ($results as $row) $temp[ $row[ 'group_id' ] ] = $row[ 'name' ];
			}
			
			if (strpos($filter_options, 'REF') !== false) {
				$fields = 'ref';
				$results = array();
				$database->execute_query (
					"SELECT " . $fields . " FROM `" . $this->file_ref  [ 99 ] . "` ORDER BY `ref`", 
					true,
					$results );
				if ( count($results) > 0 ){
					foreach ( $results as $row ) {
						$hulp = substr ( $row [ 'ref' ], 0, 2);
						$temp [ $hulp ] = $hulp;
					}
				}
			}	
			if (strpos($filter_options, 'STAND') !== false) {
				$fields = 'ref';
				$results = array();
				$database->execute_query (
					"SELECT " . $fields . " FROM `" . $this->file_ref  [ 98 ] . "` ORDER BY `ref`", 
					true,
					$results );
				if ( count($results) > 0 ){
					foreach ( $results as $row ) {
						$hulp = substr ( $row [ 'ref' ], 0, 4);
						if (strlen ( trim ( $hulp ) ) >2 ) $temp [ $hulp ] = $hulp;
					}
				}
			}							
			break;
		}
		return $temp;
	} 

	
	

	public function gsm_registerProcess ( 
		// ============================
		$username='', 
		$password='' 
		// ============================
		) {  
		global $database;
		$this->version [ __FUNCTION__ ] = "20230808";  
		$temp = '';
		$display_name = $this->gsm_sanitizeStringS ( $_POST [ 'username' ], "s{NAME}" );
		$results = array ( );	
		// create user record if not existing
		$database->execute_query ( 
			sprintf ( "SELECT * FROM `%s` WHERE `email` = '%s'", $this->file_ref  [ 97 ], $username ) , 
			true, 
			$results );
		if ( count ( $results ) == 0 ) {
			// Insert user record 
			$hulpArr = array (
				'group_id' => FRONTEND_SIGNUP,
				'groups_id' => FRONTEND_SIGNUP,
				'active' => 1,
				'statusflags' => 6,
				'username' => $username,
				'password' => password_hash ( TABLE_PREFIX . $username, PASSWORD_DEFAULT ),
				'display_name' => $display_name,
				'language' => 'NL',
				'home_folder' => '',
				'email' => $username 
			);
			$database->build_and_execute (
				"insert",
				$this->file_ref  [ 97 ],
				$hulpArr );
			$results = array( );
			$database->execute_query ( 
				sprintf ( "SELECT * FROM `%s` WHERE `email` = '%s'", $this->file_ref  [ 97 ], $username ) , 
				true, 
				$results );
				if ( count ( $results ) == 0 )  $temp .= $this->language [ 'LOGIN_ERROR_SHORT' ];
		}
		$row = current ( $results );
		$this->recid  = $row [ 'user_id' ] ?? 0;
		$hulp1Arr = array ( 'password' => password_hash ( $_POST [ 'password' ], PASSWORD_DEFAULT ) ) ; 
		$results = array ( );
		$nameFromEmail = "|". str_replace (".", "|", $display_name); 
		$database->execute_query ( 
			sprintf ( "SELECT * FROM `%s` WHERE `email` = '%s'", $this->file_ref  [ 99 ], $username ), 
			true, 
			$results );
		if ( count ( $results ) == 0 ) {
			// insert new adres recod.
			$hulp2Arr = array( );
			$hulp2Arr ['adresid'] = $row[ 'user_id' ];
			$hulp2Arr ['email'] = $username;
			$hulp2Arr ['type'] = 2;
			$hulp2Arr ['name'] = $nameFromEmail;
			$hulp2Arr ['ref'] = $this->setting [ 'owner' ];
			$database->build_and_execute(
				"insert",
				$this->file_ref  [ 99 ],
				$hulp2Arr);
			$results = array();
			$database->execute_query ( 
				sprintf ( "SELECT * FROM `%s` WHERE `email` = '%s'", $this->file_ref  [ 99 ], $username ),
				true, 
				$results );
		}
		// adres record exists
		$row = current ( $results ) ;
		$hulp2Arr = array( );
		$hulp2Arr [ 'adresid' ] = $this->recid;
		$hulp2Arr [ 'zoek' ] =  sprintf ( '|%s|%s|%s%s|', $row['name'], $row['email'], $this->setting [ 'owner' ], $row['id'] ); // default zoek
		$hulp2Arr [ 'ref' ] = sprintf ( '%s%s', $this->setting [ 'owner' ], $row['id'] );
		$database->build_and_execute(
			"update",
			$this->file_ref  [ 99 ],
			$hulp2Arr,
			"`id` = '" . $row['id'] . "'");
		$job = array ( );
		$job [ ] = sprintf ( "UPDATE `%s` SET %s WHERE `user_id` = '%s'", 
			$this->file_ref  [ 97 ],  
			$this->gsm_accessSql ( $hulp1Arr , 2 ), 
			$this->recid ); 
		// save the string for the moment the pincode is entered
		if ( count ($job) > 0 ) $this->gsm_pinCode ( array ( 0=>$username, 1=>$display_name ), 1 , $job);
		return $temp;
	}
	
	public function gsm_registerEmailChange ( 
		// ============================
		$mailist,   // mail as is
		$mailsoll 	// mail as requested
		// ============================
		) {  
		global $database;
		$this->version [ __FUNCTION__ ] = "20230808";  
		$LocalHulp = $this->gsm_sanitizeStringS ( $mailsoll, "s{EMAIL}" );
/* debug * / Gsm_debug ( array (	'van' => $mailist, 'naar' => $mailsoll, 'this' => $this ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$temp = '';
		$job = array ( );
		$result = array ( );	
		// existing adres record
		$database->execute_query ( 
			sprintf ( "SELECT * FROM `%s` WHERE `email` = '%s'", $this->file_ref  [ 99 ], $mailist ) , 
			true, 
			$result );
		if ( count ( $result ) == 0 ) return $temp;
		$row = current ( $result );
		if ( $row [ 'adresid' ] < 1 ) return $temp;
		$job [ ] = sprintf ( "UPDATE `%s` SET `email` = '%s'  WHERE `id` = '%s'", $this->file_ref  [ 99 ], $LocalHulp, $row [ 'id'] );
		// pre existing
		$result = array ( );	
		$database->execute_query ( 
			sprintf ( "SELECT * FROM `%s` WHERE `email` = '%s'", $this->file_ref  [ 99 ], $LocalHulp ) , 
			true, 
			$result );
		if ( count ( $result ) > 0 ) {
			$this->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . __FUNCTION__ .
					$this->language [ 'TXT_LOGIN_ERROR' ] . NL;
			$job = array ( );
			return $temp;
		}
		$result = array ( );	
		// existing user record
		$database->execute_query ( 
			sprintf ( "SELECT * FROM `%s` WHERE `user_id` = '%s'", $this->file_ref  [ 97 ], $row [ 'adresid'] ) , 
			true, 
			$result );
		if ( count ( $result ) == 0 ) return $temp;
		$row = current ( $result );
		$hulp1Arr = array( );
		$hulp1Arr [ 'email' ] = $LocalHulp;
		if ( $row [ 'username' ] == $mailist ) $hulp1Arr [ 'username' ] = $LocalHulp;
		$job [ ] = sprintf ( "UPDATE `%s` SET %s WHERE `user_id` = '%s'", 
			$this->file_ref  [ 97 ],  
			$this->gsm_accessSql ( $hulp1Arr , 2 ), 
			$row['user_id'] ); 
		if ( count ( $job ) > 0 ) $this->gsm_pinCode ( array ( 0=>$mailsoll, 1=>$row [ 'display_name' ] ), 1 , $job);
		$this->page_content [ 'MODE' ] = 9;
		return $temp;
	}
	
	public function gsm_scanCastor ( 
		// ============================
		$prefix, 
		$project 
		// ============================
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "202300811";
		$temp = '';
		/* check  precense of castor file */
		$results = array ( );
		$database->execute_query ( sprintf ("SHOW TABLE STATUS LIKE '%s'", LOAD_DBBASE . '_castor' ) , 
			true, 
			$results );
		if ( count ( $results ) == 0 ) return $temp; // not existing exit
		
		$query = "SELECT `id`, `name`, `area`, `type`, `ref`, `location`, `filetype`, `keywords`, `date`, `hash` 	FROM `" . LOAD_DBBASE . '_castor' . "`";
		$query .= " WHERE `type` = '".$prefix."' AND `ref` = '".$project."'";
		$query .= " ORDER BY `date`";
		$results = array ( ); // the output buffer
		$database->execute_query ( $query, 
			true, 
			$results );
		if ( count ( $results ) > 0 ) { 
			$nodisplay = true; $height = " "; $prev = ''; $clear ="."; $full = false; if ( $this->setting [ 'jpgwidth' ] < 401 ) $full = true;
			$temp .= ( isset ( $this->language [ 'layout' ] [ 'castor0' ] ) ) ? $this->language [ 'layout' ] [ 'castor0' ] : '<h1>Achtergrond Informatie</h1>';
			foreach ( $results as $row ) { 
				if ( $row [ 'filetype' ] =="pdf" ) { 
					if ( $nodisplay || $prev == "jpg" ) $temp .= NL.NL;
					$temp .= '<p><a class="pdf" href="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .'" target="_123">' . $row [ 'date' ] . " " . $row [ 'keywords' ] .'</a></p>';
					$nodisplay=false; $prev = "pdf"; // skip no data message
				 } elseif ( $row [ 'filetype' ] =="zip" ) { 
					if ( $nodisplay || $prev == "jpg" ) $temp .= NL.NL;
					$temp .= '<p><a class="zip" href="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .'" target="_123">' . $row [ 'date' ] . " " . $row [ 'keywords' ] .'</a></p>';
					$nodisplay=false; $prev = "zip"; // skip no data message
				 } elseif ( $row [ 'filetype' ] =="mp4" ) { 
					if ( $nodisplay || $prev == "jpg" ) $temp .= NL.NL;
					$temp .= '<p><a class="mp4" href="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .'" target="_123">' . $row [ 'date' ] . " " . $row [ 'keywords' ] .'</a></p>';
					$nodisplay=false; $prev = "mp4"; // skip no data message
				 } elseif ( $row [ 'filetype' ] =="html" ) { 
					if ( $nodisplay || $prev == "jpg" ) $temp .= NL.NL; 
					ob_start ( );
					$value= LEPTON_PATH . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ];
					include $value;
					$temp .= ob_get_clean ( );
					$temp .= NL.NL;
					$nodisplay=false; $prev = "html"; // skip no data message
				 } elseif ( $row [ 'filetype' ] == "jpg" ) { 
					if ( $nodisplay || $prev == "jpg" ) $temp .= NL.NL; 
					if ( $full ) { // opt to display full image
						$temp .= '<a href="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .'" target="_123"><img src="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .' " width="'.$this->setting [ 'jpgwidth' ].'" height="'.$height.'" /></a>';
					 } else { // only in displayed size
						$temp .= '<p><img src="'.LEPTON_URL . $row [ 'area' ] . $row [ 'location' ] . $row [ 'name' ] .' " width="'.$this->setting [ 'jpgwidth' ].'" height="'.$height.'" vspace="5"/></p>';
					 } 
					$nodisplay=false; $prev = "jpg"; // skip no data message
				 } 
			 } 
			$temp .= ( isset ( $this->language  [ 'layout' ] [ 'castor9' ] ) ) ? $this->language  [ 'layout' ] [ 'castor9' ] : ' ';
		 } 
		 /* debug * / Gsm_debug ( array ( $temp  ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		return $temp; 
	 } 	 
	
	public function gsm_standenStatus ( 
		// ============================
		$id, 
		$input = "start" 
		// ============================		
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20240208";  
/* debug * / Gsm_debug ( array ( 'id' => $id, 'input' => $input, $this->field_content ?? "", $this->usert ?? "" ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$returnvalue = '';
		if (  isset ( $this->field_content ) && count ( $this->field_content ) < 2 ) {
			$this->page_content [ 'MODE' ] = 0;
			return $returnvalue;
		}
		$temp = is_array ( $input) ? $input : array ( $input );
		
		$query  = "SELECT * FROM `" . $this->file_ref [ 98 ] . "` WHERE `adresid`= '" . $id . "' ORDER BY `type`, `ref`, `datumsoll`";
		$results = array();
		$database->execute_query( 
			$query, 
			true, 
			$results);
		$TEMPbegin = '';
		$TEMPend = '';	
		if ( count ( $results ) > 0 ) {
			$TEMPbegin = '<table><tr><th>&nbsp;Reference&nbsp;</th><th>&nbsp;Details&nbsp;</th><th>&nbsp;Status&nbsp;</th><th>&nbsp;Opmerking</th></tr>';
			$TEMPend = '</table>';
			foreach ($results as $srow) {
				if ($srow['active']==1) {
					switch ( $srow['type'] ) { 
						case 1:
							if ($srow['amtsoll'] > $srow['amtist']) {	
								$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s</td></tr>", $srow['ref'], $this->gsm_sanitizeStrings (($srow['amtsoll']- $srow['amtist']), 's{EURO|KOMMA}'), "open" , $srow['content_long'] );
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", $srow['ref'], $this->gsm_sanitizeStrings($srow['amtsoll'], 's{EURT|KOMMA}'), "open" , $srow['content_long'], '', '' ) ) );
							} else {
								$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s</td></tr>", $srow['ref'], $this->gsm_sanitizeStrings($srow['amtist'], 's{EURO|KOMMA}'), "voldaan", $srow['content_long']);
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", $srow['ref'], $this->gsm_sanitizeStrings ($srow['amtist'], 's{EURT|KOMMA}'), "voldaan", $srow['content_long'], '', '' ) ) );
							}
							if (strlen ($srow['content_short'])>4) { 
								$returnvalue .= sprintf ('<tr><td colspan="3">%s&nbsp;</td><td>&nbsp;%s</td></tr>'," ", $srow['content_short'] );
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', '', '', $srow['content_short'],'', '' ) ) );
							}
							if (strlen ($srow['comment'])>4){
								$returnvalue .= sprintf ('<tr><td colspan="3">%s&nbsp;</td><td>&nbsp;%s</td></tr>'," ", $srow['comment']);
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', '', '', $srow['comment'],'', '' ) ) );
							}
							break;
						case 2:
							if ($srow['datumist'] == "0000-00-00") {
								$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s</td></tr>",  $srow['ref'], "", "", $srow['content_long']);
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", $srow['ref'], '', "", $srow['content_long'],'', '' ) ) );	
								if ($srow['datumsoll'] != "0000-00-00" ) {
									$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>%s</td><td>&nbsp;%s</td></tr>", "",  $srow['datumsoll'], "uitgenodigd" , "");
									$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', $srow['datumsoll'], "uitgenodigd", '', '', '' ) ) );	
								}
							} else {
								if ($srow['datumsoll'] != "0000-00-00") {
									if ($srow['datumsoll'] > $this->page_content [ 'DATE' ]) {  
										$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td></tr>", $srow['ref'], $srow['datumist'], "ok" , $srow['content_long']);
										$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s</td></tr>", "",  $srow['datumsoll'], "eind geldigheid" , "");
										$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", $srow['ref'], $srow['datumist'], "ok", $srow['content_long'],'', '' ) ) );	
										$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', $srow['datumsoll'], "eind geldigheid", '', '', '' ) ) );	
									} else {
										$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td></tr>", $srow['ref'], '', "&nbsp;" , $srow['content_long']);
										$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s</td></tr>", "",  $srow['datumsoll'], "NOK" , "");	 
										$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", $srow['ref'], '', '', $srow['content_long'],'', '' ) ) );	
										$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', $srow['datumsoll'], "NOK", '', '', '' ) ) );	
									}
								} else {
									$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td></tr>", $srow['ref'], $srow['datumist'], "actie ok" , $srow['content_long']);
									$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", $srow['ref'], $srow['datumist'], "actie ok",  $srow['content_long'],'', '' ) ) );
								}				  
							}  
							if ( $srow['amtsoll'] >0.1 && $srow['amtsoll']>$srow['amtist'] && $srow['datumist'] == "0000-00-00" ) {
								$returnvalue .= sprintf ('<tr><td colspan="3">%s&nbsp;</td><td>&nbsp;%s / %s&nbsp;</td></tr>'," ", 
								($srow['amtist']>0.01) ? $this->gsm_sanitizeStrings($srow['amtist'], 's{KOMMA}') : "-" ,$this->gsm_sanitizeStrings($srow['amtsoll'], 's{KOMMA}'));
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s / %s;%s;%s", '', '', '', 
								($srow['amtist']>0.01) ? $this->gsm_sanitizeStrings($srow['amtist'], 's{KOMMA}') : "-",
								$this->gsm_sanitizeStrings($srow['amtsoll'], 's{KOMMA}'), '', '' ) ) );
							}
							if (strlen ($srow['content_short'] )>4  && $srow['datumist'] == "0000-00-00" ) {
								$returnvalue .= sprintf ('<tr><td colspan="3">%s&nbsp;</td><td>&nbsp;%s</td></tr>'," ", $srow['content_short']);
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', '', '', $srow['content_short'],'', '' ) ) );
							}
							if (strlen ($srow['comment'] ) >4 && $srow['datumist'] == "0000-00-00" ) {
								$returnvalue .= sprintf ('<tr><td colspan="3">%s&nbsp;</td><td>&nbsp;%s</td></tr>'," ", $srow['comment']);
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', '', '', $srow['comment'],'', '' ) ) );	
							}
							break;
						case 3:
							if ($srow['datumist'] == "0000-00-00") {
								$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td></tr>",  $srow['ref'], "", "&nbsp;melding&nbsp;", $srow['content_short']);
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", $srow['ref'], '', 'melding', $srow['content_short'],'', '' ) ) );
								if ($srow['datumsoll'] != "0000-00-00" && ($srow['datumsoll'] != "1970-01-01" || $srow['datumsoll'] != "0000-00-00")) {
									$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s</td></tr>", "",  $srow['datumsoll'], "&nbsp;afmelding&nbsp;" , "");
									$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', $srow['datumsoll'], 'afmelding', '' ,'', '' ) ) );
								}
							} else {
								$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s</td></tr>", $srow['ref'], $srow['datumist'], "&nbsp;melding&nbsp;" , $srow['content_short']);
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", $srow['ref'], $srow['datumist'], "melding", $srow['content_long'],'', '' ) ) );
								if ($srow['datumsoll'] != "0000-00-00" && $srow['datumsoll'] != "1970-01-01") {
									$returnvalue .= sprintf ("<tr><td>%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td><td>&nbsp;%s&nbsp;</td></tr>", "", $srow['datumsoll'], "&nbsp;afmelding&nbsp;" , "");
									$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', $srow['datumsoll'], "afmelding", '','', '' ) ) );
								}
							}  
							if ($srow['amtsoll']>0.1 & $srow['datumsoll'] != "0000-00-00" && $srow['datumsoll'] != "1970-01-01") {
								$returnvalue .= sprintf ('<tr><td colspan="3">%s&nbsp;</td><td>&nbsp;%s&nbsp;stand :&nbsp;%s&nbsp;</td></tr>'," ", $srow['datumsoll'] ,$this->gsm_sanitizeStrings($srow['amtsoll'], 's{KOMMA}'));
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', '', '', $srow['datumsoll'] . " stand : " . $this->gsm_sanitizeStrings($srow['amtsoll'], 's{KOMMA}'),'', '' ) ) );
							}
							if ($srow['amtist'] > 0.1 & $srow['datumist'] != "0000-00-00" && $srow['datumist'] != "1970-01-01"){
								$returnvalue .= sprintf ('<tr><td colspan="3">%s&nbsp;</td><td>&nbsp;%s&nbsp;stand :&nbsp;%s&nbsp;</td></tr>'," ", $srow['datumist'] ,$this->gsm_sanitizeStrings($srow['amtist'], 's{KOMMA}'));
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', '', '', $srow['datumist'] . " stand : " . $this->gsm_sanitizeStrings($srow['amtist'], 's{KOMMA}'),'', '' ) ) );
							}
							if (strlen ($srow['content_short']) > 4) {
								$returnvalue .= sprintf ('<tr><td colspan="3">%s&nbsp;</td><td>&nbsp;%s</td></tr>'," ", $srow['content_short']);
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', '', '', $srow['content_short'],'', '' ) ) );
							}
							if (strlen ($srow['comment']) > 4) {
								$returnvalue .= sprintf ('<tr><td colspan="3">%s&nbsp;</td><td>&nbsp;%s</td></tr>'," ", $srow['comment']);
								$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', '', '', $srow['comment'],'', '' ) ) );
							}
							break;
						default:
							break;
		}	}	}	}
		$temp[ ] = explode( ';', trim( sprintf( "%s;%s;%s;%s;%s;%s", '', '---000---', '', '' , '', '', '' ) ) );
		$returnvalue = is_array ( $input ) ? $temp : $TEMPbegin . $returnvalue . $TEMPend;
/* debug * / Gsm_debug ( array ( $returnvalue  ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		return $returnvalue;
	}
	
	public function gsm_standenSelect ( 
		// ============================
		$filter, 
		$input, 
		$page_content 
		// ============================		
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20250108";  
/* debug * / Gsm_debug ( array ( $filter, $input, $page_content  ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$returnvalue = array();
//		$description = '';
		// check if a valid filter was supplied
		if (!preg_match('#(s)#i', $filter, $match)) {
			die ( sprintf ( 'Oeps unexpected case. Filter: <b>%s</b> is invalid ( %s-%s )</br>', 
				htmlentities($filter), 
				__LINE__, 
				$this->version ) );
		}
		if ($input == ''){ $temp=array ( ); } else { $temp = is_array ( $input ) ? $input : array ( $input );}
		$filter_type = strtolower ( $match [ 1 ] );
		switch ( $filter_type) {
			case 's': 
				// check for correct string filter: s{key}
				if ( !preg_match ( '#(s)\{(.+)\}#i', $filter, $match ) ) {
					die ( sprintf ( 'Oeps unexpected case. String filter: <b>%s</b> is invalid ( %s-%s )</br>', 
						htmlentities ( $filter ), 
						__LINE__, 
						$this->version ) );
				}
				$filter_options = strtoupper ( $match [ 2 ] );
				if ( strpos ( $filter_options, 'TO' ) !== false && $page_content[ 'TO' ] == 1 ) {
					$returnvalue [ 'text' ] = ( $temp ['type'] > 1 ) 
						? "mail/"
						: "post/";
					$returnvalue [ 'keuze' ] = "sel";
					if ( !isset ( $returnvalue [ 'extra' ] ) ) $returnvalue [ 'extra' ] = "----";
				}
				if ( strpos ( $filter_options, 'TO' ) !== false && $page_content [ 'TO' ] == 2 ) {
					$returnvalue [ 'keuze' ] = ( $temp [ 'type' ] > 1 ) 
						? "sel"
						: "";
					$returnvalue [ 'text' ] = ( $temp [ 'type' ] > 1 ) 
						? "mail/"
						: "";
					$returnvalue [ 'extra' ] = ( $temp ['type'] > 1 ) 
						? "----"
						: "";
				}
				if ( strpos ( $filter_options, 'TO' ) !== false && $page_content [ 'TO' ] == 3 ) {
					$returnvalue [ 'text' ] = "";
					$returnvalue [ 'keuze' ] = "";
					$returnvalue [ 'extra' ] = "";
				if ( $temp [ 'dat1' ] > "1970-01-01" && $temp [ 'dat1' ] < $this->page_content [ 'DATE' ] ) {
						if ( $temp [ 'dat1' ] > $temp [ 'dat2' ] ) { 
							$returnvalue [ 'keuze' ] = "sel"; 
							$returnvalue [ 'text' ] = "lid /"; 
							$returnvalue [ 'extra' ] = sprintf ( "Lid vanaf: %s" ,$temp ['dat1']); 
						} elseif ( $temp ['dat2'] < $this->page_content [ 'DATE' ] ) {
							$returnvalue [ 'keuze' ] = "not"; 
							$returnvalue [ 'text' ] = "ex-lid /"; 
							$returnvalue [ 'extra' ] = sprintf ( "Lid vanaf: %s  tot " ,$temp ['dat1'] ,$temp ['dat2']);
						} else {
							$returnvalue [ 'keuze' ] = "sel"; 
							$returnvalue [ 'text' ] = "lid /"; 
							$returnvalue [ 'extra' ] = sprintf ( "Lid vanaf: %s  tot " ,$temp ['dat1'] ,$temp ['dat2']);
						}						
					}	
				}
				if ( strpos ( $filter_options, 'TO' ) !== false && $page_content [ 'TO' ] == 4 ) {
					// lees de standen records erbij
					$sql_stand = "SELECT * FROM `" . $this->file_ref  [ 98 ] . "` ";
					$sql_stand .= " WHERE `adresid`= '" . $input [ 'id' ] . "' AND `active` > 0 ";
					// voor open betaling	
					$sql_stand .= " AND `type` = 1 ORDER BY `ref`";
					$standen = array ( );
					if ( $database->execute_query ( $sql_stand, true, $standen ) ) {
						$rapporteren = false;
						$cum = 0; 
						$i=0;
						$returnvalue [ 'keuze' ] = '';
						$returnvalue [ 'extra' ] = '';
						foreach ( $standen as $stand) {
							$cum = ( $stand['amtsoll'] > $stand['amtist'] ) ? $cum + $stand [ 'amtsoll' ]- $stand [ 'amtist' ] : $cum;
							$returnvalue [ 'extra' ] .= sprintf( "%s: %s  %s  %s  %s </br>",
								$stand ['content_long'],	
								( $stand [ 'amtsoll' ] > $stand ['amtist'] ) ? $this->gsm_sanitizeStrings ( $stand ['amtsoll'] - $stand ['amtist'] , 's{KOMMA|EURT}') : " " ,
								( $stand [ 'amtsoll' ] > $stand ['amtist'] ) ? " open " : " betaald op ",
								( $stand [ 'amtsoll' ] > $stand ['amtist'] ) ? " " : $stand ['datumist'] ,
								""); 
							$rapporteren = ( $cum > 0.10 ) ? true : false ;
							$returnvalue [ 'keuze' ] = 'not';
						}
						if ( $rapporteren ) {
							$returnvalue [ 'keuze' ] = 'sel';
							$returnvalue [ 'extra' ] .= sprintf( "<br /> %s %s </br>Graag betalen onder vermelding van %s / %s", 
								" Totaal open : ", 
								$this->gsm_sanitizeStrings ( $cum, 's{KOMMA|EURT}'),
								$stand ['ref'],
								$stand ['adresid'] ); 
						} else {
							$returnvalue [ 'keuze' ] = '';
						}
					}
				}
				if (strpos($filter_options, 'TO') !== false && $page_content[ 'TO'] == 5) {
					if ( $this->memory [ 3] == substr ( $page_content[ 'REF'], 0, 2 ) ) { 
						$returnvalue [ 'keuze' ] = 'sel'; 
						$returnvalue [ 'text' ] = $this->memory [ 3] . " /";
						$returnvalue [ 'extra' ] = $page_content[ 'REF'] ;
					} else {  
						$returnvalue [ 'keuze' ] = 'not'; 
						$returnvalue [ 'text' ] = $page_content[ 'REF'] ;
						$returnvalue [ 'extra' ] = $page_content[ 'REF'] ;
					}
				}
				if (strpos($filter_options, 'TO') !== false && $page_content[ 'TO'] == 6) {
					$numtest_qtystr = $this->gsm_sanitizeStringv ($temp[ 'ref0' ], 'v{0;-100000;100000}');
					if  ( is_numeric ( $numtest_qtystr ) && $numtest_qtystr > 0 ) { 
						$returnvalue [ 'keuze' ] = 'sel'; 
						$returnvalue [ 'text' ] = 'partner/'; 
						$returnvalue [ 'extra' ] = sprintf( "%s %s /", " share ", $numtest_qtystr ); 
					} else {  
						$returnvalue [ 'text' ] = " - /";
						$returnvalue [ 'keuze' ] = ( strlen ( $temp[ 'ref0' ] ) > 1 ) 
							? "not" 
							: "";
						$returnvalue [ 'extra' ] = ( strlen ( $temp[ 'ref0' ] ) > 1 ) 
							? $temp[ 'ref0' ]
							: '';
					}
				}
				if (strpos($filter_options, 'TO') !== false && $page_content[ 'TO'] == 7) {
					// lees de standen records erbij
					$sql_stand = "SELECT * FROM `" . $this->file_ref  [ 98 ] . "` ";
					$sql_stand .= " WHERE `adresid`= '" . $input ['id'] . "' AND `active` > 0 ";
					$sql_stand .= " AND `ref` LIKE '%" . $page_content[ 'STAND'] . "%'";
					$sql_stand .= " ORDER BY `type`";
					$standen =array();
					$database->execute_query ( $sql_stand, true, $standen);
					if ( count ( $standen ) > 0) {
						$returnvalue [ 'text' ] = "";
						$returnvalue [ 'extra' ] = "";
						$returnvalue [ 'keuze' ] = "";
						$rapporteren = false;
						$notselected = false;
						foreach ( $standen as $stand) {
							if ( $stand [ 'type'] == 1 ) {
								if ( $stand['amtsoll'] > $stand['amtist'] ) {
									if ( !$rapporteren ) $rapporteren = true;
									$returnvalue [ 'text' ] .= sprintf( "%s %s /", " Open ", $this->gsm_sanitizeStrings ( $stand [ 'amtsoll' ], 's{KOMMA|EURT}')); 
									$returnvalue [ 'extra' ] .= sprintf( "%s ( %s ) %s: %s  %s  %s  %s </br>",
										$stand [ 'datumsoll' ],
										$stand [ 'id' ],
										$stand ['content_short'],	
										( $stand [ 'amtsoll' ] > $stand ['amtist'] ) 
											? $this->gsm_sanitizeStrings ( $stand ['amtsoll'] - $stand ['amtist'] , 's{KOMMA|EURT}') 
											: " " ,
										( $stand [ 'amtsoll' ] > $stand ['amtist'] ) 
											? " open " 
											: " betaald op ",
										( $stand [ 'amtsoll' ] > $stand ['amtist'] ) 
											? " " 
											: $stand ['datumist'] ,
										""); 
								} else  {
									$returnvalue [ 'extra' ] .= sprintf( "%s ( %s ) %s: %s  %s  %s  %s </br>",
										$stand [ 'datumsoll' ],
										$stand [ 'id' ],
										$stand ['content_short'],	
										$this->gsm_sanitizeStrings (  $stand ['amtist'] , 's{KOMMA|EURT}') ,
										" betaald op ",
										$stand ['datumist'],
										""); 
								}	
								$returnvalue [ 'keuze' ] =  ( $notselected ) ? 'not' : "";
								$returnvalue [ 'keuze' ] =  ( $rapporteren ) 
									? 'sel' 
									: $returnvalue [ 'keuze' ];
							} 
							if ( $stand [ 'type'] == 2 ) {
								// case1: geslaagd definitief
								if ( $stand [ 'datumsoll' ] == "0000-00-00" && $stand [ 'datumist' ] != "0000-00-00") { 
									// case1: geslaagd definitief 
									if ( !$notselected ) $notselected = true;
									$returnvalue [ 'text' ] .= sprintf( "%s %s /", $page_content[ 'STAND'], " Ok "); 	
									$returnvalue [ 'extra' ] .= sprintf( "%s ( %s ) %s: %s </br>",
										$stand [ 'datumist' ],
										$stand [ 'id' ],
										$stand ['content_short'],
										"ok" );	
								} elseif ( $stand [ 'datumsoll' ] != "0000-00-00" && $stand [ 'datumist' ] != "0000-00-00" && $stand [ 'datumsoll' ] > $stand [ 'datumist' ] ) { 
									// geslaagd met houdbaarheidsdatum 
									if ( $stand [ 'datumsoll' ] > $this->page_content [ 'DATE' ]) {
										// nog geldig
										if ( !$notselected ) $notselected = true;
										$returnvalue [ 'text' ] .= sprintf( "%s %s /", $page_content[ 'STAND'], " Ok "); 
										$returnvalue [ 'extra' ] .= sprintf( "%s ( %s ) %s: %s </br>",
											$stand [ 'datumist' ],
											$stand [ 'id' ],
											$stand ['content_short'],
											"ok" );				
									} else {
											// verlopen 
											if ( !$rapporteren ) $rapporteren = true;
											$returnvalue [ 'text' ] .= sprintf( "%s %s /", $page_content[ 'STAND'], " Expired "); 
											$returnvalue [ 'extra' ] .= sprintf( "%s ( %s ) %s: %s %s</br>",
												$stand [ 'datumist' ],
												$stand [ 'id' ],
												$stand ['content_short'],
												"expired",
												$stand [ 'datumsoll' ]);				
									} 
								} else {
									if ( !$rapporteren ) $rapporteren = true;
									$returnvalue [ 'text' ] .= sprintf( "%s %s /", $page_content[ 'STAND'], " uitgenodigd "); 
									$returnvalue [ 'extra' ] .= sprintf( "%s: %s %s</br>",
										$stand [ 'datumsoll' ],
										"uitgenodigd ",
										$stand ['content_long']);
									$returnvalue [ 'extra' ] .= sprintf( " %s</br> %s</br>",
									$stand ['content_short'],
									$stand ['comment']);
								}
								$returnvalue [ 'keuze' ] =  ( $notselected ) ? 'not' : "";
								$returnvalue [ 'keuze' ] =  ( $rapporteren ) 
									? 'sel' 
									: $returnvalue [ 'keuze' ];
							} 
							if ( $stand [ 'type'] == 3 ) {
								$returnvalue [ 'keuze' ] = '';
							}
						}
					} else {
						$returnvalue [ 'text' ] = "";
						$returnvalue [ 'extra' ] = "";
						$returnvalue [ 'keuze' ] = "";
						$rapporteren = false;
						$notselected = false;
					}
				}						
				break;
		}
		if ( isset ( $returnvalue [ 'keuze'] ) && strlen ( $returnvalue [ 'keuze'] ) > 1 ) {
			if ( $returnvalue ['keuze'] == "sel" ) {
				if ( strlen ( $temp [ 'email' ] ) < 10 ) { $returnvalue['keuze'] = "pst";
					} elseif ( $temp [ 'type' ] == 1 ) { $returnvalue ['keuze'] = "pst"; }
			}
		}

		if ( isset ($temp [ 'keuze' ] ) && $temp [ 'keuze' ] == "des" ) $returnvalue [ 'keuze' ] = "des";
/* debug * / Gsm_debug ( array ( $returnvalue ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */

		return $returnvalue;
	}
	
	public function gsm_standenSelectie ( 
		// ============================
		$func, 	// func = 
		$ref1, 	// standen ref
		$ref2 = "",  // groepen 
		$vink = "",
		$type = ""
		// ============================		
		) { 
		global $database;
		$this->version [ __FUNCTION__ ] = "20250210";  
/* debug * / Gsm_debug ( array ( $func, $ref1, $ref2, "vink"=>$vink, $type, $this ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$temp = array ();
/*  */	$ref3 = array ();
		/* case  standenref  */
		$refArr = array ();
		$standArr = array ();
		if ( strlen ($ref1 ) >1) {
			$query = "SELECT `" . $this->file_ref [ 99 ] . "`.*,
					`" . $this->file_ref [ 98 ] . "`.`ref` as `user_referlist`
				FROM `" . $this->file_ref [ 99 ] . "` LEFT JOIN `" . $this->file_ref [ 98 ] . "`
				ON `" . $this->file_ref [ 98 ] . "`.`id` = `" . $this->file_ref [ 99 ] . "`.`adresid`";
			$query .= " WHERE `" . $this->file_ref [ 99 ] . "`.`ref` = '" . $ref1 . "'";
			$query .= " ORDER BY `" . $this->file_ref[ 98 ] . "`.`ref`";
			$results = array ();
			$database->execute_query (
				$query,
				true, 
				$results); 
			foreach ($results as $row ) {
				if ( $type == "" ||  ( $type != "" && $row [ 'type' ] == $type ) ) { 
					$standArr [ ] = $row [ 'adresid' ];	
					$hulp = substr ( $row [ 'user_referlist' ], 0, 2 );
					$refArr [ $hulp ] = $hulp;
/*  */				$ref3 [ $row [ 'id' ] ] = $row [ 'adresid' ];
		}	}	}
		/* case  standenref  */
		if ( !isset ( $refArr ) ) $refArr = array ();
		if ( is_array ( $ref2 ) ) $refArr = $ref2;
		
		/* vorige cyclus */
		$vinkArr = array ();
		if ( is_array ( $vink ) ) $vinkArr = $vink;
		
		$results = array ();
		$database->execute_query (
			sprintf ("SELECT * FROM `%s` ORDER BY `%s`.`ref`", $this->file_ref [ 98 ] , $this->file_ref [ 98 ] ),
			true, 
			$results);  
		$index_rec=0;
		foreach ( $results as $row) { 
			$index_rec++;
			if ( $row [ 'dat1' ] > "1970-01-01" && $row [ 'dat1' ] < $this->page_content  [ 'DATE' ] ) {
				if ( $row [ 'dat1' ] > $row [ 'dat2' ] ) { 
					$temp [ $index_rec ] [ 'keuze' ] = "sel"; 
					$temp [ $index_rec ] [ 'text' ] = "lid /"; 
					$temp [ $index_rec ] [ 'extra' ] = sprintf ( "Lid vanaf: %s", $row ['dat1']); 
				} elseif ( $row [ 'dat2' ] < $this->page_content  [ 'DATE' ]) {
					$temp [ $index_rec ] [ 'keuze' ] = "not"; 
					$temp [ $index_rec ] [ 'text' ] = "ex-lid /"; 
					$temp [ $index_rec ] [ 'extra' ] = sprintf ( "Lid vanaf: %s  tot ", $row ['dat1'], $row ['dat2']);
				} else {
					$temp [ $index_rec ] [ 'keuze' ] = "sel"; 
					$temp [ $index_rec ] [ 'text' ] = "lid /"; 
					$temp [ $index_rec ] [ 'extra' ] = sprintf ( "Lid vanaf: %s  tot ", $row ['dat1'], $row ['dat2']);
			}	}
			if ( isset ( $temp [ $index_rec ] [ 'keuze' ] )) {
				if ( in_array ( $row [ 'id' ] , $standArr ) ) $temp [ $index_rec ] [ 'keuze' ] = 'oke';
				if ( count ( $refArr ) > 0 && !in_array ( substr ($row [ 'ref' ], 0, 2 ), $refArr ) ) $temp [ $index_rec ] [ 'keuze' ] = 'not';
				$temp [  $index_rec ] [ 'id' ] =  $row [ 'id' ];
				$temp [  $index_rec ] [ 'ref' ] =  $row [ 'ref' ];
				$temp [  $index_rec ] [ 'email' ] =  $row [ 'email' ];				
				$temp [  $index_rec ] [ 'name' ] = $this->gsm_sanitizeStringS ( $row [ 'name' ], "s{CLEAN}" );
				$temp [  $index_rec ] [ 'adres' ] = $this->gsm_sanitizeStringS ( $row [ 'adres' ], "s{CLEAN}" );
				$temp [  $index_rec ] [ 'active' ] = $row [ 'active' ];						
			}
			if ( count ( $vinkArr ) > 0 
				&& !in_array ( $row [ 'id' ] , $vinkArr ) 
				&& isset ($temp [  $index_rec ] [ 'keuze' ] )
				&& $temp [  $index_rec ] [ 'keuze' ] == 'sel' ) 
					$temp [  $index_rec ] [ 'keuze' ] = 'des';
					
			foreach ( $ref3 as $pay => $load ) { 	
				if ( $row [ 'id' ] == $load ) {
					if ( isset ( $temp [  $index_rec ] [ 'standid' ] ) ) {
						$index_rec_old = $index_rec;
						$index_rec++;					
						$temp [ $index_rec ] [ 'keuze' ] = $temp [ $index_rec_old ] [ 'keuze' ];
						$temp [ $index_rec ] [ 'text' ] = $temp [ $index_rec_old ] [ 'text' ];
						$temp [ $index_rec ] [ 'extra' ] = $temp [ $index_rec_old ] [ 'extra' ];
						$temp [ $index_rec ] [ 'id' ] = $temp [ $index_rec_old ] [ 'id' ];
						$temp [ $index_rec ] [ 'ref' ] = $temp [ $index_rec_old ] [ 'ref' ];
						$temp [ $index_rec ] [ 'email' ] = $temp [ $index_rec_old ] [ 'email' ];
						$temp [ $index_rec ] [ 'name' ] = $temp [ $index_rec_old ] [ 'name' ];
						$temp [ $index_rec ] [ 'adres' ] = $temp [ $index_rec_old ] [ 'adres' ];
						$temp [ $index_rec ] [ 'active' ] = $temp [ $index_rec_old ] [ 'active' ];
						$temp [  $index_rec ] [ 'standid' ] = $pay;
					} else { 
						$temp [  $index_rec ] [ 'standid' ] = $pay;
					}

				}
			}			
		}
		/* debug * / Gsm_debug ( array ( $temp ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		return $temp;
	}
	
	public function gsm_standenText ( 
		// ============================	
		$text_id = 0, 
		$func = 0 
		// ============================
		) { 
		global $database;

		$this->version [ __FUNCTION__ ] = "20230816";  
		$returnvalue = false;
/* debug * / Gsm_debug ( array ( $text_id, $func ), __LINE__ .  __FUNCTION__ . $this->version [ __FUNCTION__ ] );  /* debug */
		$payload = $text_id;
		if ( strlen ( $payload ) >3  && !is_numeric ( $payload ) ) { 
			$result = array ( );
			$database->execute_query (
				"SELECT * FROM `".TABLE_PREFIX."sections` WHERE `name` = '".$payload."'",
				true,
				$result,
				false );
			if (empty ( $result ) ) die ( sprintf ( '%s ( %s - %s ) -%s- absent',
				$this->language ["TXT_ERROR_INIT"] ,
				__LINE__ ,
				$this->version [ __FUNCTION__ ],
				$payload ) );
			$payload = ( $func == 0 ) ? $result [ 'page_id'] : $result  [ 'section_id'];
		}
		if ( is_numeric ( $payload ) && $payload > 0 ) {
			if ($func == 0 ) { 
				// array output
				$query = "SELECT section_id, page_id, name FROM ".TABLE_PREFIX."sections WHERE page_id = '".$payload."'"; 
				$results = array ( );
				if ( $database->execute_query( $query, true, $results ) && count ( $results ) > 0 ){
					foreach ( $results as $row ) $arr_uit [ $row [ 'section_id' ] ] = $row [ 'name' ];	
					return $this->gsm_sanitizeStrings ( $arr_uit, "s{DECODE}");
				}	
			} else {
				// content output	
				$query = "SELECT page_id, section_id, content  FROM " . TABLE_PREFIX . "mod_wysiwyg WHERE section_id = '" . $payload . "'"; 
				$results = array ( );
				if ( $database->execute_query( $query, true, $results ) && count ( $results ) > 0 ){
					$row = current ( $results );
					return $this->gsm_sanitizeStrings ( $row ['content'], "s{DECODE}");
				}
			}
		} else { 
			die( __LINE__ . $this->language [ 'TXT_ERROR_PAGE' ]. $payload); 
		}
		return $returnvalue;
	}	
	
	 	public function gsm_truncate ( 
		// ============================	
		$text, 
		$length = 100, 
		$ending = '...', 
		$exact = true, 
		$considerHtml = false
		// ============================
		/**
		 * based on http://www.gsdesign.ro/blog/cut-html-string-without-breaking-the-tags/
		 *
		 * Code to truncate HTML a HTML string without breaking the tags.
		 *
		 * Cuts a string to the length of $length and replaces the last characters
		 * with the ending if the text is longer than length.
		 *
		 * @param string  $text String to truncate.
		 * @param integer $length Length of returned string, including ellipsis.
		 * @param string  $ending Ending to be appended to the trimmed string.
		 * @param boolean $exact If false, $text will not be cut mid-word
		 * @param boolean $considerHtml If true, HTML tags would be handled correctly
		 * @return string Trimmed string.
		 */
		) {  
		if ($considerHtml) {
			$this->version [ __FUNCTION__ ] = "20241203";  
			// if the plain text is shorter than the maximum length, return the whole text
			if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
				return $text;
			}
			// splits all html-tags to scanable lines
			preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
			$total_length = strlen($ending);
			$open_tags = array();
			$truncate = '';
			foreach ($lines as $line_matchings) {
				// if there is any html-tag in this line, handle it and add it (uncounted) to the output
				if (! empty($line_matchings[1])) {
					// if it's an "empty element" with or without xhtml-conform closing slash (f.e. <br/>)
					if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
						// do nothing
						// if tag is a closing tag (f.e. </b>)
					} else
						if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
							// delete tag from $open_tags list
							$pos = array_search($tag_matchings[1], $open_tags);
							if ($pos !== false) {
								unset($open_tags[$pos]);
							}
							// if tag is an opening tag (f.e. <b>)
						} else
							if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
								// add tag to the beginning of $open_tags list
								array_unshift($open_tags, strtolower($tag_matchings[1]));
							}
					// add html-tag to $truncate'd text
					$truncate .= $line_matchings[1];
				}
				// calculate the length of the plain text part of the line; handle entities as one character
				$content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
				if ($total_length + $content_length > $length) {
					// the number of characters which are left
					$left = $length - $total_length;
					$entities_length = 0;
					// search for html entities
					if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
						// calculate the real length of all entities in the legal range
						foreach ($entities[0] as $entity) {
							if ($entity[1] + 1 - $entities_length <= $left) {
								$left--;
								$entities_length += strlen($entity[0]);
							} else {
								// no more characters left
								break;
							}
						}
					}
					$truncate .= substr($line_matchings[2], 0, $left + $entities_length);
					// maximum lenght is reached, so get off the loop
					break;
				} else {
					$truncate .= $line_matchings[2];
					$total_length += $content_length;
				}
				// if the maximum length is reached, get off the loop
				if ($total_length >= $length) {
					break;
				}
			}
		} else {
			if (strlen($text) <= $length) {
				return $text;
			} else {
				$truncate = substr($text, 0, $length - strlen($ending));
			}
		}
		// if the words shouldn't be cut in the middle...
		if (! $exact) {
			// ...search the last occurance of a space...
			$spacepos = strrpos($truncate, ' ');
			if (isset($spacepos)) {
				// ...and cut the text in this position
				$truncate = substr($truncate, 0, $spacepos);
			}
		}
		// add the defined ending to the text
		$truncate .= $ending;
		if ($considerHtml) {
			// close all unclosed html-tags
			foreach ($open_tags as $tag) {
				$truncate .= '</' . $tag . '>';
			}
		}
		return $truncate;
	}	

} //end class