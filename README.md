# gsmoffl

Login extension with mailing and member administration

This document is relevant for `Gsmoffl` / `Membership`  version 7.1.2 and up.
Version 7.1.2 is suitable for LEPTON *** 7.2 *** 
Version 7.1.0 is suitable for LEPTON *** 7.0 or 7.1 *** 

## Download
The released stable `Gsmoffl` / `Membership` It is recommended to install/update to the latest available version listed. Older versions may contain bugs, lack newer functions or have security issues. 

## License
`Gsmoffl` / `Membership` is licensed under the [GNU General Public License (GPL) v3.0](http://www.gnu.org/licenses/gpl-3.0.html). The module is available "as is"

### You are free to:
Share — copy and redistribute the material in any medium or format
Adapt — remix, transform, and build upon the material
for any purpose, even commercially. 

### Under the following terms:
Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

### Notices:
You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

## Pre-conditions
The minimum requirements to get this application running on your LEPTON installation are as follows:

- LEPTON *** 7.2 *** of higher depending on the version
- the module uses Twig support
- the module uses Fomantic
- the taxonomy module installed.

The module is tested in combination with the Office Tegel template

## Installation

This description assumes you have a standard installation of Lepton 6.0. All tests are done exclusively with the TFA during installation selected off (not ticked) *)  

1. download the installation package
2. install the downloaded zip archive via the LEPTON module installer
3. create a page which uses the indicated module
4. start the backend functions setupl/instellingen and optionally activate one of the following functions where needed 

  * d REMOVE 	remove data 
  * d IMAGE 	image directory copied from frontend
  * d LOGGING 	empty the logging
  * d DETAIL 	detailed data
  * d INSTALL 	frontend files are created: customized values are overwritten bij default values !!
enter ? for other functions

The function can be started by selecting d_...._ where ... is a number of the function names can be concatenated.

The system will automatically install the file upon the first start of this module. 
 

### backend funtions SET_function
 - setupl 	also called instellingen tot setup and manitenace of the data
 - dummy	a dummy function
 - mail 	the mail function (this mail function is similar to the front end mail function)
 - stand	the standen function (this stand function is similar to the front end stand function)
 - leden	the member function (this leden function part is similar to the front end stand function)

 Depending on the contents of the SET file modules are made available it may be needed to modify it to get the required functionality

### frontend menu  SET_menu

- pass the login function	the default login function 

the other functions are to be used as administration functions
 
 - partner  the visitor maintenance function
 - dummy	a dummy function
 - mail 	the mail function
 - stand	the standen function 
 - leden	the member function 
 
 Depending on the contents of the SET file modules are made available it may be needed to modify it to get the required functionality
 
### For taxonomy (gsmoffl) use

the following is suggested

	 1. droplet	EN1 	(shared with a droplet)
	 2. droplet	EN4 	(shared with a droplet)
	 3. settin display 	(shared with a droplet)	 
	 4. setting	mail	mail		default tekst file for the mail
	 5. setting mail_max 200 		a limitation from the mail system 
	 
to run the mail functio there should be apage with the name mail 
	 
### related droplets

- Gsm_login 	Puts Login link with name if logged in 		
- Gsm_loginbox 	Displays Login / Logout box on the page 

#### droplet_Gsm_login

droplet which can be used instead for a simplified login function of the pass menu item
Usage [[Gsm_login]] 
 - mediadir=/archive 	to change the media subdirectory
 - project=XX 			to change the project prefix
 - login=no 			yes will allow only logged in persons to access the function
 
e.g [[Gsm_login]]

#### droplet_Gsm_loginbox

droplet to demonstrate the login status and an invitation to go to the login page
Usage [[Gsm_loginbox]] 

e.g. [[Gsm_loginbox]]