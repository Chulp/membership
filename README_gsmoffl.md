# gsmoffl
 Login extension with mailing and member administration

This document is relevant for `Gsmoffl` / `Leden admin`  version 7.0.0 and up.

## Download
The released stable `Gsmoffl` / `Leden admin` It is recommended to install/update to the latest available version listed. Older versions may contain bugs, lack newer functions or have security issues. 

## License
`Gsmoffa` / `castor` is licensed under the [GNU General Public License (GPL) v3.0](http://www.gnu.org/licenses/gpl-3.0.html). The module is available "as is"

## Installation

This description assumes you have a standard installation of Lepton 6.0. All tests are done exclusively with the TFA during installation selected off (not ticked) *)  

1. download the installation package
2. install the downloaded zip archive via the LEPTON module installer
3. create a page which uses the indicated module
4. start the backend functions setupa/instellingen and optionally activate one of the following functions where needed 

  * d REMOVE 	remove data (ref = weg or keyword = recycle).
  * d IMAGE 	image directory copied from frontend
  * d LOGGING 	empty the logging
  * d DETAIL 	detailed data
  * d INSTALL 	frontend files are created: customized values are overwritten bij default values !!
enter ? for other functions

The function can be started by selecting d_...._ where ... is a number of the function names can be concatenated.

The system will automatically install the file upon the first start of this module. 
 

### backend funtions SET_function
 - setupl 	also called instellingen tot setup and manitenace of the data
 - dummy	a dummy function
 - mail 	the mail function (this mail function is similar to the front end mail function)
 - stand	the standen function (this stand function is similar to the front end stand function)
 - leden	the member function (this leden function part is similar to the front end stand function)

 Depending on the contents of the SET file modules are made available it may be needed to modify it to get the required functionality

### frontend menu  SET_menu

- pass the login function	the default login function 

the other functions are to be used as administration functions
 
 - partner  the visitor maintenance function
 - dummy	a dummy function
 - mail 	the mail function
 - stand	the standen function 
 - leden	the member function 
 - tafel  tafel seatment function
 
 Depending on the contents of the SET file modules are made available it may be needed to modify it to get the required functionality
 
### For taxonomy (gsmoffl) use

the following is suggested

	 1. droplet	EN1 	(shared with a droplet)
	 2. droplet	EN4 	(shared with a droplet)
	 3. settin display 	(shared with a droplet)	 
	 4. setting	mail	mail		default tekst file for the mail
	 5. setting mail_max 200 		a limitation from the mail system 
	 
to run the mail functio there should be apage with the name mail 

- partner  the visitor maintenance function
 - dummy	a dummy function
 - mail 	the mail function
 - stand	the standen function 
 - leden	the member function 
 - tafel  tafel seatment function

## pass function

Basically this is tthe login page on the front end
showing
### for non registered users 
  - the possibility to login using e-mail address and password. For this there is a droplet as an alternative
  - the possibiity to register. the possibity to register as a new user with an e-mail address and a password. A confirmation code will be send by mail allowing the user to confirm the existence and the access to the e-mail adress
  - the possibility to change the password. Basically this is the same routine as the possibility to register. The data belonging to the used e-mail address will be kept. if the confirmation code is not provided the old password will be kept. 

### for registered users
- to alter the contact details of the person. To alter the e-mail address you need still to have access to the old e-mail address !!
- see which data is kept by the system about this person.
  When modules are propoerly installed this includes contact details and remarks, personal and general documents, rights. data on followed courses or course invitations or end of validity, payment details / open payments / partial payments. Membership from / until owner ship / meter standen  

## partner function 
basically the functon for registered users ( but now for the central administrator)

## stand	function. 
the function to see modify and add the varios standen records. (general purpose)

Note (not obvious) on the individual records the id can be modified on the standen type the line below Refer and the Description can be modified.
Save as new allows to add a record 
Process adds a tick field allowing to add multiple records (tick records and select save as new.
Process adds a tick field allowing records to be deactivated or to be removed

use select to see the non active records!


## leden	function 
a specialised function to alter the standen records for membership payment regitration

## tafel	function 
a specialised function alter and create table arrangements from the standen records 

### registration table number
for each of the activity the table numbers can be entered for the individual persons.

### output functions

in the following xyz is assumed the activity

#### print1xyz (or print xyz)
This will give a survey of all participants who payed their contribution
#### print2xyz 
This will give a survey of all participants who did not pay (yet) their contribution
#### print3xyz 
all participants to the program
#### part1xyz 
The names of all the participants (who payed) and their table seating. in the form fitting on an 8x4 sticker page
If no seating is printed seating is not shown. Check if seating is provided.
#### part2xyz 
The names of all the participants (who payed) on the right side (4 on an a4 page) of the page allowing to be used a place card
#### part3xyz 
Menu card to be combined with part2xyz 
To modify the data printed: 
taxonomy group tafel 
reference tafel0|gsmoffl upto and including tafel4|gsmoffl is shown on the  left side
reference tafel5|gsmoffl upto and including tafel9|gsmoffl is shown on the  right side
#### part4xyz 
Kaartje om de tafel te markeren (8 tafels)
#### part5xyz 
Presentielijst of voorintekenklijst
voor de titel titel 
taxonomy group tafel 
reference tafel10|gsmoffl
#### part6xyz 
Borst kaartjes
#### part8xyz 
Tafelkaarten conferentie

## dummy function 

Basically you should see this function only when the installation is not yet done as a signal that the installation is incomplete.

### mail function 

The mail function is used to send out mailings

The contents is basically a from a single wysiwyg section.

De mail kan parameters bevatten die automatisch individueel aangepast worden.
Vanuit adres record
{WEB_EMAIL}
{WEB_NAME}
{WEB_ADRES}
{WEB_CONTACT}
{WEB_SINDS}
{WEB_QTYSTR}
{WEB_ADRES0}
{WEB_ADRES1}
{WEB_ADRES2}
{WEB_ADRES3}

Vanuit standen record
{WEB_STAND}

Vanuit systeem functies
{WEB_MASTER}
{WEB_MASTER_MAIL}
{WEB_DATE}
{WEB_TIME}

### documents

Gebruikersbestanden
De directory  media/rapport bevat de files die worden getoond bij allen met de parameter mediadir kan dit worden aangepast
De subdirectory XX1 wordt getoond bij de persoon met customer ref XX1
Standaard de file formaten .pdf, .zip en .jpg worden getoond bij vlogin met de parameter allowed kan dit worden aangepast.