<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2024 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

$oFC->version [ 'setup_mod_go_standen' ] = "20240912"; 
/* 1 The table involved */
$product = "standen";
$oFC->file_ref [ 0 ] = LOAD_DBBASE . "_" . $product;
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Create / Upgrade function started ' . $oFC->file_ref [ 0 ] .  NL; 

/* 1B database creation if database does not exist */
$oFC->description .= $oFC->gsm_existDb ( $oFC->file_ref  [ 0 ] );

/* 1C Modifications needed will be stored at this place */
$job = array ();
	
/* 1D which fields are present in the main file */	
$result = array ( );
$database->execute_query ( sprintf ( 'DESCRIBE %s', $oFC->file_ref [ 0 ] ),
	true, 
	$result );
if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug ( array ($result ), __LINE__ . $oFC->file_ref [ 0 ] );
	
/* 1E_add /change fields not present  */
$localHulpA = array();
foreach ( $result as $row ) $localHulpA [$row [ 'Field' ]] = $row [ 'Type' ];

/* 1F wijzigen */
if ( isset ( $localHulpA[ 'reference' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `reference` `content_short` varchar(255) NOT NULL DEFAULT ''", $oFC->file_ref [ 0 ] );
	$localHulpA[ 'content_short' ] = true;
}
if ( isset ( $localHulpA[ 'omschrijving' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `omschrijving` `content_long` varchar(255) NOT NULL DEFAULT ''", $oFC->file_ref [ 0 ] );
	$localHulpA[ 'content_long' ] = true;
}
if ( isset ( $localHulpA[ 'standtype' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `standtype` `type` int(16) NOT NULL DEFAULT '0'", $oFC->file_ref [ 0 ] );
	$localHulpA[ 'type' ] = true;
}
if ( isset ( $localHulpA[ 'refer' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `refer` `ref` varchar(255) NOT NULL DEFAULT ''", $oFC->file_ref [ 0 ] );
	$localHulpA[ 'ref' ] = true;
}
if ( isset ( $localHulpA[ 'standist' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `standist` `amtist` decimal(9,2) NOT NULL DEFAULT '0.00'", $oFC->file_ref [ 0 ] );
	$localHulpA[ 'amtist' ] = true;
}
if ( isset ( $localHulpA[ 'standsoll' ] ) ) {  // wijzigen	
	$job [ ] = sprintf ( "ALTER TABLE `%s` CHANGE `standsoll` `amtsoll` decimal(9,2) NOT NULL DEFAULT '0.00'", $oFC->file_ref [ 0 ] );
	$localHulpA[ 'amtsoll' ] = true;
}
	
/* 1G toevoegen */
if ( !isset ( $localHulpA[ 'comment' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `comment` varchar(255) NOT NULL DEFAULT '' AFTER `zoek`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'content_long' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `content_long` varchar(255) NOT NULL DEFAULT '' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA [ 'amtist' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amtist`  decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA [ 'amtsoll' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD `amtsoll`  decimal(9,2) NOT NULL DEFAULT '0.00' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'datumist' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `datumist` date NOT NULL DEFAULT '0000-00-00' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'datumsoll' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `datumsoll` date NOT NULL DEFAULT '0000-00-00' AFTER `name`", $oFC->file_ref [ 0 ] );}
if ( !isset ( $localHulpA[ 'adresid' ] ) ) {  // toevoegen
	$job [ ] = sprintf ( "ALTER TABLE `%s` ADD  `adresid` int(11) NOT NULL DEFAULT '0' AFTER `name`", $oFC->file_ref [ 0 ] );}

/* 1H achtual upgrade */
if ( isset ( $job ) && count( $job ) > 0 ) {
	foreach( $job as $key => $query ) {
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
		$database->simple_query ( $query );
}	}
/* 1I upgraded */

/* 1J other entries needed ? */
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default settings checked' .  NL; 
$job = array ();

/* check if upload set for file types is present * /
$results = array ( );
$database->execute_query ( 
	sprintf ( "SELECT * FROM `%smod_go_taxonomy` WHERE `type` = 'setting' AND `ref` = '%s'", 
		TABLE_PREFIX, "allowed|gsmoffl|2" ) , 
	true, 
	$results );
if ( count ( $results ) == 0 ) { 
	$main_parameter = "jpg|html|zip|pdf|mp4";
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('setting', '%s', '%s', '1' )",
		TABLE_PREFIX , "allowed|gsmoffl|2", $main_parameter );
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' setting allowed  added  ' . NL;
	$oFC->setting [ 'allowed' ] = $main_parameter;
}
/* end */

if ( !isset ( $oFC->setting [ 'zoek' ] [ $product ] ) ) {
	$main_parameter = '|id|name|content_long|';
	$job [] = sprintf ( "INSERT INTO `%smod_go_taxonomy` ( `type`, `ref`, `name`, `active`) 
		VALUES ('zoek', '%s', '%s', '1' )",
		TABLE_PREFIX, $product, $main_parameter);
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' zoek schema toevoegd ' . $product . NL;
	$oFC->setting [ 'zoek' ] [ $product ] = $main_parameter;
}

/* 1K achtual upgrade */
if ( isset ( $job ) && count( $job ) > 0 ) {
	foreach( $job as $key => $query ) {
		$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' ' . $query . NL;
		$database->simple_query ( $query ); 
	} 
	$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Default setting added' .  NL; 
}
$oFC->description .= date ( "H:i:s " ) . __LINE__  . ' Upgrade function completed ' . $oFC->file_ref [ 0 ] .  NL; 
?> 