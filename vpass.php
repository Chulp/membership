<?php
/*
 *  @template       GSM_Lepton 7 Standard
 *  @version        see info.php of this template
 *  @author         Gerard Smelt
 *  @copyright      2014-2025 ContractHulp
 *  @license        see info.php of this template
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

/* module id*/  
$module_name = 'vpass';
$version='20250207';
$project = "Login";
$main_file = "adres"; 
$sub_file = "standen";
$default_template = '/display.lte';

/* start initialize module */
global $oLEPTON;
$oFC = gsmoffl::getInstance();
$oTWIG = lib_twig_box::getInstance ( );
$oTWIG-> registerModule ( LOAD_MODULE . LOAD_SUFFIX );
$template_name= '@' . LOAD_MODULE . LOAD_SUFFIX . "/". LANGUAGE . $default_template;

/* version data */
$oFC->version [ $module_name ] = $version;
$oFC->version = array_merge ( $oFC->version, $version_display);
$oFC->version [ $oFC-> language [ 'LANG' ] ] = $oFC-> language [ 'VERS' ];
$oFC->version [ "set" ] = $FC_SET [ 'version' ] ;
$project= sprintf ("%s %s " , $oFC-> language [ 'TXT_SETUP' ], strtoupper ( $main_file )) ;

/* file references */
$oFC->file_ref [ 99 ] = LOAD_DBBASE . "_".$main_file;
$oFC->file_ref [ 98 ] = LOAD_DBBASE . "_" .$sub_file;
$oFC->file_ref [ 97 ] = TABLE_PREFIX . 'users';
/* settings */
$oFC->setting [ 'includes' ] 		= $place [ 'includes' ];
$oFC->setting [ 'frontend' ] 		= $place [ 'frontend' ];
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "zoek" );
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "entity");
$oFC->gsm_initTaxo ( LOAD_MODULE . LOAD_SUFFIX, $oFC->user [ 'privileged' ], $FC_SET, "droplet");

if (!isset ( $oFC->setting [ 'master' ] ) ) $oFC->setting [ 'master' ] =  $oFC->setting [ 'droplet' ] [ LANGUAGE . '4' ];
if (!isset ( $oFC->setting [ 'email' ] ) ) $oFC->setting [ 'email' ] = $oFC->setting [ 'droplet' ] [ LANGUAGE . '10' ];

/* default values */
$oFC->page_content [ 'FORMULIER' ] = $project;
$oFC->page_content [ 'MODULE' ] = $module_name;
$oFC->page_content [ 'MODE' ] = 9;
$oFC->page_content [ 'PAGE_ID' ] = $page_id ?? 0;
$oFC->page_content [ 'SECTION_ID' ] = $section_id ?? 0;

/* extra */
$oFC->page_content [ 'CONTROLELINK' ] = '';
$oFC->page_content [ 'REFERENCE_ACTIVITY_0' ]= 1;
$oFC->page_content [ 'FORM_CLASS_S' ] = 'nixhier';
$oFC->page_content [ 'FORM_CLASS_D' ] = 'nixhier'; 
$oFC->page_content [ 'FORM_CLASS_F' ] = 'nixhier';
$oFC->page_content [ 'PASSWORD_CHAR' ] = "a-zA-Z0-9_-!$#*+@?";

/* Gebruik limited door rechthebbenden */
$oFC->nodata = false;
if ( $oFC->user [ 'privileged' ] > 0 ) {
	$oFC->user  = array_merge (	$oFC->user, $oFC->gsm_adresDet ( $page_id, $oFC->setting [ 'owner' ] ) );
	$oFC->recid = substr ( $oFC->user [ 'ref' ], 2) ; 
} else { 
	$oFC->nodata = true; 
}	

/* spif test */
$_SESSION[ 'page_h' ] = $oFC->page_content [ 'HASH' ]; 

/* get memory values */
$oFC->gsm_memorySaved ( );

if ($oFC->setting [ 'debug' ] == "yes" ) Gsm_debug (array ($_POST, $_GET ?? "", $oFC , $selection ), __LINE__ . __FUNCTION__ );  

/* 7.2 */ if ( FRONTEND_LOGIN == 'enabled' && VISIBILITY != 'private' && !isset($_SESSION['USER_ID']) )   {
/* 7.1 */ // if ( FRONTEND_LOGIN == 'enabled' && VISIBILITY != 'private' && $oLEPTON->get_session( 'USER_ID' ) == '' ) {
	// not logged in
	$oFC->page_content [ 'MODE' ] = 9;
	if ( isset( $_POST[ 'command' ] ) ) {
		$oFC->page_content [ 'REFERENCE_ACTIVITY_0' ] = 2;
		switch ( $_POST[ 'command' ] ) {
			case 'register' :  
				// process data from registration screen
				$LhulpA = sprintf ( "%s%s/%s_%s", LEPTON_PATH, $oFC->setting [ 'collectdir' ], $oFC->setting [ 'owner' ], session_id ( ) );
				//check data
				$LhulpB = $oFC->gsm_sanitizeStringS ( $_POST [ 'username' ], 's{LOWER|STRIP|CLEAN|TRIM|EMAIL}');
				$LhulpC = true;
				if ( strlen ( $_POST [ 'password' ] ) < 8 ) $LhulpC = false;
				if ( preg_match ("/[^A-Za-z0-9\@\_\-\!\$\ย\#\*\+]/", $_POST [ 'password' ] ) ) $LhulpC = false;
				if ( $LhulpC == false || $LhulpB == false )  {
					unset ( $_POST [ 'username' ] );
					unset ( $_POST [ 'password' ] );
					$oFC->description .= $oFC->language [ 'TXT_LOGIN_ERROR_SHORT' ];
				}
				if (isset ($_POST [ 'username' ] ) ) {
					$LhulpC = $oFC->gsm_sanitizeStrings ( $_POST [ 'username' ], "s{NAME}" );
					$LhulpD = $oFC->gsm_registerProcess ( $_POST[ 'username' ], $_POST[ 'password' ] );
					$oFC->description .= $LhulpD;
					if ( strlen ( $LhulpD ) <4 ) $oFC->page_content [ 'CONTROLELINK' ] = $oFC->gsm_pinCode ( array ( 0=>$_POST[ 'username' ], 1=>$LhulpC ), 1 ); 
					$oFC->page_content[ 'EMAIL' ] = $_POST [ 'username' ];
				}
				break;
			case 'getalok' : 
				$oFC->page_content [ 'CONTROLELINK' ] = $oFC->gsm_pinCode ( $_POST [ 'qf_controlegetal' ] ); 
				if ( $oFC->page_content [ 'CONTROLELINK' ] === true ) {  // pincode ok 
					$oFC->page_content [ 'REFERENCE_ACTIVITY_0' ] = 1;
					$oFC->description .= $oFC->language [ 'TXT_LOGIN_NOW' ];
				}
				$oFC->page_content[ 'EMAIL' ] = $_POST [ 'qf_email' ];
				break;
			default:
				$oFC->gsm_pinCode ( "", 9 ); 
				break;
		}
	} elseif ( isset( $_GET[ 'command' ] ) ) {
		$oFC->page_content [ 'REFERENCE_ACTIVITY_0' ] = 3;
		switch ( $_GET[ 'command' ] ) {
			case 'reset' :
				// escape route 
				$oFC->page_content['CONTROLELINK'] = $oFC->gsm_pinCode( "-", 9); 
				$oFC->page_content [ 'REFERENCE_ACTIVITY_0' ] = 2;
				break;
			case 'reg' :
				$oFC->page_content [ 'REFERENCE_ACTIVITY_0' ] = 2;
				break;
			default:
				// escape route 
				break;
		} 
	} else {
		$oFC->page_content [ 'MODE' ] = 9;
	}

/* 7.2 */} elseif ( FRONTEND_LOGIN == 'enabled' && ( $oLEPTON->getValue ( 'user_id', 'integer', 'session' ) > 0 ) ) {
//	LEPTON_VERSION . SUBVERSION
/* 7.1 */ // } elseif ( FRONTEND_LOGIN == 'enabled' && is_numeric( $oLEPTON->get_session( 'USER_ID' ) ) ) {
	// logged in 
	$oFC->page_content [ 'MODE' ] = 8;
	if ( isset ( $_POST[ 'command' ] ) ) {
		switch ( $_POST[ 'command' ] ) {
			case 'edit' : 
				$oFC->page_content [ 'MODE' ] = 7;
//				$oFC->recid = $_POST[ 'recid' ];
				$FieldArr = array ();
				$oFC->page_content [ 'ACCESS'] = $oFC->gsm_accessRec ( $FieldArr, $oFC->recid, 1, $main_file );
				$oFC->page_content [ 'ACCESS'] = array_merge ( $oFC->page_content [ 'ACCESS'] , $oFC->gsm_sanitizeStringNA ( 2, 'name' , $oFC->page_content [ 'ACCESS'] [ 'name' ] ) ) ;
				$oFC->page_content [ 'ACCESS'] = array_merge ( $oFC->page_content [ 'ACCESS'] , $oFC->gsm_sanitizeStringNA ( 2, 'adres' , $oFC->page_content [ 'ACCESS'] [ 'adres' ] ) ) ;
				break;
			case 'getalok' : 
				$oFC->page_content [ 'CONTROLELINK' ] = $oFC->gsm_pinCode ( $_POST [ 'qf_controlegetal' ] ); 
				if ( $oFC->page_content [ 'CONTROLELINK' ] === true ) {  // pincode ok 
					$oFC->page_content [ 'REFERENCE_ACTIVITY_0' ] = 1;
					$oFC->description .= $oFC->language [ 'TXT_LOGIN_NOW' ];
				}
//				$oFC->page_content[ 'EMAIL' ] = $_POST [ 'qf_email' ];
				break;
			case 'change' : 
				$fieldArr = array ();
				$oFC->page_content [ 'MODE' ] = 8;
				$fieldArr [ 'name'] = $oFC->gsm_sanitizeStringNA ( 3, 'name', $_POST [ 'gsmc_name1' ] ?? "" , $_POST [ 'gsmc_name2' ] ?? "", $_POST [ 'gsmc_name3' ] ?? "" , $_POST [ 'gsmc_name4' ] ?? "");
				$fieldArr [ 'adres'] = $oFC->gsm_sanitizeStringNA ( 3, 'adres', $_POST [ 'gsmc_adres1' ] ?? "" , $_POST [ 'gsmc_adres2' ] ?? "", $_POST [ 'gsmc_adres3' ] ?? "" , $_POST [ 'gsmc_adres4' ] ?? "");

				/* change */
				$oFC->page_content [ 'ACCESS'] = $oFC->gsm_accessRec ( $fieldArr, $oFC->recid, 1, $main_file, "gsm_", "email" ); 
				if ( $_POST [ 'gsmc_email' ] != $_POST [ 'gsm_email' ] )  $oFC->gsm_registerEmailChange ( $_POST [ 'gsm_email' ] , $_POST [ 'gsmc_email' ]);
				
				/* latest changed data */
				$oFC->page_content [ 'ACCESS'] = $oFC->gsm_accessRec ( $fieldArr, $oFC->recid, 1, $main_file, "gsm_", "email" );
				$oFC->page_content [ 'ACCESS'] = array_merge ( $oFC->page_content [ 'ACCESS'] , $oFC->gsm_sanitizeStringNA ( 2, 'name' , $oFC->page_content [ 'ACCESS'] [ 'name' ] ) ) ;
				$oFC->page_content [ 'ACCESS'] = array_merge ( $oFC->page_content [ 'ACCESS'] , $oFC->gsm_sanitizeStringNA ( 2, 'adres' , $oFC->page_content [ 'ACCESS'] [ 'adres' ] ) ) ;
				/* debug * / Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", $fieldArr, $oFC->recid, $main_file, $oFC ), __LINE__ . __FUNCTION__ ); /* debug */
				break;
			default:
				$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . $oFC->language [ 'TXT_ERROR_PAGE' ].  $_POST [ 'command' ] . NL;
				/* debug */ Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC, "selection" =>$selection ), __LINE__ . __FUNCTION__ ); /* debug */
				break;
		}
	} elseif ( isset ( $_GET[ 'command' ] ) ) {
		switch ( $_GET[ 'command' ] ) {
			default:
				$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . $oFC->language [ 'TXT_ERROR_PAGE' ].  $_GET [ 'command' ] . NL;
				/* debug */ Gsm_debug (array ("post"=> $_POST, "get"=>$_GET ?? "", "this"=>$oFC, "selection" =>$selection ), __LINE__ . __FUNCTION__ ); /* debug */
				// escape route 
				break;
		}
	} else {
//		$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . NL;
}	} 
/*
 * the output to the screen
 */
switch ( $oFC->page_content [ 'MODE' ] ) {
	case 7:
		$template_name = '@' . LOAD_MODULE . LOAD_SUFFIX . '/' . LANGUAGE . '/login_wijzigen.lte';
		break;
	case 8:
		$template_name ='@' . LOAD_MODULE . LOAD_SUFFIX. '/' . LANGUAGE . '/login_aangemeld.lte';
		$oFC->page_content [ 'LOGOUT_URL' ] = LOGOUT_URL . '?redirect=' . $_SERVER[ 'SCRIPT_NAME' ];
		$alleenUser = ( isset ( $oFC->field_content [ 'id' ] ) ) ? false : true ;
		if ( !isset ( $oFC->user [ 'name' ] ) ) $oFC->setting [ 'display' ] = "";
		$oFC->page_content [ 'ACCESS' ] [ 'name' ] = $oFC->user [ 'name' ] ?? "";
		$oFC->page_content [ 'ACCESS' ] [ 'adres' ] = $oFC->user [ 'adres' ] ?? "";
		$oFC->page_content [ 'ACCESS' ] [ 'email' ] = $oFC->user [ 'email' ] ?? "";
		$oFC->page_content [ 'ACCESS' ] [ 'contact' ]= $oFC->user [ 'tel' ] ?? "";

		if ( isset ( $oFC->setting [ 'display' ]  ) ) {
			$oFC->page_content [ 'FORM_CLASS_1' ] = 'nixhier';
			$oFC->page_content [ 'FORM_CLASS_2' ] = 'nixhier';
			$oFC->page_content [ 'FORM_CLASS_3' ] = 'nixhier';
			$oFC->page_content [ 'FORM_CLASS_S' ] = 'nixhier';
			$oFC->page_content [ 'FORM_CLASS_D' ] = 'nixhier';
			$oFC->page_content [ 'FORM_CLASS_F' ] = 'nixhier';
			if ( strstr ( $oFC->setting [ 'display' ] , "cont" ) )	$oFC->page_content [ 'FORM_CLASS_1' ] = "";
			if ( strstr ( $oFC->setting [ 'display' ] , "curs" ) )	$oFC->page_content [ 'FORM_CLASS_2' ] = "";
			if ( strstr ( $oFC->setting [ 'display' ] , "meld" ) )	$oFC->page_content [ 'FORM_CLASS_3' ] = "";
			if ( strstr ( $oFC->setting [ 'display' ] , "stand" ) ) $oFC->page_content [ 'FORM_CLASS_S' ] = "";
			if ( strstr ( $oFC->setting [ 'display' ] , "data" ) )  $oFC->page_content [ 'FORM_CLASS_D' ] = "";
			if ( strstr ( $oFC->setting [ 'display' ] , "file" ) )  $oFC->page_content [ 'FORM_CLASS_F' ] = "";
		}
		
		/* label */
		if ( $oFC->page_content [ 'FORM_CLASS_D' ] != 'nixhier' ) { 
			$oFC->page_content [ 'GSM_ADRES_LABEL' ] = $oFC->gsm_persoonInfo ( 'a', $oFC->field_content, $oFC->user );
			$oFC->page_content [ 'GSM_NOTES' ] = $oFC->gsm_persoonInfo ( 'b', $oFC->field_content, $oFC->user );
			$oFC->page_content [ 'GSM_REF' ] = $oFC->gsm_persoonInfo ( 'c', $oFC->field_content, $oFC->user );
		}

		/* Standen */
		if ( $oFC->page_content [ 'FORM_CLASS_S' ] != 'nixhier' ) { 
			$oFC->page_content [ 'GSM_STANDEN' ] = $oFC->gsm_standenStatus ( $oFC->recid );
			if (strlen ( $oFC->page_content ['GSM_STANDEN' ] ) < 140 )  $oFC->page_content ['FORM_CLASS_S'] = 'nixhier';
		}
		
		/* documenten */
		if ( $oFC->page_content [ 'FORM_CLASS_F' ] != 'nixhier' ) { 
			$oFC->page_content [ 'GSM_ARCHIEF' ] = "";
			if ( strlen( $oFC->user [ 'ref' ] ) > 2 ) {
				$help = array( $oFC->user [ 'ref' ] , substr ( $oFC->user [ 'ref' ] , 0, 2 ) ) ;
				$oFC->page_content ['GSM_ARCHIEF' ] .= $oFC->gsm_scanDir ( LEPTON_PATH . $oFC->setting [ 'mediadir' ], 3, 50, $help );
				$oFC->page_content ['GSM_ARCHIEF' ] .= $oFC->gsm_scanCastor (
					substr ( $oFC->user [ 'ref' ], 0, 2 ), 
					substr ( $oFC->user [ 'ref' ], 2 )  );
			}
			if (strlen($oFC->page_content ['GSM_ARCHIEF' ]) <10)  $oFC->page_content ['FORM_CLASS_F'] = 'nixhier';
		}
		break;
	default:
	case 9: 
		if ( $oFC->page_content [ 'REFERENCE_ACTIVITY_0' ] > 1 ){ 		// check case login system not initialised	
			$results = array ( );
			$TEMPLATE = "SHOW TABLE STATUS LIKE '%s'";
			if ( $database->execute_query ( sprintf ( $TEMPLATE, $oFC->file_ref [ 99 ] ) , true, $results ) && count ( $results ) <1 ) { 
				$oFC->page_content [ 'REFERENCE_ACTIVITY_0' ] = 1;
				$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . $oFC->language [ 'TXT_ERROR_INIT' ] . NL;
		}	}
		if ( defined ( 'LOGIN_URL' ) ) {
			$template_name = '@' . LOAD_MODULE.LOAD_SUFFIX. '/' . LANGUAGE . '/login_aanmelden.lte';
			$oFC->page_content [ 'FORMULIER' ] = $oFC->language [ 'TXT_LOGIN' ];
			$oFC->page_content [ 'RETURN1' ] = LOGIN_URL . '?redirect=' . $_SERVER[ 'SCRIPT_NAME' ];
			$oFC->page_content [ 'RETURN' ] = LOAD_RETURN;
			$oFC->page_content [ 'REDIRECT' ] = LEPTON_URL . $_SERVER['SCRIPT_NAME']."?section_id=" . $section_id; 
			if ( $oFC->page_content [ 'REFERENCE_ACTIVITY_0' ] == 2 )  $oFC->page_content [ 'FORMULIER' ] = $oFC->language [ 'TXT_LOGIN_REGISTER' ];
			$oFC->page_content [ 'CONTROLELINK' ] = $oFC->gsm_pinCode ( "" , 2 );  // pincode is uitgegeven ??
			if ( strlen ( $oFC->page_content [ 'CONTROLELINK' ] ) > 6 ) {
				$oFC->page_content [ 'REFERENCE_ACTIVITY_0' ] = 3;
				$oFC->page_content [ 'FORMULIER' ] = $oFC->language [ 'TXT_LOGIN_VERIFY' ];
			}
		} else {  // routine when frontend login is unavailable 
			$oFC->description .= date('G:i:s'.substr ( ( string ) microtime ( ), 1, 8).' : ') . __LINE__  . ' ' . $oFC->language [ 'TXT_NO_ACCESS' ] . NL;
		}
		break;
}

/* memory save */
$oFC->page_content ['MEMORY'] = $oFC->gsm_memorySaved ( ); 

/* output processing */
// als er boodschappen zijn deze tonen in een error blok
$oFC->page_content [ 'RECID' ] = $oFC->recid;
$oFC->page_content [ 'STATUS_MESSAGE' ] .= $oFC->description; 
if ( strlen ( $oFC->page_content [ 'STATUS_MESSAGE' ] ) >4 ) $oFC->page_content ['MESSAGE_CLASS']= "ui error message"; 
$oFC->page_content [ 'VERSIE' ] = $oFC->version; 
$oFC->page_content  [ 'MODE' ] = $oFC->page_content [ 'MODE' ];
if (LOAD_MODE == "x" )  $_SESSION[ 'last_edit_section' ] = $section_id; 

/* actual output */
echo $oTWIG->render( 
	$template_name, // template-filename
    $oFC->page_content // template-data
);

if ($oFC->setting [ 'debug' ] == "yes" ){
	Gsm_debug ($oFC->page_content, __LINE__ . $template_name );  
	if (LOAD_MODE == "x" )  Gsm_debug ($oFC->version, $template_name );
}
?>